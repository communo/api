<?php

declare(strict_types=1);

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude(['bin', 'var', 'vendor', 'node_modules', 'config', 'public']);

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules([
        '@Symfony' => true,
        '@PHP80Migration' => true,
        'no_extra_blank_lines' => [
            'tokens' => [
                'break',
                'continue',
                'extra',
                'return',
                'throw',
                'use',
                'parenthesis_brace_block',
                'square_brace_block',
                'curly_brace_block',
            ],
        ],
        'phpdoc_align' => ['align' => 'left'],
        'increment_style' => ['style' => 'post'],
        'no_unused_imports' => true,
        'no_useless_else' => true,
        'no_useless_return' => true,
        'ordered_class_elements' => true,
        'ordered_imports' => true,
        'backtick_to_shell_exec' => true,
        'declare_strict_types' => true,
        'no_superfluous_phpdoc_tags' => true,
        'yoda_style' => ['equal' => false, 'identical' => false, 'less_and_greater' => false],
        'phpdoc_to_comment' => false,
        'phpdoc_separation' => ['groups' => [
            [
                'deprecated',
                'link',
                'see',
                'since',
            ],
            [
                'author',
                'copyright',
                'license',
            ],
            [
                'category',
                'package',
                'subpackage',
            ],
            [
                'property',
                'property-read',
                'property-write',
            ],
            [
                'ParamConverter',
                'Entity',
                'IsGranted',
            ],
            [
                'return',
                'throws',
            ],
        ]],
    ])
    ->setFinder($finder);
