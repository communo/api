include .env
export

# Executables (local)
DOCKER_COMPOSE_FILE = -f docker-compose.yml -f docker-compose.override.yml
DOCKER_COMP = docker compose $(DOCKER_COMPOSE_FILE)
DOCKER_OPTS=
# Docker containers
PHP_RUN = $(DOCKER_COMP) run --rm --no-deps php
EXEC_PHP = $(DOCKER_COMP) exec -T php
PHP_CONT = $(DOCKER_COMP) exec php
DB_CONT = $(DOCKER_COMP) exec database
RUN_S3CMD = docker run --rm -v $(PWD):/app d3fk/s3cmd --config /app/.s3cfg

# Executables
PHP      = $(PHP_CONT) php
COMPOSER = $(PHP_RUN) composer
SYMFONY  = $(PHP_CONT) bin/console

# Misc
.DEFAULT_GOAL = help
.PHONY        = help build up start down logs sh composer vendor sf cc

## —— 🎵 🐳 The Symfony Docker Makefile 🐳 🎵 ——————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Docker 🐳 ————————————————————————————————————————————————————————————————
BUILD_OPT=--pull
build: ## Builds the Docker images
	$(DOCKER_COMP) build ${BUILD_OPT}

push:
	@$(DOCKER_COMP) push

pull:
	@$(DOCKER_COMP) pull

up: DOCKER_OPTS = --detach
up: ## Start the docker hub in detached mode (no logs)
	$(DOCKER_COMP) up ${DOCKER_OPTS}

docker-login:
	@echo "LOGIN TO GITLAB CONTAINER REGISTRY";
	@echo "You can generate some token with read_registry scope in https://gitlab.com/-/profile/personal_access_tokens";
	@echo "=====================";
	@read -p "Username: " username; \
	read -p "Token: " token; \
	echo $$token | docker login registry.gitlab.com --username $$username --password-stdin

docker-network:
	@docker network create 'communo'

prod:
	@$(DOCKER_COMP) -f docker-compose.yml up --detach

start: build up

restart:
	@$(DOCKER_COMP) restart

down:
	@$(DOCKER_COMP) down --remove-orphans

logs: ## Show live logs
	@$(DOCKER_COMP) logs --follow $(CONTAINER) -n 100

ps: ## Show process
	@$(DOCKER_COMP) ps -a

sh:
	$(DOCKER_COMP) exec php bash

dump_workflow:
	$(EXEC_PHP) bin/console workflow:dump ${WORKFLOW_NAME} | dot -Tjpeg -o workflow_${WORKFLOW_NAME}.jpg

stop:
	@$(DOCKER_COMP) stop

## —— Composer 🧙 ——————————————————————————————————————————————————————————————
composer: ## Run composer, pass the parameter "c=" to run a given command, example: make composer c='req symfony/orm-pack'
	@$(eval c ?=)
	@$(COMPOSER) $(c)

vendor: ## Install vendors according to the current composer.lock file
vendor: c=install --prefer-dist --no-progress --no-scripts --no-interaction
vendor: composer

## —— Symfony 🎵 ———————————————————————————————————————————————————————————————
sf: ## List all Symfony commands or pass the parameter "c=" to run a given command, example: make sf c=about
	@$(eval c ?=)
	@$(SYMFONY) $(c)

cc: c=c:c ## Clear the cache
cc: sf

waiter:
	@echo 🔥🔥🔥 heavy and destructive command 🔥🔥🔥
	@echo "Will start in 5 seconds. Press ctrl + C to cancel."
	@sleep 5

hard-reset: waiter up
	rm -rf vendor/* var/*
	$(DOCKER_COMP) kill
	$(DOCKER_COMP) down --volumes --remove-orphans

hard-rebuild: hard-reset build install minio_create_bucket fixtures

install: start vendor db_reset cc migration_migrate jwt-generate-keypair open_browser

MESSENGER_MEMORY_LIMIT = 1G
MESSENGER_TIME_LIMIT = 7200
MESSENGER_OPT = -vv
messenger-consume: PHP_OPTIONS = -dmemory_limit=$(MESSENGER_MEMORY_LIMIT)
messenger-consume:
	$(SYMFONY) messenger:consume async $(MESSENGER_OPT) --memory-limit=$(MESSENGER_MEMORY_LIMIT) --time-limit=$(MESSENGER_TIME_LIMIT)
messenger-stop:
	$(SYMFONY) messenger:stop $(MESSENGER_OPT)

jwt-generate-keypair:
	$(SYMFONY) lexik:jwt:generate-keypair --overwrite

jwt-generate-token:
	$(SYMFONY) lexik:jwt:generate-token ${u}
token: jwt-generate-token

open_browser:
	open $(SERVER_NAME):$(HTTP_PORT)/graphql

## Database
## -----
##
MIGRATION_OPTIONS=
db_drop:
	$(SYMFONY) doctrine:database:drop --force | true
db_create:
	$(SYMFONY) doctrine:database:create
db_reset: db_drop db_create db_enable_postgis migration_migrate

DB_URL ?= postgres://$(POSTGRES_USER):$(POSTGRES_PASSWORD)@localhost:$(POSTGRES_PORT)/$(POSTGRES_DB)
db_dump_restore: db_drop db_create
	@$(DB_CONT) pg_restore --clean --no-acl --no-owner --dbname=${DB_URL} /app/$(DUMP_FILE)
db_dump:
	$(DB_CONT) pg_dump -p 5432 -d $(POSTGRES_DB) -U $(POSTGRES_USER) -Fc >> db.dump
db_bash:
	$(DB_CONT) bash
db_enable_postgis:
	$(DB_CONT) psql -p 5432 -d $(POSTGRES_DB) -U $(POSTGRES_USER) -c "CREATE EXTENSION postgis;"

fixtures:
	$(SYMFONY) hautelook:fixtures:load --no-interaction --purge-with-truncate --no-bundles
.PHONY: fixtures

migration_diff:
	$(SYMFONY) doctrine:migrations:diff $(MIGRATION_OPTIONS)

migration_migrate:
	$(SYMFONY) doctrine:migrations:migrate -n $(MIGRATION_OPTIONS)

update_qa_image:
	@docker pull jakzal/phpqa:php8.2-alpine

PHPSTAN_CONFIG_FILE=phpstan.dist.neon
PHPSTAN_LEVEL=7
PHPSTAN_DIR=src
phpstan_jakzal: update_qa_image ## Perform PHPStan check
	docker run -t --volume $(PWD):/srv/app --workdir /srv/app jakzal/phpqa:php8.2-alpine phpstan analyse --no-progress -c $(PHPSTAN_CONFIG_FILE) $(PHPSTAN_DIR) --level $(PHPSTAN_LEVEL)
phpstan:
	@echo "Running PHPSTAN with level \033[92m$(PHPSTAN_LEVEL)\033[0m"
	@$(PHP) vendor/bin/phpstan analyse --no-progress -c $(PHPSTAN_CONFIG_FILE) $(PHPSTAN_DIR) --level $(PHPSTAN_LEVEL)

PHPCS_OPTIONS = --verbose --show-progress=dots
phpcs: update_qa_image ## Run PHP-CS fixer
	docker run -t --volume $(PWD):/srv/app --workdir /srv/app jakzal/phpqa:php8.2-alpine php-cs-fixer fix --config=.php-cs-fixer.dist.php $(PHPCS_OPTIONS)
phpcs-dry:
	make --no-print-directory phpcs PHPCS_OPTIONS="--dry-run --diff"
opt=
psalm: update_qa_image ## PSALM check
	docker run -t --volume $(PWD):/srv/app --workdir /srv/app jakzal/phpqa:php8.2-alpine psalm $(opt)

PHPUNIT = $(EXEC_PHP) vendor/bin/phpunit
PHPUNIT_OPTIONS=
phpunit: export APP_ENV=test
phpunit:
	$(PHPUNIT) $(PHPUNIT_OPTIONS)


debug: export XDEBUG_MODE=debug
debug: stop up
debug_off: export XDEBUG_MODE=off
debug_off: build up

minio_create_bucket: DOCKER_COMPOSE_FILE = -f docker-compose.yml -f docker-compose.override.yml -f docker/minio/createBucket.yml
minio_create_bucket: DOCKER_OPTS="--remove-orphans createBucket"
minio_create_bucket: up

MINIO_CERTS_PATH=var/certs
minio_generate_certs:
	mkdir -p $(MINIO_CERTS_PATH)
	openssl req -x509 -newkey rsa:4096 -keyout $(MINIO_CERTS_PATH)/private.key -out $(MINIO_CERTS_PATH)/public.crt -days 365 -nodes
	chmod 644 $(MINIO_CERTS_PATH)/public.crt
	chmod 600 $(MINIO_CERTS_PATH)/private.key

S3_CORS_FILE=docker/minio/cors.xml
s3_cors_set:
	$(RUN_S3CMD) -s setcors /app/$(S3_CORS_FILE) s3://$(S3_BUCKET_NAME)

s3_cors_info:
	@$(RUN_S3CMD) -s info s3://$(S3_BUCKET_NAME)


#postgresql://uaclxceog3tgsbucrhak:OIM3EZRxqgIzU7CF2GEgc1h9hNwowm@bzzxr0zqojdipxxetdcp-postgresql.services.clever-cloud.com:5384/bzzxr0zqojdipxxetdcp
