<img src="public/images/svg/communo-yellow-banner-en.svg" alt="Communo"/>

Communo is a [free GPL3 licensed](LICENSE.md) platform developed to offer communities tools to develop solidarity and in particular the pooling
of goods & services. The project can accept donations to sustain its work, but it can’t seek to make a profit by selling 
services, by charging for enhancements or add-ons, or by other means.

This repo is mainly built with [Symfony](https://github.com/symfony/symfony), [ApiPlatform](https://github.com/api-platform/api-platform). 
This is the `api` part of the Communo project and you'll probably be interested by the 👉️ [communo/front](https://gitlab.com/communo/front) client.

## Installation

```bash
make install
```
```bash
make fixtures #if you want fixtures
```

## Documentation

There is a lot of documentation, so please read it and do not hesitate to participate :

👉️ https://letscommuno.atlassian.net/l/cp/1Szs3cfJ