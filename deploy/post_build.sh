bin/console lexik:jwt:generate-keypair --overwrite
bin/console doctrine:migrations:migrate -n
bin/console cache:clear
bin/console cache:warmup