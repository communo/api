<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220813225026 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE refresh_token_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE avatar (id UUID NOT NULL, file_path VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN avatar.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE circle (id UUID NOT NULL, parent_id UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, logo_name VARCHAR(255) DEFAULT NULL, logo_size INT DEFAULT NULL, description TEXT DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D4B76579727ACA70 ON circle (parent_id)');
        $this->addSql('COMMENT ON COLUMN circle.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle.parent_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE circle_user (circle_id UUID NOT NULL, user_id UUID NOT NULL, PRIMARY KEY(circle_id, user_id))');
        $this->addSql('CREATE INDEX IDX_DC9CB5370EE2FF6 ON circle_user (circle_id)');
        $this->addSql('CREATE INDEX IDX_DC9CB53A76ED395 ON circle_user (user_id)');
        $this->addSql('COMMENT ON COLUMN circle_user.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_user.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE discussion (id UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN discussion.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE discussion_user (discussion_id UUID NOT NULL, user_id UUID NOT NULL, PRIMARY KEY(discussion_id, user_id))');
        $this->addSql('CREATE INDEX IDX_A8FD7A7F1ADED311 ON discussion_user (discussion_id)');
        $this->addSql('CREATE INDEX IDX_A8FD7A7FA76ED395 ON discussion_user (user_id)');
        $this->addSql('COMMENT ON COLUMN discussion_user.discussion_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN discussion_user.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE material (id UUID NOT NULL, category_id UUID NOT NULL, owner_id UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, brand VARCHAR(255) DEFAULT NULL, reference VARCHAR(255) DEFAULT NULL, model VARCHAR(255) DEFAULT NULL, description TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7CBE759512469DE2 ON material (category_id)');
        $this->addSql('CREATE INDEX IDX_7CBE75957E3C61F9 ON material (owner_id)');
        $this->addSql('COMMENT ON COLUMN material.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material.category_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material.owner_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE material_booking (id UUID NOT NULL, material_id UUID DEFAULT NULL, user_id UUID NOT NULL, discussion_id UUID NOT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, status VARCHAR(15) DEFAULT \'estimating\' NOT NULL, price DOUBLE PRECISION DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_51963671E308AC6F ON material_booking (material_id)');
        $this->addSql('CREATE INDEX IDX_51963671A76ED395 ON material_booking (user_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_519636711ADED311 ON material_booking (discussion_id)');
        $this->addSql('COMMENT ON COLUMN material_booking.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material_booking.material_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material_booking.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material_booking.discussion_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE material_booking_date_period (id UUID NOT NULL, booking_id UUID NOT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, price DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A91D728C3301C60 ON material_booking_date_period (booking_id)');
        $this->addSql('COMMENT ON COLUMN material_booking_date_period.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material_booking_date_period.booking_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE material_category (id UUID NOT NULL, parent_id UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_40943D63727ACA70 ON material_category (parent_id)');
        $this->addSql('COMMENT ON COLUMN material_category.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material_category.parent_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE material_image (id UUID NOT NULL, material_id UUID NOT NULL, image_name VARCHAR(255) NOT NULL, image_size INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_93DAA811E308AC6F ON material_image (material_id)');
        $this->addSql('COMMENT ON COLUMN material_image.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material_image.material_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE message (id UUID NOT NULL, author_id UUID DEFAULT NULL, discussion_id UUID NOT NULL, content TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B6BD307FF675F31B ON message (author_id)');
        $this->addSql('CREATE INDEX IDX_B6BD307F1ADED311 ON message (discussion_id)');
        $this->addSql('COMMENT ON COLUMN message.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN message.author_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN message.discussion_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE pricing (id UUID NOT NULL, material_id UUID NOT NULL, value DOUBLE PRECISION NOT NULL, period DOUBLE PRECISION NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E5F1AC33E308AC6F ON pricing (material_id)');
        $this->addSql('COMMENT ON COLUMN pricing.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN pricing.material_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE pricing_circle (pricing_id UUID NOT NULL, circle_id UUID NOT NULL, PRIMARY KEY(pricing_id, circle_id))');
        $this->addSql('CREATE INDEX IDX_4E2424478864AF73 ON pricing_circle (pricing_id)');
        $this->addSql('CREATE INDEX IDX_4E24244770EE2FF6 ON pricing_circle (circle_id)');
        $this->addSql('COMMENT ON COLUMN pricing_circle.pricing_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN pricing_circle.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE rating (id UUID NOT NULL, author_id UUID DEFAULT NULL, user_id UUID NOT NULL, booking_id UUID NOT NULL, value SMALLINT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D8892622F675F31B ON rating (author_id)');
        $this->addSql('CREATE INDEX IDX_D8892622A76ED395 ON rating (user_id)');
        $this->addSql('CREATE INDEX IDX_D88926223301C60 ON rating (booking_id)');
        $this->addSql('COMMENT ON COLUMN rating.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN rating.author_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN rating.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN rating.booking_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE refresh_token (id INT NOT NULL, refresh_token VARCHAR(128) NOT NULL, username VARCHAR(255) NOT NULL, valid TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C74F2195C74F2195 ON refresh_token (refresh_token)');
        $this->addSql('CREATE TABLE settings (id UUID NOT NULL, language VARCHAR(5) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN settings.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE "user" (id UUID NOT NULL, avatar_id UUID DEFAULT NULL, settings_id UUID DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(255) DEFAULT NULL, lastname VARCHAR(255) NOT NULL, phone_number_object VARCHAR(35) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, gender VARCHAR(255) DEFAULT NULL, confirmed BOOLEAN DEFAULT false NOT NULL, confirmation_token TEXT DEFAULT NULL, street_address VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D64986383B10 ON "user" (avatar_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D64959949888 ON "user" (settings_id)');
        $this->addSql('COMMENT ON COLUMN "user".id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "user".avatar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "user".settings_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "user".phone_number_object IS \'(DC2Type:phone_number)\'');
        $this->addSql('CREATE TABLE messenger_messages (id BIGSERIAL NOT NULL, body TEXT NOT NULL, headers TEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql('CREATE OR REPLACE FUNCTION notify_messenger_messages() RETURNS TRIGGER AS $$
            BEGIN
                PERFORM pg_notify(\'messenger_messages\', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$ LANGUAGE plpgsql;');
        $this->addSql('DROP TRIGGER IF EXISTS notify_trigger ON messenger_messages;');
        $this->addSql('CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON messenger_messages FOR EACH ROW EXECUTE PROCEDURE notify_messenger_messages();');
        $this->addSql('ALTER TABLE circle ADD CONSTRAINT FK_D4B76579727ACA70 FOREIGN KEY (parent_id) REFERENCES circle (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_user ADD CONSTRAINT FK_DC9CB5370EE2FF6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_user ADD CONSTRAINT FK_DC9CB53A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE discussion_user ADD CONSTRAINT FK_A8FD7A7F1ADED311 FOREIGN KEY (discussion_id) REFERENCES discussion (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE discussion_user ADD CONSTRAINT FK_A8FD7A7FA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material ADD CONSTRAINT FK_7CBE759512469DE2 FOREIGN KEY (category_id) REFERENCES material_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material ADD CONSTRAINT FK_7CBE75957E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_booking ADD CONSTRAINT FK_51963671E308AC6F FOREIGN KEY (material_id) REFERENCES material (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_booking ADD CONSTRAINT FK_51963671A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_booking ADD CONSTRAINT FK_519636711ADED311 FOREIGN KEY (discussion_id) REFERENCES discussion (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_booking_date_period ADD CONSTRAINT FK_A91D728C3301C60 FOREIGN KEY (booking_id) REFERENCES material_booking (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_category ADD CONSTRAINT FK_40943D63727ACA70 FOREIGN KEY (parent_id) REFERENCES material_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_image ADD CONSTRAINT FK_93DAA811E308AC6F FOREIGN KEY (material_id) REFERENCES material (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FF675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F1ADED311 FOREIGN KEY (discussion_id) REFERENCES discussion (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pricing ADD CONSTRAINT FK_E5F1AC33E308AC6F FOREIGN KEY (material_id) REFERENCES material (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pricing_circle ADD CONSTRAINT FK_4E2424478864AF73 FOREIGN KEY (pricing_id) REFERENCES pricing (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pricing_circle ADD CONSTRAINT FK_4E24244770EE2FF6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rating ADD CONSTRAINT FK_D8892622F675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rating ADD CONSTRAINT FK_D8892622A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rating ADD CONSTRAINT FK_D88926223301C60 FOREIGN KEY (booking_id) REFERENCES material_booking (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D64986383B10 FOREIGN KEY (avatar_id) REFERENCES avatar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D64959949888 FOREIGN KEY (settings_id) REFERENCES settings (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE refresh_token_id_seq CASCADE');
        $this->addSql('ALTER TABLE circle DROP CONSTRAINT FK_D4B76579727ACA70');
        $this->addSql('ALTER TABLE circle_user DROP CONSTRAINT FK_DC9CB5370EE2FF6');
        $this->addSql('ALTER TABLE circle_user DROP CONSTRAINT FK_DC9CB53A76ED395');
        $this->addSql('ALTER TABLE discussion_user DROP CONSTRAINT FK_A8FD7A7F1ADED311');
        $this->addSql('ALTER TABLE discussion_user DROP CONSTRAINT FK_A8FD7A7FA76ED395');
        $this->addSql('ALTER TABLE material DROP CONSTRAINT FK_7CBE759512469DE2');
        $this->addSql('ALTER TABLE material DROP CONSTRAINT FK_7CBE75957E3C61F9');
        $this->addSql('ALTER TABLE material_booking DROP CONSTRAINT FK_51963671E308AC6F');
        $this->addSql('ALTER TABLE material_booking DROP CONSTRAINT FK_51963671A76ED395');
        $this->addSql('ALTER TABLE material_booking DROP CONSTRAINT FK_519636711ADED311');
        $this->addSql('ALTER TABLE material_booking_date_period DROP CONSTRAINT FK_A91D728C3301C60');
        $this->addSql('ALTER TABLE material_category DROP CONSTRAINT FK_40943D63727ACA70');
        $this->addSql('ALTER TABLE material_image DROP CONSTRAINT FK_93DAA811E308AC6F');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307FF675F31B');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307F1ADED311');
        $this->addSql('ALTER TABLE pricing DROP CONSTRAINT FK_E5F1AC33E308AC6F');
        $this->addSql('ALTER TABLE pricing_circle DROP CONSTRAINT FK_4E2424478864AF73');
        $this->addSql('ALTER TABLE pricing_circle DROP CONSTRAINT FK_4E24244770EE2FF6');
        $this->addSql('ALTER TABLE rating DROP CONSTRAINT FK_D8892622F675F31B');
        $this->addSql('ALTER TABLE rating DROP CONSTRAINT FK_D8892622A76ED395');
        $this->addSql('ALTER TABLE rating DROP CONSTRAINT FK_D88926223301C60');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D64986383B10');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D64959949888');
        $this->addSql('DROP TABLE avatar');
        $this->addSql('DROP TABLE circle');
        $this->addSql('DROP TABLE circle_user');
        $this->addSql('DROP TABLE discussion');
        $this->addSql('DROP TABLE discussion_user');
        $this->addSql('DROP TABLE material');
        $this->addSql('DROP TABLE material_booking');
        $this->addSql('DROP TABLE material_booking_date_period');
        $this->addSql('DROP TABLE material_category');
        $this->addSql('DROP TABLE material_image');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE pricing');
        $this->addSql('DROP TABLE pricing_circle');
        $this->addSql('DROP TABLE rating');
        $this->addSql('DROP TABLE refresh_token');
        $this->addSql('DROP TABLE settings');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
