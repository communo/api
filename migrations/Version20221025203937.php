<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221025203937 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'harmonize images, add slugs';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE avatar RENAME file_path TO image_name');
        $this->addSql('ALTER TABLE avatar ADD image_size INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE avatar ALTER image_size DROP DEFAULT');
        $this->addSql('ALTER TABLE material_image ALTER image_size DROP DEFAULT');
        $this->addSql('ALTER TABLE avatar ALTER image_name SET NOT NULL');
        $this->addSql('ALTER TABLE avatar ALTER image_size DROP DEFAULT');
        $this->addSql('ALTER TABLE material_image ALTER image_size DROP DEFAULT');
        $this->addSql('ALTER TABLE "user" ALTER password DROP NOT NULL');
        $this->addSql('CREATE TABLE circle_logo (id UUID NOT NULL, image_name VARCHAR(255) NOT NULL, image_size INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN circle_logo.id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE circle ADD logo_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE circle DROP logo_name');
        $this->addSql('ALTER TABLE circle DROP logo_size');
        $this->addSql('COMMENT ON COLUMN circle.logo_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE circle ADD CONSTRAINT FK_D4B76579F98F144A FOREIGN KEY (logo_id) REFERENCES circle_logo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4B76579F98F144A ON circle (logo_id)');
        $this->addSql('ALTER TABLE material ADD slug VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE material ALTER slug SET NOT NULL');
        $this->addSql('ALTER TABLE "user" ADD slug VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE circle ADD slug VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE material_category ADD slug VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE avatar RENAME image_name TO file_path');
        $this->addSql('ALTER TABLE avatar DROP image_size');
        $this->addSql('ALTER TABLE material_image ALTER image_size SET DEFAULT 0');
        $this->addSql('ALTER TABLE avatar ALTER image_name DROP NOT NULL');
        $this->addSql('ALTER TABLE avatar ALTER image_size SET DEFAULT 0');
        $this->addSql('ALTER TABLE "user" ALTER password SET NOT NULL');
        $this->addSql('ALTER TABLE circle DROP CONSTRAINT FK_D4B76579F98F144A');
        $this->addSql('DROP TABLE circle_logo');
        $this->addSql('DROP INDEX UNIQ_D4B76579F98F144A');
        $this->addSql('ALTER TABLE circle ADD logo_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE circle ADD logo_size INT DEFAULT NULL');
        $this->addSql('ALTER TABLE circle DROP logo_id');
        $this->addSql('ALTER TABLE material DROP slug');
        $this->addSql('ALTER TABLE "user" DROP slug');
        $this->addSql('ALTER TABLE material ALTER slug DROP NOT NULL');
        $this->addSql('ALTER TABLE circle DROP slug');
        $this->addSql('ALTER TABLE material_category DROP slug');
    }
}
