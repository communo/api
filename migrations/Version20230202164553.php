<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230202164553 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'allow a booking not having a discussion (unavailability case)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material_booking ALTER discussion_id DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material_booking ALTER discussion_id SET NOT NULL');
    }
}
