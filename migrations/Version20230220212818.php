<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230220212818 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'cascade on user delete';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material DROP CONSTRAINT FK_7CBE75957E3C61F9');
        $this->addSql('ALTER TABLE material ADD CONSTRAINT FK_7CBE75957E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_booking DROP CONSTRAINT FK_51963671A76ED395');
        $this->addSql('ALTER TABLE material_booking ADD CONSTRAINT FK_51963671A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307FF675F31B');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FF675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rating DROP CONSTRAINT FK_D8892622F675F31B');
        $this->addSql('ALTER TABLE rating DROP CONSTRAINT FK_D8892622A76ED395');
        $this->addSql('ALTER TABLE rating DROP CONSTRAINT FK_D88926223301C60');
        $this->addSql('ALTER TABLE rating ALTER user_id DROP NOT NULL');
        $this->addSql('ALTER TABLE rating ADD CONSTRAINT FK_D8892622F675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rating ADD CONSTRAINT FK_D8892622A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rating ADD CONSTRAINT FK_D88926223301C60 FOREIGN KEY (booking_id) REFERENCES material_booking (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE rating DROP CONSTRAINT fk_d8892622f675f31b');
        $this->addSql('ALTER TABLE rating DROP CONSTRAINT fk_d8892622a76ed395');
        $this->addSql('ALTER TABLE rating DROP CONSTRAINT fk_d88926223301c60');
        $this->addSql('ALTER TABLE rating ALTER user_id SET NOT NULL');
        $this->addSql('ALTER TABLE rating ADD CONSTRAINT fk_d8892622f675f31b FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rating ADD CONSTRAINT fk_d8892622a76ed395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rating ADD CONSTRAINT fk_d88926223301c60 FOREIGN KEY (booking_id) REFERENCES material_booking (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material DROP CONSTRAINT fk_7cbe75957e3c61f9');
        $this->addSql('ALTER TABLE material ADD CONSTRAINT fk_7cbe75957e3c61f9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT fk_b6bd307ff675f31b');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT fk_b6bd307ff675f31b FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_booking DROP CONSTRAINT fk_51963671a76ed395');
        $this->addSql('ALTER TABLE material_booking ADD CONSTRAINT fk_51963671a76ed395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
