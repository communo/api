<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230221223416 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add circle cover and rename circle_logo to circle_image';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle DROP CONSTRAINT FK_D4B76579F98F144A');
        $this->addSql('ALTER TABLE circle_logo rename to circle_image');
        $this->addSql('ALTER TABLE circle ADD cover_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE circle ADD website VARCHAR(255) DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN circle.cover_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE circle ADD CONSTRAINT FK_D4B76579922726E9 FOREIGN KEY (cover_id) REFERENCES circle_image (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle ADD CONSTRAINT FK_D4B76579F98F144A FOREIGN KEY (logo_id) REFERENCES circle_image (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4B76579922726E9 ON circle (cover_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle DROP CONSTRAINT FK_D4B76579922726E9');
        $this->addSql('ALTER TABLE circle_image rename to circle_logo');
        $this->addSql('ALTER TABLE circle DROP CONSTRAINT fk_d4b76579f98f144a');
        $this->addSql('DROP INDEX UNIQ_D4B76579922726E9');
        $this->addSql('ALTER TABLE circle DROP cover_id');
        $this->addSql('ALTER TABLE circle DROP website');
        $this->addSql('ALTER TABLE circle ADD CONSTRAINT fk_d4b76579f98f144a FOREIGN KEY (logo_id) REFERENCES circle_logo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
