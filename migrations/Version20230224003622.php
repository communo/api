<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230224003622 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'change manyToMany relation between user and circle to add permission inside + add missing vich image fields';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE circle_membership (circle_id UUID NOT NULL, user_id UUID NOT NULL, id UUID NOT NULL, permission TEXT NOT NULL, PRIMARY KEY(circle_id, user_id))');
        $this->addSql('CREATE INDEX IDX_F947540D70EE2FF6 ON circle_membership (circle_id)');
        $this->addSql('CREATE INDEX IDX_F947540DA76ED395 ON circle_membership (user_id)');
        $this->addSql('COMMENT ON COLUMN circle_membership.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_membership.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_membership.id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE circle_membership ADD CONSTRAINT FK_F947540D70EE2FF6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_membership ADD CONSTRAINT FK_F947540DA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE avatar ADD mime VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE avatar ADD original_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE avatar ADD dimensions TEXT NOT NULL');
        $this->addSql('ALTER TABLE avatar RENAME COLUMN image_size TO size');
        $this->addSql('COMMENT ON COLUMN avatar.dimensions IS \'(DC2Type:simple_array)\'');
        $this->addSql('ALTER TABLE circle_image ADD mime VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE circle_image ADD original_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE circle_image ADD dimensions TEXT NOT NULL');
        $this->addSql('ALTER TABLE circle_image RENAME COLUMN image_size TO size');
        $this->addSql('COMMENT ON COLUMN circle_image.dimensions IS \'(DC2Type:simple_array)\'');
        $this->addSql('ALTER TABLE material_image ADD mime VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE material_image ADD original_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE material_image ADD dimensions TEXT NOT NULL');
        $this->addSql('ALTER TABLE material_image RENAME COLUMN image_size TO size');
        $this->addSql('COMMENT ON COLUMN material_image.dimensions IS \'(DC2Type:simple_array)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle_membership DROP CONSTRAINT FK_F947540D70EE2FF6');
        $this->addSql('ALTER TABLE circle_membership DROP CONSTRAINT FK_F947540DA76ED395');
        $this->addSql('DROP TABLE circle_membership');
        $this->addSql('ALTER TABLE circle_image DROP mime');
        $this->addSql('ALTER TABLE circle_image DROP original_name');
        $this->addSql('ALTER TABLE circle_image DROP dimensions');
        $this->addSql('ALTER TABLE circle_image RENAME COLUMN size TO image_size');
        $this->addSql('ALTER TABLE material_image DROP mime');
        $this->addSql('ALTER TABLE material_image DROP original_name');
        $this->addSql('ALTER TABLE material_image DROP dimensions');
        $this->addSql('ALTER TABLE material_image RENAME COLUMN size TO image_size');
        $this->addSql('ALTER TABLE avatar DROP mime');
        $this->addSql('ALTER TABLE avatar DROP original_name');
        $this->addSql('ALTER TABLE avatar DROP dimensions');
        $this->addSql('ALTER TABLE avatar RENAME COLUMN size TO image_size');
    }
}
