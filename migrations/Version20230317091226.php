<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230317091226 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'change manyToMany MaterialPricing:circles to ManyToOne MaterialPricing:circle';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE material_pricing (id UUID NOT NULL, material_id UUID NOT NULL, circle_id UUID DEFAULT NULL, value DOUBLE PRECISION DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_546B779CE308AC6F ON material_pricing (material_id)');
        $this->addSql('CREATE INDEX IDX_546B779C70EE2FF6 ON material_pricing (circle_id)');
        $this->addSql('COMMENT ON COLUMN material_pricing.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material_pricing.material_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material_pricing.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE material_pricing ADD CONSTRAINT FK_546B779CE308AC6F FOREIGN KEY (material_id) REFERENCES material (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_pricing ADD CONSTRAINT FK_546B779C70EE2FF6 FOREIGN KEY (circle_id) REFERENCES circle (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_user DROP CONSTRAINT fk_dc9cb5370ee2ff6');
        $this->addSql('ALTER TABLE circle_user DROP CONSTRAINT fk_dc9cb53a76ed395');
        $this->addSql('ALTER TABLE pricing DROP CONSTRAINT fk_e5f1ac33e308ac6f');
        $this->addSql('ALTER TABLE pricing_circle DROP CONSTRAINT fk_4e2424478864af73');
        $this->addSql('ALTER TABLE pricing_circle DROP CONSTRAINT fk_4e24244770ee2ff6');
        $this->addSql('DROP TABLE circle_user');
        $this->addSql('DROP TABLE pricing');
        $this->addSql('DROP TABLE pricing_circle');
        $this->addSql('ALTER TABLE circle DROP CONSTRAINT FK_D4B76579922726E9');
        $this->addSql('ALTER TABLE circle DROP CONSTRAINT FK_D4B76579F98F144A');
        $this->addSql('ALTER TABLE circle ADD CONSTRAINT FK_D4B76579922726E9 FOREIGN KEY (cover_id) REFERENCES circle_image (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle ADD CONSTRAINT FK_D4B76579F98F144A FOREIGN KEY (logo_id) REFERENCES circle_image (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_membership ALTER permission TYPE TEXT');
        $this->addSql('ALTER TABLE settings ADD currency_symbol VARCHAR(3) DEFAULT \'€\' NOT NULL');
        $this->addSql('ALTER TABLE material_pricing ADD enabled BOOLEAN DEFAULT true NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE circle_user (circle_id UUID NOT NULL, user_id UUID NOT NULL, PRIMARY KEY(circle_id, user_id))');
        $this->addSql('CREATE INDEX idx_dc9cb53a76ed395 ON circle_user (user_id)');
        $this->addSql('CREATE INDEX idx_dc9cb5370ee2ff6 ON circle_user (circle_id)');
        $this->addSql('COMMENT ON COLUMN circle_user.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_user.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE pricing (id UUID NOT NULL, material_id UUID NOT NULL, value DOUBLE PRECISION NOT NULL, period DOUBLE PRECISION NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_e5f1ac33e308ac6f ON pricing (material_id)');
        $this->addSql('COMMENT ON COLUMN pricing.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN pricing.material_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE pricing_circle (pricing_id UUID NOT NULL, circle_id UUID NOT NULL, PRIMARY KEY(pricing_id, circle_id))');
        $this->addSql('CREATE INDEX idx_4e24244770ee2ff6 ON pricing_circle (circle_id)');
        $this->addSql('CREATE INDEX idx_4e2424478864af73 ON pricing_circle (pricing_id)');
        $this->addSql('COMMENT ON COLUMN pricing_circle.pricing_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN pricing_circle.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE circle_user ADD CONSTRAINT fk_dc9cb5370ee2ff6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_user ADD CONSTRAINT fk_dc9cb53a76ed395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pricing ADD CONSTRAINT fk_e5f1ac33e308ac6f FOREIGN KEY (material_id) REFERENCES material (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pricing_circle ADD CONSTRAINT fk_4e2424478864af73 FOREIGN KEY (pricing_id) REFERENCES pricing (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pricing_circle ADD CONSTRAINT fk_4e24244770ee2ff6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_pricing DROP CONSTRAINT FK_546B779CE308AC6F');
        $this->addSql('ALTER TABLE material_pricing DROP CONSTRAINT FK_546B779C70EE2FF6');
        $this->addSql('DROP TABLE material_pricing');
        $this->addSql('ALTER TABLE circle_membership ALTER permission TYPE TEXT');
        $this->addSql('ALTER TABLE settings DROP currency_symbol');
        $this->addSql('ALTER TABLE circle DROP CONSTRAINT fk_d4b76579f98f144a');
        $this->addSql('ALTER TABLE circle DROP CONSTRAINT fk_d4b76579922726e9');
        $this->addSql('ALTER TABLE circle ADD CONSTRAINT fk_d4b76579f98f144a FOREIGN KEY (logo_id) REFERENCES circle_image (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle ADD CONSTRAINT fk_d4b76579922726e9 FOREIGN KEY (cover_id) REFERENCES circle_image (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_pricing DROP enabled');
    }
}
