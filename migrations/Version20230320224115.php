<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230320224115 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add circle\'s localization fields';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle ADD street_address VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE circle ADD country VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE circle ADD postal_code VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE circle_membership ADD status VARCHAR(15) DEFAULT \'pending\' NOT NULL');
        $this->addSql('ALTER TABLE circle_membership ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE circle_membership ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle DROP street_address');
        $this->addSql('ALTER TABLE circle DROP country');
        $this->addSql('ALTER TABLE circle DROP postal_code');
        $this->addSql('ALTER TABLE circle_membership DROP status');
        $this->addSql('ALTER TABLE circle_membership DROP created_at');
        $this->addSql('ALTER TABLE circle_membership DROP updated_at');
    }
}
