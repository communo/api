<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230323200023 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER table circle_membership drop constraint circle_membership_pkey');
        $this->addSql('ALTER TABLE circle_membership ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE circle ADD latitude VARCHAR(30) DEFAULT NULL');
        $this->addSql('ALTER TABLE circle ADD longitude VARCHAR(30) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX circle_membership_pkey');
        $this->addSql('ALTER TABLE circle_membership ADD PRIMARY KEY (circle_id, user_id)');
        $this->addSql('ALTER TABLE circle DROP latitude');
        $this->addSql('ALTER TABLE circle DROP longitude');
    }
}
