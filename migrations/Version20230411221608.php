<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230411221608 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'rename streetAddress to address + add blameable on circle_image';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle RENAME COLUMN street_address TO address');
        $this->addSql('ALTER TABLE circle_image ADD created_by_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE circle_image ADD updated_by_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN circle_image.created_by_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_image.updated_by_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE circle_image ADD CONSTRAINT FK_38DFA738B03A8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_image ADD CONSTRAINT FK_38DFA738896DBBDE FOREIGN KEY (updated_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_38DFA738B03A8386 ON circle_image (created_by_id)');
        $this->addSql('CREATE INDEX IDX_38DFA738896DBBDE ON circle_image (updated_by_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle RENAME COLUMN address TO street_address');
        $this->addSql('ALTER TABLE circle_image DROP CONSTRAINT FK_38DFA738B03A8386');
        $this->addSql('ALTER TABLE circle_image DROP CONSTRAINT FK_38DFA738896DBBDE');
        $this->addSql('DROP INDEX IDX_38DFA738B03A8386');
        $this->addSql('DROP INDEX IDX_38DFA738896DBBDE');
        $this->addSql('ALTER TABLE circle_image DROP created_by_id');
        $this->addSql('ALTER TABLE circle_image DROP updated_by_id');
    }
}
