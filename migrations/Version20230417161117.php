<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230417161117 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add circle indexable boolean';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle ADD indexable BOOLEAN DEFAULT true NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle DROP indexable');
    }
}
