<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230420161809 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add explicitLocationLabel';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle ADD explicit_location_label VARCHAR(255) DEFAULT NULL');
        $this->addSql('UPDATE circle SET explicit_location_label = city');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle DROP explicit_location_label');
    }
}
