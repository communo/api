<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230512135537 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle_membership ALTER permission TYPE TEXT');
        $this->addSql('ALTER TABLE "user" RENAME COLUMN phone_number_object TO phone_number');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE circle_membership ALTER permission TYPE TEXT');
        $this->addSql('ALTER TABLE "user" RENAME COLUMN phone_number TO phone_number_object');
    }
}
