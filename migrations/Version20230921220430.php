<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230921220430 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add invitation and contact';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE contact (id UUID NOT NULL, message TEXT NOT NULL, email VARCHAR(180) NOT NULL, name VARCHAR(255) DEFAULT NULL, newsletter_opt_in BOOLEAN DEFAULT false NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4C62E638E7927C74 ON contact (email)');
        $this->addSql('COMMENT ON COLUMN contact.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE invitation (id UUID NOT NULL, email VARCHAR(180) NOT NULL, postal_code VARCHAR(10) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F11D61A2E7927C74 ON invitation (email)');
        $this->addSql('COMMENT ON COLUMN invitation.id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE circle_membership ALTER permission TYPE TEXT');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE invitation');
        $this->addSql('ALTER TABLE circle_membership ALTER permission TYPE TEXT');
    }
}
