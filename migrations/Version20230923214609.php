<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230923214609 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'remove unique on contact\'s email';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX uniq_4c62e638e7927c74');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE UNIQUE INDEX uniq_4c62e638e7927c74 ON contact (email)');
    }
}
