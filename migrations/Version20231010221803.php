<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231010221803 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add invited and date columns to invitation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle_membership ALTER permission TYPE TEXT');
        $this->addSql('ALTER TABLE invitation ADD invited BOOLEAN DEFAULT false NOT NULL');
        $this->addSql('ALTER TABLE invitation ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE invitation ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle_membership ALTER permission TYPE TEXT');
        $this->addSql('ALTER TABLE invitation DROP invited');
        $this->addSql('ALTER TABLE invitation DROP created_at');
        $this->addSql('ALTER TABLE invitation DROP updated_at');
    }
}
