<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231011202116 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add postal code user\'s property';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "user" ADD postal_code VARCHAR(6) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "user" DROP postal_code');
    }
}
