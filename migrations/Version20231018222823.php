<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231018222823 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'remove enabled field from material_pricing and change permission type to text in circle_membership';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DELETE FROM material_pricing WHERE enabled = false');
        $this->addSql('ALTER TABLE material_pricing DROP enabled');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material_pricing ADD enabled BOOLEAN DEFAULT true NOT NULL');
    }
}
