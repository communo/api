<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231021155410 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add parameters to message for automatic message be translated in user language';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE message ADD parameters JSON NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE message DROP parameters');
    }
}
