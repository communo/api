<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231024065047 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add message property on circle_membership to record initial message';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle_membership ADD message TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle_membership DROP message');
    }
}
