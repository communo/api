<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231031201150 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'link user to invitation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE invitation ADD user_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN invitation.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE invitation ADD CONSTRAINT FK_F11D61A2A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F11D61A2A76ED395 ON invitation (user_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE invitation DROP CONSTRAINT FK_F11D61A2A76ED395');
        $this->addSql('DROP INDEX IDX_F11D61A2A76ED395');
        $this->addSql('ALTER TABLE invitation DROP user_id');
    }
}
