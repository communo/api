<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231103075148 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add material\'s status';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material ADD status VARCHAR(15) DEFAULT \'published\' NOT NULL');
        $this->addSql('ALTER TABLE material ALTER status SET DEFAULT \'draft\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material DROP status');
    }
}
