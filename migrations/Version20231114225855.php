<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231114225855 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add material ownership system';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE material_ownership (id UUID NOT NULL, material_id UUID NOT NULL, dtype VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E18D3EB2E308AC6F ON material_ownership (material_id)');
        $this->addSql('COMMENT ON COLUMN material_ownership.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material_ownership.material_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE material_ownership_circle (id UUID NOT NULL, circle_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C60D51D570EE2FF6 ON material_ownership_circle (circle_id)');
        $this->addSql('COMMENT ON COLUMN material_ownership_circle.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material_ownership_circle.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE material_ownership_user (id UUID NOT NULL, user_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8C4A108FA76ED395 ON material_ownership_user (user_id)');
        $this->addSql('COMMENT ON COLUMN material_ownership_user.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material_ownership_user.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE material_ownership ADD CONSTRAINT FK_E18D3EB2E308AC6F FOREIGN KEY (material_id) REFERENCES material (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_ownership_circle ADD CONSTRAINT FK_C60D51D570EE2FF6 FOREIGN KEY (circle_id) REFERENCES circle (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_ownership_circle ADD CONSTRAINT FK_C60D51D5BF396750 FOREIGN KEY (id) REFERENCES material_ownership (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_ownership_user ADD CONSTRAINT FK_8C4A108FA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_ownership_user ADD CONSTRAINT FK_8C4A108FBF396750 FOREIGN KEY (id) REFERENCES material_ownership (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material_ownership DROP CONSTRAINT FK_E18D3EB2E308AC6F');
        $this->addSql('ALTER TABLE material_ownership_circle DROP CONSTRAINT FK_C60D51D570EE2FF6');
        $this->addSql('ALTER TABLE material_ownership_circle DROP CONSTRAINT FK_C60D51D5BF396750');
        $this->addSql('ALTER TABLE material_ownership_user DROP CONSTRAINT FK_8C4A108FA76ED395');
        $this->addSql('ALTER TABLE material_ownership_user DROP CONSTRAINT FK_8C4A108FBF396750');
        $this->addSql('DROP TABLE material_ownership');
        $this->addSql('DROP TABLE material_ownership_circle');
        $this->addSql('DROP TABLE material_ownership_user');
    }
}
