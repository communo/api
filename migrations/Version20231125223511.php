<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231125223511 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'rename owner to main_owner';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material DROP CONSTRAINT fk_7cbe75957e3c61f9');
        $this->addSql('DROP INDEX idx_7cbe75957e3c61f9');
        $this->addSql('ALTER TABLE material RENAME COLUMN owner_id TO main_owner_id');
        $this->addSql('ALTER TABLE material ADD CONSTRAINT FK_7CBE7595A71E708C FOREIGN KEY (main_owner_id) REFERENCES "user" (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_7CBE7595A71E708C ON material (main_owner_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material DROP CONSTRAINT FK_7CBE7595A71E708C');
        $this->addSql('DROP INDEX IDX_7CBE7595A71E708C');
        $this->addSql('ALTER TABLE material RENAME COLUMN main_owner_id TO owner_id');
        $this->addSql('ALTER TABLE material ADD CONSTRAINT fk_7cbe75957e3c61f9 FOREIGN KEY (owner_id) REFERENCES "user" (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_7cbe75957e3c61f9 ON material (owner_id)');
    }
}
