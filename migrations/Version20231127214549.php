<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Ramsey\Uuid\Uuid;

final class Version20231127214549 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material ADD main_ownership_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN material.main_ownership_id IS \'(DC2Type:uuid)\'');

        $this->connection->beginTransaction();
        try {
            $materials = $this->connection->fetchAllAssociative('SELECT id, main_owner_id FROM material');

            foreach ($materials as $material) {
                $materialOwnershipId = Uuid::uuid4()->toString();
                $this->addSql('INSERT INTO material_ownership (id, material_id, dtype) VALUES (:id, :material_id, :dtype)', [
                    'id' => $materialOwnershipId,
                    'material_id' => $material['id'],
                    'dtype' => 'user',
                ]);
                $this->addSql('INSERT INTO material_ownership_user (id, user_id) VALUES (:id, :user_id)', [
                    'id' => $materialOwnershipId,
                    'user_id' => $material['main_owner_id'],
                ]);
                $this->addSql('UPDATE material SET main_ownership_id = :materialOwnershipId WHERE id = :id', [
                    'id' => $material['id'],
                    'materialOwnershipId' => $materialOwnershipId,
                ]);
            }

            // Commit Transaction
            $this->connection->commit();
        } catch (\Exception $e) {
            // Rollback Transaction on Exception
            $this->connection->rollBack();
            throw $e;
        }
        $this->addSql('ALTER TABLE material ADD CONSTRAINT FK_7CBE759587274F19 FOREIGN KEY (main_ownership_id) REFERENCES material_ownership (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7CBE759587274F19 ON material (main_ownership_id)');
        $this->addSql('ALTER TABLE material_ownership_circle DROP CONSTRAINT fk_c60d51d570ee2ff6');
        $this->addSql('ALTER TABLE material_ownership_circle ADD CONSTRAINT FK_E96A9D9970EE2FF6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_ownership_user DROP CONSTRAINT fk_8c4a108fa76ed395');
        $this->addSql('ALTER TABLE material_ownership_user ADD CONSTRAINT FK_E0F3A08EA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material DROP CONSTRAINT FK_7CBE759587274F19');
        $this->addSql('DROP INDEX UNIQ_7CBE759587274F19');
        $this->addSql('ALTER TABLE material DROP main_ownership_id');
        $this->addSql('ALTER TABLE material_ownership_user DROP CONSTRAINT FK_E0F3A08EA76ED395');
        $this->addSql('ALTER TABLE material_ownership_user ADD CONSTRAINT fk_8c4a108fa76ed395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_ownership_circle DROP CONSTRAINT FK_E96A9D9970EE2FF6');
        $this->addSql('ALTER TABLE material_ownership_circle ADD CONSTRAINT fk_c60d51d570ee2ff6 FOREIGN KEY (circle_id) REFERENCES circle (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
