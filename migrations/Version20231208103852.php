<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231208103852 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'rename duplicate to fork';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material ADD forked_from_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN material.forked_from_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE material ADD CONSTRAINT FK_7CBE7595684644A7 FOREIGN KEY (forked_from_id) REFERENCES material (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_7CBE7595684644A7 ON material (forked_from_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material DROP CONSTRAINT FK_7CBE7595684644A7');
        $this->addSql('DROP INDEX IDX_7CBE7595684644A7');
        $this->addSql('ALTER TABLE material DROP forked_from_id');
    }
}
