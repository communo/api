<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231213200439 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'drop not null on material';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material_ownership ALTER material_id DROP NOT NULL');
        $this->addSql('ALTER INDEX idx_e18d3eb2e308ac6f RENAME TO IDX_200EF674E308AC6F');
        $this->addSql('ALTER INDEX idx_c60d51d570ee2ff6 RENAME TO IDX_E96A9D9970EE2FF6');
        $this->addSql('ALTER INDEX idx_8c4a108fa76ed395 RENAME TO IDX_E0F3A08EA76ED395');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER INDEX idx_e96a9d9970ee2ff6 RENAME TO idx_c60d51d570ee2ff6');
        $this->addSql('ALTER INDEX idx_e0f3a08ea76ed395 RENAME TO idx_8c4a108fa76ed395');
        $this->addSql('ALTER TABLE material_ownership ALTER material_id SET NOT NULL');
        $this->addSql('ALTER INDEX idx_200ef674e308ac6f RENAME TO idx_e18d3eb2e308ac6f');
    }
}
