<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240112215701 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add comment on booking';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material_booking ADD comment TEXT');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material_booking DROP comment');
    }
}
