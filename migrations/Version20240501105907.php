<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240501105907 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'allow null message in contact table (newsletter case)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE contact ALTER message DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE contact ALTER message SET NOT NULL');
    }
}
