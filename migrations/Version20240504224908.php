<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240504224908 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add material search alert entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE material_search_alert (id UUID NOT NULL, user_id UUID NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2DB01128A76ED395 ON material_search_alert (user_id)');
        $this->addSql('COMMENT ON COLUMN material_search_alert.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material_search_alert.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE material_search_alert ADD CONSTRAINT FK_2DB01128A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material_search_alert DROP CONSTRAINT FK_2DB01128A76ED395');
        $this->addSql('DROP TABLE material_search_alert');
    }
}
