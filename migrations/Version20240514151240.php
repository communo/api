<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240514151240 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'allow nullable category while it is not used';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material ALTER category_id DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material ALTER category_id SET NOT NULL');
    }
}
