<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240515213118 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add plan + feature + subscription + translations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE subscription (id UUID NOT NULL, plan_id UUID DEFAULT NULL, circle_id UUID DEFAULT NULL, price DOUBLE PRECISION DEFAULT NULL, description TEXT DEFAULT NULL, status TEXT NOT NULL, start_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A3C664D3E899029B ON subscription (plan_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A3C664D370EE2FF6 ON subscription (circle_id)');
        $this->addSql('COMMENT ON COLUMN subscription.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN subscription.plan_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN subscription.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE subscription_plan (id UUID NOT NULL, price DOUBLE PRECISION NOT NULL, slug VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN subscription_plan.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE subscription_plan_feature (id UUID NOT NULL, plan_id UUID DEFAULT NULL, work_in_progress BOOLEAN DEFAULT false NOT NULL, new BOOLEAN DEFAULT false NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CF3E21F9E899029B ON subscription_plan_feature (plan_id)');
        $this->addSql('COMMENT ON COLUMN subscription_plan_feature.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN subscription_plan_feature.plan_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE subscription_plan_feature_translation (id UUID NOT NULL, translatable_id UUID DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, description TEXT DEFAULT NULL, locale VARCHAR(5) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_65A60D112C2AC5D3 ON subscription_plan_feature_translation (translatable_id)');
        $this->addSql('CREATE UNIQUE INDEX subscription_plan_feature_translation_unique_translation ON subscription_plan_feature_translation (translatable_id, locale)');
        $this->addSql('COMMENT ON COLUMN subscription_plan_feature_translation.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN subscription_plan_feature_translation.translatable_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE subscription_plan_translation (id UUID NOT NULL, translatable_id UUID DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, description TEXT DEFAULT NULL, locale VARCHAR(5) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3390A8802C2AC5D3 ON subscription_plan_translation (translatable_id)');
        $this->addSql('CREATE UNIQUE INDEX subscription_plan_translation_unique_translation ON subscription_plan_translation (translatable_id, locale)');
        $this->addSql('COMMENT ON COLUMN subscription_plan_translation.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN subscription_plan_translation.translatable_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE subscription ADD CONSTRAINT FK_A3C664D3E899029B FOREIGN KEY (plan_id) REFERENCES subscription_plan (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subscription ADD CONSTRAINT FK_A3C664D370EE2FF6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subscription_plan_feature ADD CONSTRAINT FK_CF3E21F9E899029B FOREIGN KEY (plan_id) REFERENCES subscription_plan (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subscription_plan_feature_translation ADD CONSTRAINT FK_65A60D112C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES subscription_plan_feature (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subscription_plan_translation ADD CONSTRAINT FK_3390A8802C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES subscription_plan (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE subscription DROP CONSTRAINT FK_A3C664D3E899029B');
        $this->addSql('ALTER TABLE subscription DROP CONSTRAINT FK_A3C664D370EE2FF6');
        $this->addSql('ALTER TABLE subscription_plan_feature DROP CONSTRAINT FK_CF3E21F9E899029B');
        $this->addSql('ALTER TABLE subscription_plan_feature_translation DROP CONSTRAINT FK_65A60D112C2AC5D3');
        $this->addSql('ALTER TABLE subscription_plan_translation DROP CONSTRAINT FK_3390A8802C2AC5D3');
        $this->addSql('DROP TABLE subscription');
        $this->addSql('DROP TABLE subscription_plan');
        $this->addSql('DROP TABLE subscription_plan_feature');
        $this->addSql('DROP TABLE subscription_plan_feature_translation');
        $this->addSql('DROP TABLE subscription_plan_translation');
    }
}
