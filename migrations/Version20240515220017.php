<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240515220017 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add button and condition to subscription_plan_translation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE subscription_plan_translation ADD button TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE subscription_plan_translation ADD condition TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE subscription_plan_translation DROP button');
        $this->addSql('ALTER TABLE subscription_plan_translation DROP condition');
    }
}
