<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240529142835 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add price to material';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material ADD price DOUBLE PRECISION DEFAULT \'0\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material DROP price');
    }
}
