<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240604130930 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add share scope';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material ADD share_scope VARCHAR(15) DEFAULT \'public\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material DROP share_scope');
    }
}
