<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240618132015 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'user\s users list';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE user_list (id UUID NOT NULL, owner_id UUID DEFAULT NULL, slug VARCHAR(100) NOT NULL, name VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3E49B4D17E3C61F9 ON user_list (owner_id)');
        $this->addSql('COMMENT ON COLUMN user_list.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN user_list.owner_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE user_list_user (user_list_id UUID NOT NULL, user_id UUID NOT NULL, PRIMARY KEY(user_list_id, user_id))');
        $this->addSql('CREATE INDEX IDX_C8BD844465A30881 ON user_list_user (user_list_id)');
        $this->addSql('CREATE INDEX IDX_C8BD8444A76ED395 ON user_list_user (user_id)');
        $this->addSql('COMMENT ON COLUMN user_list_user.user_list_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN user_list_user.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE user_list ADD CONSTRAINT FK_3E49B4D17E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_list_user ADD CONSTRAINT FK_C8BD844465A30881 FOREIGN KEY (user_list_id) REFERENCES user_list (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_list_user ADD CONSTRAINT FK_C8BD8444A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_ownership DROP CONSTRAINT fk_e18d3eb2e308ac6f');
        $this->addSql('ALTER TABLE material_ownership ADD CONSTRAINT FK_200EF674E308AC6F FOREIGN KEY (material_id) REFERENCES material (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_ownership_circle DROP CONSTRAINT fk_c60d51d5bf396750');
        $this->addSql('ALTER TABLE material_ownership_circle ADD CONSTRAINT FK_E96A9D99BF396750 FOREIGN KEY (id) REFERENCES material_ownership (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_ownership_user DROP CONSTRAINT fk_8c4a108fbf396750');
        $this->addSql('ALTER TABLE material_ownership_user ADD CONSTRAINT FK_E0F3A08EBF396750 FOREIGN KEY (id) REFERENCES material_ownership (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user_list DROP CONSTRAINT FK_3E49B4D17E3C61F9');
        $this->addSql('ALTER TABLE user_list_user DROP CONSTRAINT FK_C8BD844465A30881');
        $this->addSql('ALTER TABLE user_list_user DROP CONSTRAINT FK_C8BD8444A76ED395');
        $this->addSql('DROP TABLE user_list');
        $this->addSql('DROP TABLE user_list_user');
        $this->addSql('ALTER TABLE material_ownership_user DROP CONSTRAINT FK_E0F3A08EBF396750');
        $this->addSql('ALTER TABLE material_ownership_user ADD CONSTRAINT fk_8c4a108fbf396750 FOREIGN KEY (id) REFERENCES material_ownership (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_ownership_circle DROP CONSTRAINT FK_E96A9D99BF396750');
        $this->addSql('ALTER TABLE material_ownership_circle ADD CONSTRAINT fk_c60d51d5bf396750 FOREIGN KEY (id) REFERENCES material_ownership (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_ownership DROP CONSTRAINT FK_200EF674E308AC6F');
        $this->addSql('ALTER TABLE material_ownership ADD CONSTRAINT fk_e18d3eb2e308ac6f FOREIGN KEY (material_id) REFERENCES material (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
