<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240819214506 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'link userLists to material';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE material_user_list (material_id UUID NOT NULL, user_list_id UUID NOT NULL, PRIMARY KEY(material_id, user_list_id))');
        $this->addSql('CREATE INDEX IDX_6D7AE31E308AC6F ON material_user_list (material_id)');
        $this->addSql('CREATE INDEX IDX_6D7AE3165A30881 ON material_user_list (user_list_id)');
        $this->addSql('COMMENT ON COLUMN material_user_list.material_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN material_user_list.user_list_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE material_user_list ADD CONSTRAINT FK_6D7AE31E308AC6F FOREIGN KEY (material_id) REFERENCES material (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE material_user_list ADD CONSTRAINT FK_6D7AE3165A30881 FOREIGN KEY (user_list_id) REFERENCES user_list (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_list ALTER name SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material_user_list DROP CONSTRAINT FK_6D7AE31E308AC6F');
        $this->addSql('ALTER TABLE material_user_list DROP CONSTRAINT FK_6D7AE3165A30881');
        $this->addSql('DROP TABLE material_user_list');
        $this->addSql('ALTER TABLE user_list ALTER name DROP NOT NULL');
    }
}
