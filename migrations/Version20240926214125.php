<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240926214125 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material ADD location geometry(POINTZ, 4326) DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD location geometry(POINTZ, 4326) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material DROP location');
        $this->addSql('ALTER TABLE "user" DROP location');
    }
}
