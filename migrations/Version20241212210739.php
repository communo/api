<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241212210739 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material_pricing DROP CONSTRAINT FK_546B779C70EE2FF6');
        $this->addSql('ALTER TABLE material_pricing ADD CONSTRAINT FK_546B779C70EE2FF6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material_pricing DROP CONSTRAINT fk_546b779c70ee2ff6');
        $this->addSql('ALTER TABLE material_pricing ADD CONSTRAINT fk_546b779c70ee2ff6 FOREIGN KEY (circle_id) REFERENCES circle (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
