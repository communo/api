<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241216194445 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle DROP CONSTRAINT fk_d4b76579922726e9');
        $this->addSql('ALTER TABLE circle DROP CONSTRAINT fk_d4b76579f98f144a');
        $this->addSql('CREATE TABLE circle_cover (id UUID NOT NULL, created_by_id UUID DEFAULT NULL, updated_by_id UUID DEFAULT NULL, circle_id UUID DEFAULT NULL, image_name VARCHAR(255) NOT NULL, mime VARCHAR(255) DEFAULT NULL, original_name VARCHAR(255) DEFAULT NULL, dimensions TEXT NOT NULL, size INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_70EA25A2B03A8386 ON circle_cover (created_by_id)');
        $this->addSql('CREATE INDEX IDX_70EA25A2896DBBDE ON circle_cover (updated_by_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_70EA25A270EE2FF6 ON circle_cover (circle_id)');
        $this->addSql('COMMENT ON COLUMN circle_cover.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_cover.created_by_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_cover.updated_by_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_cover.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_cover.dimensions IS \'(DC2Type:simple_array)\'');
        $this->addSql('CREATE TABLE circle_logo (id UUID NOT NULL, created_by_id UUID DEFAULT NULL, updated_by_id UUID DEFAULT NULL, circle_id UUID DEFAULT NULL, image_name VARCHAR(255) NOT NULL, mime VARCHAR(255) DEFAULT NULL, original_name VARCHAR(255) DEFAULT NULL, dimensions TEXT NOT NULL, size INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_64D48709B03A8386 ON circle_logo (created_by_id)');
        $this->addSql('CREATE INDEX IDX_64D48709896DBBDE ON circle_logo (updated_by_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_64D4870970EE2FF6 ON circle_logo (circle_id)');
        $this->addSql('COMMENT ON COLUMN circle_logo.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_logo.created_by_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_logo.updated_by_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_logo.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_logo.dimensions IS \'(DC2Type:simple_array)\'');
        $this->addSql('ALTER TABLE circle_cover ADD CONSTRAINT FK_70EA25A2B03A8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_cover ADD CONSTRAINT FK_70EA25A2896DBBDE FOREIGN KEY (updated_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_cover ADD CONSTRAINT FK_70EA25A270EE2FF6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_logo ADD CONSTRAINT FK_64D48709B03A8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_logo ADD CONSTRAINT FK_64D48709896DBBDE FOREIGN KEY (updated_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_logo ADD CONSTRAINT FK_64D4870970EE2FF6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_image DROP CONSTRAINT fk_38dfa738b03a8386');
        $this->addSql('ALTER TABLE circle_image DROP CONSTRAINT fk_38dfa738896dbbde');
        $this->addSql('DROP TABLE circle_image');
        $this->addSql('DROP INDEX uniq_d4b76579922726e9');
        $this->addSql('DROP INDEX uniq_d4b76579f98f144a');
        $this->addSql('ALTER TABLE circle DROP logo_id');
        $this->addSql('ALTER TABLE circle DROP cover_id');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE circle_image (id UUID NOT NULL, created_by_id UUID DEFAULT NULL, updated_by_id UUID DEFAULT NULL, image_name VARCHAR(255) NOT NULL, size INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, mime VARCHAR(255) DEFAULT NULL, original_name VARCHAR(255) DEFAULT NULL, dimensions TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_38dfa738896dbbde ON circle_image (updated_by_id)');
        $this->addSql('CREATE INDEX idx_38dfa738b03a8386 ON circle_image (created_by_id)');
        $this->addSql('COMMENT ON COLUMN circle_image.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_image.created_by_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_image.updated_by_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_image.dimensions IS \'(DC2Type:simple_array)\'');
        $this->addSql('ALTER TABLE circle_image ADD CONSTRAINT fk_38dfa738b03a8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_image ADD CONSTRAINT fk_38dfa738896dbbde FOREIGN KEY (updated_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_cover DROP CONSTRAINT FK_70EA25A2B03A8386');
        $this->addSql('ALTER TABLE circle_cover DROP CONSTRAINT FK_70EA25A2896DBBDE');
        $this->addSql('ALTER TABLE circle_cover DROP CONSTRAINT FK_70EA25A270EE2FF6');
        $this->addSql('ALTER TABLE circle_logo DROP CONSTRAINT FK_64D48709B03A8386');
        $this->addSql('ALTER TABLE circle_logo DROP CONSTRAINT FK_64D48709896DBBDE');
        $this->addSql('ALTER TABLE circle_logo DROP CONSTRAINT FK_64D4870970EE2FF6');
        $this->addSql('DROP TABLE circle_cover');
        $this->addSql('DROP TABLE circle_logo');
        $this->addSql('ALTER TABLE circle ADD logo_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE circle ADD cover_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN circle.logo_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle.cover_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE circle ADD CONSTRAINT fk_d4b76579922726e9 FOREIGN KEY (cover_id) REFERENCES circle_image (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle ADD CONSTRAINT fk_d4b76579f98f144a FOREIGN KEY (logo_id) REFERENCES circle_image (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_d4b76579922726e9 ON circle (cover_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_d4b76579f98f144a ON circle (logo_id)');
    }
}
