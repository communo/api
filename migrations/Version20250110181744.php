<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250110181744 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material_booking ALTER user_id DROP NOT NULL');
        $this->addSql('ALTER TABLE material_search_alert ALTER user_id DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE material_booking ALTER user_id SET NOT NULL');
        $this->addSql('ALTER TABLE material_search_alert ALTER user_id SET NOT NULL');
    }
}
