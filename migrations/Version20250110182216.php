<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250110182216 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle_cover DROP CONSTRAINT FK_70EA25A2B03A8386');
        $this->addSql('ALTER TABLE circle_cover ADD CONSTRAINT FK_70EA25A2B03A8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_logo DROP CONSTRAINT FK_64D48709B03A8386');
        $this->addSql('ALTER TABLE circle_logo ADD CONSTRAINT FK_64D48709B03A8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD bio TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD birth_date DATE DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle_cover DROP CONSTRAINT fk_70ea25a2b03a8386');
        $this->addSql('ALTER TABLE circle_cover ADD CONSTRAINT fk_70ea25a2b03a8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle_logo DROP CONSTRAINT fk_64d48709b03a8386');
        $this->addSql('ALTER TABLE circle_logo ADD CONSTRAINT fk_64d48709b03a8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" DROP bio');
        $this->addSql('ALTER TABLE "user" DROP birth_date');
    }
}
