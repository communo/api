<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250123214641 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add user settings';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE settings ADD theme_mode VARCHAR(255) DEFAULT \'SYSTEM\' NOT NULL');
        $this->addSql('ALTER TABLE settings ADD use_abbreviated_name BOOLEAN DEFAULT true NOT NULL');
        $this->addSql('ALTER TABLE settings ADD prefered_communication_mode VARCHAR(255) DEFAULT \'EMAIL\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE settings DROP theme_mode');
        $this->addSql('ALTER TABLE settings DROP use_abbreviated_name');
        $this->addSql('ALTER TABLE settings DROP prefered_communication_mode');
    }
}
