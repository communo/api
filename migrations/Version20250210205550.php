<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250210205550 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add location to circle';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle ADD location geometry(POINTZ, 4326) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle DROP location');
    }
}
