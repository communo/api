<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250210212929 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'move circle logo and cover relation side';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle ADD logo_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE circle ADD cover_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN circle.logo_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle.cover_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE circle ADD CONSTRAINT FK_D4B76579F98F144A FOREIGN KEY (logo_id) REFERENCES circle_logo (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE circle ADD CONSTRAINT FK_D4B76579922726E9 FOREIGN KEY (cover_id) REFERENCES circle_cover (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4B76579F98F144A ON circle (logo_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4B76579922726E9 ON circle (cover_id)');
        $this->addSql('ALTER TABLE circle_cover DROP CONSTRAINT fk_70ea25a270ee2ff6');
        $this->addSql('DROP INDEX uniq_70ea25a270ee2ff6');
        $this->addSql('ALTER TABLE circle_cover DROP circle_id');
        $this->addSql('ALTER TABLE circle_logo DROP CONSTRAINT fk_64d4870970ee2ff6');
        $this->addSql('DROP INDEX uniq_64d4870970ee2ff6');
        $this->addSql('ALTER TABLE circle_logo DROP circle_id');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle DROP CONSTRAINT FK_D4B76579F98F144A');
        $this->addSql('ALTER TABLE circle DROP CONSTRAINT FK_D4B76579922726E9');
        $this->addSql('DROP INDEX UNIQ_D4B76579F98F144A');
        $this->addSql('DROP INDEX UNIQ_D4B76579922726E9');
        $this->addSql('ALTER TABLE circle DROP logo_id');
        $this->addSql('ALTER TABLE circle DROP cover_id');
        $this->addSql('ALTER TABLE circle_cover ADD circle_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN circle_cover.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE circle_cover ADD CONSTRAINT fk_70ea25a270ee2ff6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_70ea25a270ee2ff6 ON circle_cover (circle_id)');
        $this->addSql('ALTER TABLE circle_logo ADD circle_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN circle_logo.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE circle_logo ADD CONSTRAINT fk_64d4870970ee2ff6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_64d4870970ee2ff6 ON circle_logo (circle_id)');
    }
}
