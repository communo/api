<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250225204012 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE invitation ADD invited_by_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE invitation ADD circle_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN invitation.invited_by_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN invitation.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE invitation ADD CONSTRAINT FK_F11D61A2A7B4A7E3 FOREIGN KEY (invited_by_id) REFERENCES "user" (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE invitation ADD CONSTRAINT FK_F11D61A270EE2FF6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F11D61A2A7B4A7E3 ON invitation (invited_by_id)');
        $this->addSql('CREATE INDEX IDX_F11D61A270EE2FF6 ON invitation (circle_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE invitation DROP CONSTRAINT FK_F11D61A2A7B4A7E3');
        $this->addSql('ALTER TABLE invitation DROP CONSTRAINT FK_F11D61A270EE2FF6');
        $this->addSql('DROP INDEX IDX_F11D61A2A7B4A7E3');
        $this->addSql('DROP INDEX IDX_F11D61A270EE2FF6');
        $this->addSql('ALTER TABLE invitation DROP invited_by_id');
        $this->addSql('ALTER TABLE invitation DROP circle_id');
    }
}
