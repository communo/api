<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250226215418 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add circle invitation link';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE circle_invitation_link (id UUID NOT NULL, circle_id UUID NOT NULL, number_of_uses INT DEFAULT 0 NOT NULL, enabled BOOLEAN DEFAULT true NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D1E38D8070EE2FF6 ON circle_invitation_link (circle_id)');
        $this->addSql('COMMENT ON COLUMN circle_invitation_link.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN circle_invitation_link.circle_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE circle_invitation_link ADD CONSTRAINT FK_D1E38D8070EE2FF6 FOREIGN KEY (circle_id) REFERENCES circle (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE circle_invitation_link DROP CONSTRAINT FK_D1E38D8070EE2FF6');
        $this->addSql('DROP TABLE circle_invitation_link');
    }
}
