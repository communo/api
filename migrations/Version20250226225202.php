<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250226225202 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'change invitation unique index';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX uniq_f11d61a2e7927c74');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE UNIQUE INDEX uniq_f11d61a2e7927c74 ON invitation (email)');
    }
}
