<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250227212608 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add firstname and lastname to invitation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE invitation ADD firstname VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE invitation ADD lastname VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE invitation DROP firstname');
        $this->addSql('ALTER TABLE invitation DROP lastname');
    }
}
