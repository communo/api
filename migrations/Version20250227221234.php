<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250227221234 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'increase postal code length';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "user" ALTER postal_code TYPE VARCHAR(15)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "user" ALTER postal_code TYPE VARCHAR(6)');
    }
}
