<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250303140500 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'initialize material location with main owner\s location';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'UPDATE material AS m
            SET location = u.location
            FROM material_ownership AS mo
            JOIN material_ownership_user AS mou ON mou.id = mo.id
            JOIN "user" AS u ON u.id = mou.user_id
            WHERE m.id = mo.material_id;'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE material AS m SET location = NULL');
    }
}
