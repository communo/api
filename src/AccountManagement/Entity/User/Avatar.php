<?php

declare(strict_types=1);

namespace App\AccountManagement\Entity\User;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Mutation;
use App\Shared\Entity\Image;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[Vich\Uploadable]
#[ApiResource(
    types: ['https://schema.org/ImageObject'],
    operations: [],
    graphQlOperations: [
        new Mutation(
            args: [
                'imageFile' => [
                    'type' => 'Upload!',
                    'description' => 'The file to upload',
                ],
            ],
            security: "is_granted('edit', object.user)",
            name: 'update'
        ),
        new Mutation(
            args: [
                'imageFile' => [
                    'type' => 'Upload!',
                    'description' => 'The file to upload',
                ],
            ],
            security: 'is_authenticated()',
            name: 'create'
        ),
    ]
)]
#[ORM\Entity]
class Avatar extends Image
{
    #[Vich\UploadableField(
        mapping: 'user_avatar',
        fileNameProperty: 'imageName',
        size: 'size',
        mimeType: 'mime',
        originalName: 'originalName',
        dimensions: 'dimensions'
    )]
    #[Assert\NotNull(groups: ['avatar_create'])]
    protected ?File $imageFile = null;

    #[ORM\OneToOne(mappedBy: 'avatar', targetEntity: User::class)]
    private User $user;

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        if ($user->getAvatar() !== $this) {
            $user->setAvatar($this);
        }
        $this->user = $user;

        return $this;
    }
}
