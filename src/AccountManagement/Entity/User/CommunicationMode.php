<?php

declare(strict_types=1);

namespace App\AccountManagement\Entity\User;

enum CommunicationMode: string
{
    case EMAIL = 'EMAIL';
    case PHONE = 'PHONE';
}
