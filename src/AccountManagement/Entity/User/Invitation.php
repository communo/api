<?php

declare(strict_types=1);

namespace App\AccountManagement\Entity\User;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\DeleteMutation;
use ApiPlatform\Metadata\GraphQl\Mutation;
use ApiPlatform\Metadata\GraphQl\Query;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use App\AccountManagement\Infra\ApiPlatform\State\HandleInvitationProcessor;
use App\AccountManagement\Infra\ApiPlatform\State\SendCircleInvitationProcessor;
use App\AccountManagement\Infra\ApiPlatform\State\SendInvitationProcessor;
use App\AccountManagement\Repository\User\InvitationRepository;
use App\Circles\Entity\Circle;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: InvitationRepository::class)]
#[ApiResource(
    operations: [],
    graphQlOperations: [
        new Mutation(
            args: [
                'email' => ['type' => 'String!'],
                'postalCode' => ['type' => 'String!'],
            ],
            name: 'create'
        ),
        new Mutation(
            args: [
                'email' => ['type' => 'String!'],
                'firstname' => ['type' => 'String!'],
                'lastname' => ['type' => 'String!'],
                'message' => ['type' => 'String!'],
                'inviteLink' => ['type' => 'String!'],
                'circle' => ['type' => 'String'],
            ],
            security: 'is_authenticated()',
            name: 'sendCircle',
            processor: SendCircleInvitationProcessor::class
        ),
        new Mutation(
            args: [
                'id' => ['type' => 'ID!'],
            ],
            security: "is_granted('ROLE_ADMIN')",
            name: 'send',
            processor: SendInvitationProcessor::class
        ),
        new Mutation(
            args: [
                'id' => ['type' => 'ID!'],
                'decision' => ['type' => 'String!'],
            ],
            security: "is_granted('handleInvitation', object)",
            name: 'handle',
            processor: HandleInvitationProcessor::class
        ),
        new DeleteMutation(
            security: "is_granted('ROLE_ADMIN')",
            name: 'delete'
        ),
        new Query(),
        new QueryCollection(
            security: "is_granted('ROLE_ADMIN')"
        ),
    ]
)]
#[ApiFilter(filterClass: BooleanFilter::class, properties: [
    'invited' => 'partial',
])]
#[ApiFilter(filterClass: SearchFilter::class, properties: [
    'email' => 'partial',
    'postalCode' => 'partial',
    'circle.id' => 'id',
])]
#[UniqueEntity(fields: ['email', 'circle'], message: 'alreadyInvited')]
class Invitation implements TimestampableInterface
{
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    private ?Uuid $id = null;

    #[ORM\Column(type: 'string', length: 180)]
    #[Assert\Email]
    private string $email;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $firstname;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $lastname;

    #[ORM\Column(type: 'string', length: 10, nullable: true)]
    private ?string $postalCode;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    private bool $invited = false;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private User $invitedBy;

    #[ORM\ManyToOne(targetEntity: Circle::class, inversedBy: 'invitations')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private Circle $circle;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: true, onDelete: 'CASCADE')]
    private ?User $user = null;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    public function isInvited(): bool
    {
        return $this->invited;
    }

    public function setInvited(bool $invited): void
    {
        $this->invited = $invited;
    }

    public function getInvitedBy(): ?User
    {
        return $this->invitedBy;
    }

    public function setInvitedBy(?User $invitedBy): void
    {
        $this->invitedBy = $invitedBy;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    public function getCircle(): ?Circle
    {
        return $this->circle;
    }

    public function setCircle(?Circle $circle): void
    {
        $this->circle = $circle;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): void
    {
        $this->firstname = $firstname;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): void
    {
        $this->lastname = $lastname;
    }
}
