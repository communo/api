<?php

declare(strict_types=1);

namespace App\AccountManagement\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshToken as GesdinetRefreshToken;

#[ORM\Entity]
#[ORM\Table(name: 'refresh_token')]
class RefreshToken extends GesdinetRefreshToken
{
}
