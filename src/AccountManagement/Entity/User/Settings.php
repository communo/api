<?php

declare(strict_types=1);

namespace App\AccountManagement\Entity\User;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Mutation;
use App\AccountManagement\Repository\User\SettingsRepository;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [],
    graphQlOperations: [
        new Mutation(
            security: "is_granted('edit', object.user)",
            name: 'update'
        ),
    ]
)]
#[ORM\Entity(repositoryClass: SettingsRepository::class)]
class Settings implements TimestampableInterface
{
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    #[Groups(['read', 'user:update'])]
    private ?Uuid $id = null;

    #[ORM\Column(type: 'string', length: 5)]
    #[Groups(['read', 'register', 'user:update'])]
    private string $language;

    #[ORM\Column(enumType: ThemeMode::class, options: ['default' => ThemeMode::SYSTEM])]
    #[Groups(['read', 'register', 'user:update'])]
    private ?ThemeMode $themeMode = ThemeMode::SYSTEM;

    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    #[Groups(['read', 'register', 'user:update'])]
    private bool $useAbbreviatedName = true;

    #[ORM\Column(type: 'string', length: 3, options: ['default' => '€'])]
    #[Groups(['read', 'register', 'user:update'])]
    private ?string $currencySymbol = '€';

    #[ORM\Column(enumType: CommunicationMode::class, options: ['default' => CommunicationMode::EMAIL])]
    #[Groups(['read', 'register', 'user:update'])]
    private CommunicationMode $preferedCommunicationMode = CommunicationMode::EMAIL;

    #[ORM\OneToOne(mappedBy: 'settings', targetEntity: User::class, cascade: ['persist', 'remove'])]
    private User $user;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        // unset the owning side of the relation if necessary
        if ($user === null && $this->user !== null) {
            $this->user->setSettings(null);
        }
        // set the owning side of the relation if necessary
        if ($user !== null && $user->getSettings() !== $this) {
            $user->setSettings($this);
        }
        $this->user = $user;

        return $this;
    }

    public function getCurrencySymbol(): string
    {
        return $this->currencySymbol;
    }

    public function setCurrencySymbol(?string $currencySymbol): void
    {
        $this->currencySymbol = $currencySymbol;
    }

    public function getThemeMode(): ThemeMode
    {
        return $this->themeMode;
    }

    public function setThemeMode(ThemeMode $themeMode): self
    {
        $this->themeMode = $themeMode;

        return $this;
    }

    public function isUseAbbreviatedName(): bool
    {
        return $this->useAbbreviatedName;
    }

    public function setUseAbbreviatedName(bool $useAbbreviatedName): Settings
    {
        $this->useAbbreviatedName = $useAbbreviatedName;

        return $this;
    }

    public function getPreferedCommunicationMode(): CommunicationMode
    {
        return $this->preferedCommunicationMode;
    }

    public function setPreferedCommunicationMode(CommunicationMode $preferedCommunicationMode): Settings
    {
        $this->preferedCommunicationMode = $preferedCommunicationMode;

        return $this;
    }
}
