<?php

declare(strict_types=1);

namespace App\AccountManagement\Entity\User;

enum ThemeMode: string
{
    case SYSTEM = 'SYSTEM';
    case LIGHT = 'LIGHT';
    case DARK = 'DARK';
}
