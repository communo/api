<?php

declare(strict_types=1);

namespace App\AccountManagement\Entity\User;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GraphQl\DeleteMutation;
use ApiPlatform\Metadata\GraphQl\Mutation;
use ApiPlatform\Metadata\GraphQl\Query;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use App\AccountManagement\Infra\ApiPlatform\Output\IsGrantedUserOutput;
use App\AccountManagement\Infra\ApiPlatform\Resolver\ChangePasswordResolver;
use App\AccountManagement\Infra\ApiPlatform\Resolver\ConfirmUserResolver;
use App\AccountManagement\Infra\ApiPlatform\Resolver\LoginUserResolver;
use App\AccountManagement\Infra\ApiPlatform\Resolver\LogoutUserResolver;
use App\AccountManagement\Infra\ApiPlatform\Resolver\UserCollectionResolver;
use App\AccountManagement\Infra\ApiPlatform\Resolver\UserResolver;
use App\AccountManagement\Infra\ApiPlatform\State\IsGrantedUserProvider;
use App\AccountManagement\Infra\ApiPlatform\State\UpdateUserProcessor;
use App\AccountManagement\Infra\ApiPlatform\State\UserRegisterWizardProcessor;
use App\AccountManagement\Repository\UserRepository;
use App\Chat\Entity\Discussion\Discussion;
use App\Circles\Entity\CircleMembership;
use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use App\Pooling\Domain\Model\Material\Booking\Rating;
use App\Pooling\Domain\Model\Material\Material;
use App\Pooling\Domain\Model\Material\MaterialSearchAlert;
use App\Pooling\Domain\Model\Material\Ownership\UserMaterialOwnership;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Jsor\Doctrine\PostGIS\Types\PostGISType;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use libphonenumber\PhoneNumber;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [
        new Get(
            security: 'is_granted(false)'
        ),
        new Get(
            uriTemplate: '/users/isGranted/{slug}',
            options: ['attribute' => 'string', 'resource' => 'string'],
            description: 'Check if a user is allowed to do such an action',
            output: IsGrantedUserOutput::class,
            provider: IsGrantedUserProvider::class
        ),
    ],
    graphQlOperations: [
        new Query(
            resolver: UserResolver::class,
            name: 'item_query'
        ),
        new QueryCollection(
            resolver: UserCollectionResolver::class,
            name: 'collection_query'
        ),
        new Mutation(
            extraArgs: [
                'countryCode' => ['type' => 'Int'],
                'nationalNumber' => ['type' => 'String'],
                'latitude' => ['type' => 'String'],
                'longitude' => ['type' => 'String'],
                'avatar' => ['type' => 'Upload'],
                'invitationId' => ['type' => 'ID'],
            ],
            denormalizationContext: ['groups' => ['register']],
            name: 'create',
            processor: UserRegisterWizardProcessor::class,
        ),
        new Mutation(
            extraArgs: [
                'countryCode' => ['type' => 'Int'],
                'nationalNumber' => ['type' => 'String'],
                'latitude' => ['type' => 'String'],
                'longitude' => ['type' => 'String'],
                'avatar' => ['type' => 'Upload'],
                'removeAvatar' => ['type' => 'Boolean'],
            ],
            denormalizationContext: ['groups' => ['user:update']],
            security: "is_granted('edit', object)",
            name: 'update',
            processor: UpdateUserProcessor::class
        ),
        new Mutation(
            resolver: ConfirmUserResolver::class,
            args: [
                'confirmationToken' => ['type' => 'String!'],
                'code' => ['type' => 'String!'],
            ],
            deprecationReason: 'we do not confirm email anymore',
            name: 'confirm'
        ),
        new Mutation(
            resolver: ChangePasswordResolver::class,
            args: [
                'id' => ['type' => 'ID!'],
                'currentPassword' => ['type' => 'String!'],
                'newPassword' => ['type' => 'String!'],
            ],
            security: "is_granted('edit', object)",
            name: 'changePassword'
        ),
        new DeleteMutation(
            security: "is_granted('edit', object)",
            name: 'delete'
        ),
        new Mutation(
            resolver: LogoutUserResolver::class,
            args: [
                'id' => ['type' => 'ID!'],
            ],
            security: "is_granted('edit', object)",
            name: 'logout'
        ),
        new Mutation(
            resolver: LoginUserResolver::class,
            args: [
                'username' => ['type' => 'String!'],
                'password' => ['type' => 'String!'],
            ],
            name: 'login'
        ),
    ]
)]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity('email')]
#[ApiFilter(filterClass: SearchFilter::class, properties: [
    'firstname' => 'ipartial',
    'lastname' => 'ipartial',
    'postalCode' => 'ipartial',
    'email' => 'exact',
])]
#[ApiFilter(filterClass: BooleanFilter::class, properties: [
    'confirmed' => 'partial',
])]
class User implements UserInterface, PasswordAuthenticatedUserInterface, TimestampableInterface
{
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    public ?string $phoneNumber;

    #[ApiProperty(identifier: true)]
    #[ORM\Column(type: 'string', length: 100)]
    #[Gedmo\Slug(fields: ['firstname', 'lastname'])]
    #[Groups(['read'])]
    private string $slug;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    #[ApiProperty(identifier: false)]
    #[Groups(['read', 'user:update'])]
    private ?Uuid $id = null;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    #[Assert\Email]
    #[Groups(['read', 'user:update', 'register'])]
    private string $email;

    /**
     * @var string[]
     */
    #[ORM\Column(type: 'json')]
    #[ApiProperty(writable: false)]
    #[Groups(['read'])]
    private array $roles = [];

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read', 'user:update', 'register'])]
    private string $password;

    /**
     * @var Collection<int, Material>
     *                                Be careful, this relation lists only material where user is mainOwner (not co-ownership)
     */
    #[ORM\OneToMany(mappedBy: 'mainOwner', targetEntity: Material::class)]
    #[Groups(['read'])]
    private Collection $materials;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['read', 'user:update', 'register'])]
    private string $firstname;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read', 'user:update', 'register'])]
    private string $lastname;

    #[ORM\OneToOne(inversedBy: 'user', targetEntity: Avatar::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups(['read'])]
    private ?Avatar $avatar = null;

    #[ORM\Column(name: 'phone_number', type: 'phone_number', nullable: true)]
    #[AssertPhoneNumber]
    #[Groups(['read', 'user:update', 'register'])]
    private ?PhoneNumber $phoneNumberObject;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['read', 'user:update', 'register'])]
    private ?string $city;

    #[ORM\Column(type: 'string', length: 15, nullable: true)]
    #[Groups(['read', 'user:update', 'register'])]
    private ?string $postalCode;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['read', 'user:update', 'register'])]
    private ?string $country;

    #[ORM\Column(type: PostGISType::GEOMETRY, options: ['geometry_type' => 'POINTZ', 'srid' => 4326], nullable: true)]
    #[Groups(['read', 'user:update', 'register'])]
    #[ApiProperty(description: 'The geographical location of the user as a POINT (longitude, latitude).')]
    private ?string $location = null;

    #[ORM\Column(type: 'date', nullable: true)]
    #[Groups(['read', 'user:update', 'register'])]
    private ?\DateTimeInterface $birthDate;

    /**
     * @var Collection<int, MaterialBooking>
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: MaterialBooking::class, orphanRemoval: true)]
    #[Groups(['read'])]
    private Collection $materialBookings;

    /**
     * @var Collection<int, CircleMembership>
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: CircleMembership::class, orphanRemoval: true)]
    #[Groups(['read'])]
    private Collection $circleMemberships;

    /**
     * @var Collection<int, Rating>
     */
    #[ORM\OneToMany(mappedBy: 'author', targetEntity: Rating::class)]
    #[Groups(['read'])]
    private Collection $ratingsSent;

    /**
     * @var Collection<int, Rating>
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Rating::class, orphanRemoval: true)]
    #[Groups(['read'])]
    private Collection $ratings;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read', 'user:update', 'register'])]
    private ?string $gender;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['read', 'user:update', 'register'])]
    private ?string $bio;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[ApiProperty(writable: false)]
    #[Groups(['read'])]
    private bool $confirmed = false;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['read'])]
    private ?string $confirmationToken;

    #[ORM\OneToOne(inversedBy: 'user', targetEntity: Settings::class, cascade: ['persist', 'remove'], fetch: 'LAZY')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[Groups(['read', 'user:update', 'register'])]
    private Settings $settings;
    private string $token;
    private ?float $tokenExpiresAt;
    private ?string $refreshToken;

    /**
     * @var Collection<int, Discussion>
     */
    #[ORM\ManyToMany(targetEntity: Discussion::class, mappedBy: 'users')]
    #[Groups(['read'])]
    private Collection $discussions;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read', 'user:update', 'register'])]
    private ?string $streetAddress;

    /**
     * @var Collection<int, UserMaterialOwnership>
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserMaterialOwnership::class, orphanRemoval: true)]
    private Collection $userOwnerships;

    /**
     * @var Collection<int, MaterialSearchAlert>
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: MaterialSearchAlert::class, orphanRemoval: true)]
    private Collection $materialSearchAlerts;

    /**
     * @var Collection<int, UserList>
     *                                The list of users I made (for example, my friends) to easily allow them to access my materials
     */
    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: UserList::class, orphanRemoval: true)]
    private Collection $userLists;

    public function __construct()
    {
        $this->materials = new ArrayCollection();
        $this->materialBookings = new ArrayCollection();
        $this->ratingsSent = new ArrayCollection();
        $this->ratings = new ArrayCollection();
        $this->discussions = new ArrayCollection();
        $this->settings = new Settings();
        $this->userOwnerships = new ArrayCollection();
        $this->materialSearchAlerts = new ArrayCollection();
        $this->userLists = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param string[] $roles
     */
    public function setRoles(?array $roles = []): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
    }

    #[Groups(['read'])]
    public function getFullname($byPassSettings = false): string
    {
        if (!$byPassSettings && $this->settings->isUseAbbreviatedName()) {
            $abbreviatedLastname = mb_substr($this->lastname, 0, 1);

            return sprintf('%s %s.', $this->firstname, $abbreviatedLastname);
        }

        return sprintf('%s %s', $this->firstname, $this->lastname);
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return array{countryCode: int|null, nationalNumber: string|null}|null
     */
    #[Groups(['read'])]
    public function getPhoneNumber(): ?array
    {
        if ($this->phoneNumberObject) {
            $prefix = '';
            for ($i = 0; $i < $this->getPhoneNumberObject()?->getNumberOfLeadingZeros(); $i++) {
                $prefix .= '0';
            }
            $nationalNumber = $prefix.$this->getPhoneNumberObject()?->getNationalNumber();

            return [
                'countryCode' => $this->phoneNumberObject->getCountryCode(),
                'nationalNumber' => $nationalNumber,
            ];
        }

        return null;
    }

    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getPhoneNumberObject(): ?PhoneNumber
    {
        return $this->phoneNumberObject;
    }

    public function setPhoneNumberObject(?PhoneNumber $phoneNumberObject): self
    {
        $this->phoneNumberObject = $phoneNumberObject;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection<int, MaterialBooking>
     */
    public function getMaterialBookings(): Collection
    {
        return $this->materialBookings;
    }

    public function addMaterialBooking(MaterialBooking $materialBooking): self
    {
        if (!$this->materialBookings->contains($materialBooking)) {
            $this->materialBookings[] = $materialBooking;
            $materialBooking->setUser($this);
        }

        return $this;
    }

    public function removeMaterialBooking(MaterialBooking $materialBooking): self
    {
        if ($this->materialBookings->removeElement($materialBooking)) {
            // set the owning side to null (unless already changed)
            if ($materialBooking->getUser() === $this) {
                $materialBooking->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Rating>
     */
    public function getRatingsSent(): Collection
    {
        return $this->ratingsSent;
    }

    public function addRatingsSent(Rating $ratingsSent): self
    {
        if (!$this->ratingsSent->contains($ratingsSent)) {
            $this->ratingsSent[] = $ratingsSent;
            $ratingsSent->setAuthor($this);
        }

        return $this;
    }

    public function removeRatingsSent(Rating $ratingsSent): self
    {
        if ($this->ratingsSent->removeElement($ratingsSent)) {
            // set the owning side to null (unless already changed)
            if ($ratingsSent->getAuthor() === $this) {
                $ratingsSent->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Rating>
     */
    public function getRatings(): Collection
    {
        return $this->ratings;
    }

    public function getAverageRatingScore(): ?float
    {
        $ratingsNumber = $this->ratings->count();
        if ($ratingsNumber === 0) {
            return null;
        }
        $scores = $this->ratings->map(function ($rating) {
            return $rating->getValue();
        })->getValues();

        return array_sum($scores) / $ratingsNumber;
    }

    public function addRating(Rating $rating): self
    {
        if (!$this->ratings->contains($rating)) {
            $this->ratings[] = $rating;
            $rating->setUser($this);
        }

        return $this;
    }

    public function removeRating(Rating $rating): self
    {
        if ($this->ratings->removeElement($rating)) {
            // set the owning side to null (unless already changed)
            if ($rating->getUser() === $this) {
                $rating->setUser(null);
            }
        }

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getBio(): ?string
    {
        return $this->bio;
    }

    public function setBio(?string $bio): self
    {
        $this->bio = $bio;

        return $this;
    }

    public function getAvatar(): ?Avatar
    {
        return $this->avatar;
    }

    public function setAvatar(?Avatar $avatar): User
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getConfirmed(): ?bool
    {
        return $this->confirmed;
    }

    public function setConfirmed(bool $confirmed): self
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function getSettings(): ?Settings
    {
        return $this->settings;
    }

    public function setSettings(?Settings $settings): self
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * //CAUTION: removing comments will break the token generation @TODO needs to be fixed.
     *
     * @return ?string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    public function setRefreshToken(?string $refreshToken): void
    {
        $this->refreshToken = $refreshToken;
    }

    public function getTokenExpiresAt(): ?float
    {
        return $this->tokenExpiresAt;
    }

    public function setTokenExpiresAt(?float $tokenExpiresAt): void
    {
        $this->tokenExpiresAt = $tokenExpiresAt;
    }

    /**
     * @return Collection<int, Discussion>
     */
    public function getDiscussions(): Collection
    {
        return $this->discussions;
    }

    public function addDiscussion(Discussion $discussion): self
    {
        if (!$this->discussions->contains($discussion)) {
            $this->discussions[] = $discussion;
            $discussion->addUser($this);
        }

        return $this;
    }

    public function removeDiscussion(Discussion $discussion): self
    {
        if ($this->discussions->removeElement($discussion)) {
            $discussion->removeUser($this);
        }

        return $this;
    }

    public function getStreetAddress(): ?string
    {
        return $this->streetAddress;
    }

    public function setStreetAddress(string $streetAddress): self
    {
        $this->streetAddress = $streetAddress;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return Collection<int, CircleMembership>
     */
    public function getCircleMemberships(): Collection
    {
        return $this->circleMemberships;
    }

    /**
     * @param Collection<int, CircleMembership> $circleMemberships
     */
    public function setCircleMemberships(Collection $circleMemberships): void
    {
        $this->circleMemberships = $circleMemberships;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return Collection<int, UserMaterialOwnership>
     */
    public function getUserOwnerships(): Collection
    {
        return $this->userOwnerships;
    }

    public function addUserOwnership(UserMaterialOwnership $userOwnership): static
    {
        if (!$this->userOwnerships->contains($userOwnership)) {
            $this->userOwnerships->add($userOwnership);
            $userOwnership->setUser($this);
        }

        return $this;
    }

    public function removeUserOwnership(UserMaterialOwnership $userOwnership): static
    {
        if ($this->userOwnerships->removeElement($userOwnership)) {
            // set the owning side to null (unless already changed)
            if ($userOwnership->getUser() === $this) {
                $userOwnership->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, MaterialSearchAlert>
     */
    public function getMaterialSearchAlerts(): Collection
    {
        return $this->materialSearchAlerts;
    }

    public function addMaterialSearchAlert(MaterialSearchAlert $materialSearchAlert): static
    {
        if (!$this->materialSearchAlerts->contains($materialSearchAlert)) {
            $this->materialSearchAlerts->add($materialSearchAlert);
            $materialSearchAlert->setUser($this);
        }

        return $this;
    }

    public function removeMaterialSearchAlert(MaterialSearchAlert $materialSearchAlert): static
    {
        if ($this->materialSearchAlerts->removeElement($materialSearchAlert)) {
            // set the owning side to null (unless already changed)
            if ($materialSearchAlert->getUser() === $this) {
                $materialSearchAlert->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, UserList>
     */
    public function getUserLists(): Collection
    {
        return $this->userLists;
    }

    public function addUserList(UserList $userList): static
    {
        if (!$this->userLists->contains($userList)) {
            $this->userLists->add($userList);
            $userList->setOwner($this);
        }

        return $this;
    }

    public function removeUserList(UserList $userList): static
    {
        if ($this->userLists->removeElement($userList)) {
            if ($userList->getOwner() === $this) {
                $userList->setOwner(null);
            }
        }

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function setCoordinates(float $latitude, float $longitude): self
    {
        $this->location = sprintf('SRID=4326;POINT(%f %f)', $longitude, $latitude);

        return $this;
    }

    public function getLatitude(): ?float
    {
        if ($this->location) {
            return (float) explode(' ', substr($this->location, strpos($this->location, '(') + 1, -1))[1];
        }

        return null;
    }

    public function getLongitude(): ?float
    {
        if ($this->location) {
            return (float) explode(' ', substr($this->location, strpos($this->location, '(') + 1, -1))[0];
        }

        return null;
    }

    public function getBirthDate(): \DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTimeInterface $birthDate): void
    {
        $this->birthDate = $birthDate;
    }
}
