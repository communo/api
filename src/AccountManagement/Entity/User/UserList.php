<?php

declare(strict_types=1);

namespace App\AccountManagement\Entity\User;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\DeleteMutation;
use ApiPlatform\Metadata\GraphQl\Mutation;
use ApiPlatform\Metadata\GraphQl\Query;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use App\AccountManagement\Infra\ApiPlatform\Filter\UserList\OnlyMinesFilter;
use App\AccountManagement\Infra\ApiPlatform\Resolver\AddUserToUserListResolver;
use App\AccountManagement\Infra\ApiPlatform\Resolver\CreateUserListResolver;
use App\AccountManagement\Infra\ApiPlatform\Resolver\RemoveUserFromUserListResolver;
use App\AccountManagement\Security\Voter\UserVoter;
use App\Pooling\Domain\Model\Material\Material;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [],
    normalizationContext: ['groups' => ['read']],
    denormalizationContext: ['groups' => ['write']],
    graphQlOperations: [
        new Query(
            security: "is_granted('".UserVoter::EDIT."', object.getOwner())",
            name: 'item_query',
        ),
        new QueryCollection(
            filters: [OnlyMinesFilter::class],
            name: 'collection_query',
        ),
        new Mutation(
            resolver: CreateUserListResolver::class,
            security: "is_granted('IS_AUTHENTICATED')",
            name: 'create'
        ),
        new Mutation(
            security: "is_granted('".UserVoter::EDIT."', object.getOwner())",
            name: 'update',
        ),
        new Mutation(
            resolver: RemoveUserFromUserListResolver::class,
            args: [
                'id' => ['type' => 'ID!'],
                'userId' => ['type' => 'ID!'],
            ],
            security: "is_granted('".UserVoter::EDIT."', object.getOwner())",
            name: 'removeUserFrom',
        ),
        new Mutation(
            resolver: AddUserToUserListResolver::class,
            args: [
                'id' => ['type' => 'ID!'],
                'userEmail' => ['type' => 'String!'],
            ],
            security: "is_granted('".UserVoter::EDIT."', object.getOwner())",
            name: 'addUserTo',
        ),
        new DeleteMutation(
            security: "is_granted('".UserVoter::EDIT."', object.getOwner())",
            name: 'delete',
        ),
    ]
)]
#[ORM\Entity]
#[ORM\Table(name: 'user_list')]
class UserList implements TimestampableInterface
{
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    #[ApiProperty(identifier: true)]
    #[ORM\Column(type: 'string', length: 100)]
    #[Gedmo\Slug(fields: ['name'])]
    #[Groups(['read'])]
    private string $slug;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    #[ApiProperty(identifier: false)]
    #[Groups(['read'])]
    private ?Uuid $id = null;

    #[ORM\ManyToMany(targetEntity: User::class)]
    #[Groups(['read', 'write'])]
    private Collection $users;

    #[ORM\ManyToOne(inversedBy: 'userLists', targetEntity: User::class)]
    #[Groups(['read'])]
    private User $owner;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read', 'write'])]
    private string $name;

    /**
     * @var Collection<int, Material>
     */
    #[ORM\ManyToMany(targetEntity: Material::class, mappedBy: 'userLists')]
    private Collection $materials;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->materials = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function setUsers(Collection $users): void
    {
        $this->users = $users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }

    public function getOwner(): User
    {
        return $this->owner;
    }

    public function setOwner(User $owner): void
    {
        $this->owner = $owner;
    }

    public function getMaterials(): Collection
    {
        return $this->materials;
    }

    public function setMaterials(Collection $materials): void
    {
        $this->materials = $materials;
    }
}
