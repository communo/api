<?php

declare(strict_types=1);

namespace App\AccountManagement\EventSubscriber\User;

use App\AccountManagement\Entity\User\User;
use CoopTilleuls\ForgotPasswordBundle\Event\CreateTokenEvent;
use CoopTilleuls\ForgotPasswordBundle\Event\UpdatePasswordEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final class ForgotPasswordEventSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly string $defaultSender,
        private readonly TranslatorInterface $translator,
        private readonly MailerInterface $mailer,
        private readonly string $clientBaseUrl,
        private readonly UserPasswordHasherInterface $passwordHasher,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CreateTokenEvent::class => 'onCreateToken',
            UpdatePasswordEvent::class => 'onUpdatePassword',
        ];
    }

    public function onCreateToken(CreateTokenEvent $event): void
    {
        $passwordToken = $event->getPasswordToken();
        /** @var User $user */
        $user = $passwordToken->getUser();
        $language = $user->getSettings()?->getLanguage();
        $this->mailer->send((new TemplatedEmail())
            ->from(new Address($this->defaultSender, 'Communo'))
            ->to(new Address($user->getEmail(), $user->getFirstname()))
            ->subject($this->translator->trans(
                id: 'email.forgotPassword.subject',
                parameters: [
                    'firstname' => $user->getFirstname(),
                ],
                locale: $language
            ))
            ->htmlTemplate('emails/password/forgotPassword.html.twig')
            ->textTemplate('emails/password/forgotPassword.txt.twig')
            ->context([
                'user' => $user,
                'actions' => [[
                    'text' => $this->translator->trans(
                        id: 'email.forgotPassword.button',
                        locale: $language
                    ),
                    'url' => sprintf('%s/forgot-password/%s', $this->clientBaseUrl, $passwordToken->getToken()),
                ]],
            ]));
    }

    public function onUpdatePassword(UpdatePasswordEvent $event): void
    {
        $passwordToken = $event->getPasswordToken();
        /** @var User $user */
        $user = $passwordToken->getUser();
        $user->setPassword($this->passwordHasher->hashPassword($user, $event->getPassword()));
        $this->entityManager->flush();
    }
}
