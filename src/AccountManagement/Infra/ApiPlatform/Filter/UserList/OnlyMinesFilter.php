<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\Filter\UserList;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\AccountManagement\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class OnlyMinesFilter extends AbstractFilter
{
    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private EntityManagerInterface $entityManager,
        ManagerRegistry $managerRegistry,
    ) {
        parent::__construct($managerRegistry);
    }

    /**
     * @param array<mixed> $context
     */
    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        /** @var User|null $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        $alias = $queryBuilder->getRootAliases()[0];

        $queryBuilder
            ->andWhere(sprintf('%s.owner = :user', $alias))
            ->setParameter('user', $user);
    }

    public function getDescription(string $resourceClass): array
    {
        return [];
    }

    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        // nothing to do
    }
}
