<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\Output;

class IsGrantedUserOutput
{
    public function __construct(
        public bool $isGranted,
    ) {
    }
}
