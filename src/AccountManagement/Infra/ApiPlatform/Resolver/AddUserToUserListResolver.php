<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\Resolver;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\AccountManagement\Entity\User\UserList;
use App\AccountManagement\Repository\UserRepository;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Contracts\Translation\TranslatorInterface;

final class AddUserToUserListResolver implements MutationResolverInterface
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    /**
     * @param UserList|null $item
     * @param array{
     *     args: array{
     *         input: array{
     *             userEmail: string
     *         }
     *     }
     * } $context
     */
    public function __invoke($item, array $context): ?UserList
    {
        $input = $context['args']['input'];
        assert($item instanceof UserList);
        $user = $this->userRepository->findOneByEmailOrPhone($input['userEmail']);
        if ($user === null) {
            throw new HttpException(404, $this->translator->trans('error.user_list.notFound'));
        }
        if ($item->getUsers()->contains($user)) {
            throw new HttpException(409, $this->translator->trans('error.user_list.already_in_list', ['{firstName}' => $user->getFirstname()]));
        }
        if ($user === $item->getOwner()) {
            throw new HttpException(422, $this->translator->trans('error.user_list.userIsOwner', ['{firstName}' => $user->getFirstname()]));
        }
        $item->addUser($user);

        return $item;
    }
}
