<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\Resolver;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\AccountManagement\Entity\User\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class ChangePasswordResolver implements MutationResolverInterface
{
    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher,
    ) {
    }

    /**
     * @param User|null $item
     * @param array{args: array{input: array{currentPassword: string, newPassword: string}}} $context
     */
    public function __invoke($item, array $context): ?User
    {
        if ($item instanceof User) {
            $input = $context['args']['input'];
            $hash = $this->passwordHasher->hashPassword($item, $input['newPassword']);
            if ($this->passwordHasher->isPasswordValid(
                user: $item,
                plainPassword: $input['currentPassword']
            )) {
                $item->setPassword($hash);
            }

            return $item;
        }

        return null;
    }
}
