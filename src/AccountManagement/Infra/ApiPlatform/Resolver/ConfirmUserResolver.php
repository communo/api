<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\Resolver;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\AccountManagement\Repository\UserRepository;
use App\AccountManagement\UseCase\GenerateUserAuthTokens;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Notifier\ChatterInterface;
use Symfony\Component\Notifier\Message\ChatMessage;

class ConfirmUserResolver implements MutationResolverInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private EntityManagerInterface $entityManager,
        private JWTEncoderInterface $jwtEncoder,
        private JWTTokenManagerInterface $tokenManager,
        private GenerateUserAuthTokens $generateUserAuthTokens,
        private Security $security,
        private ChatterInterface $chatter
    ) {
    }

    /**
     * @param array{args: array{input: array{confirmationToken: string, code: string}}} $context
     */
    public function __invoke(?object $item, array $context): object
    {
        $input = $context['args']['input'];
        $user = $this->userRepository->findOneBy(['confirmationToken' => $input['confirmationToken']]);
        $confirmationToken = $this->jwtEncoder->decode($input['confirmationToken']);
        $user->setConfirmed($confirmationToken['code'] === $input['code']);
        $this->security->login($user, 'json_login');
        $user->setToken($this->tokenManager->create($user));
        $refreshToken = ($this->generateUserAuthTokens)($user);

        $this->entityManager->persist($refreshToken);

        $chatMessage = new ChatMessage(sprintf('%s has joined in', $user->getFullname(byPassSettings: true)));
        $this->chatter->send($chatMessage);

        return $user;
    }
}
