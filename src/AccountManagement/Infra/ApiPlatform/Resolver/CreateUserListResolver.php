<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\Resolver;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\AccountManagement\Entity\User\UserList;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class CreateUserListResolver implements MutationResolverInterface
{
    public function __construct(
        private readonly TokenStorageInterface $tokenStorage,
    ) {
    }

    /**
     * @param UserList|null $item
     * @param array{
     *     args: array{
     *         input: array{
     *             message: string
     *         }
     *     }
     * } $context
     */
    public function __invoke($item, array $context): ?UserList
    {
        $input = $context['args']['input'];
        $user = $this->tokenStorage->getToken()?->getUser();
        if (!$user instanceof User) {
            return null;
        }
        $item->setOwner($user);

        return $item;
    }
}
