<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\Resolver;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\AccountManagement\Repository\UserRepository;
use App\AccountManagement\UseCase\GenerateUserAuthTokens;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Contracts\Translation\TranslatorInterface;

class LoginUserResolver implements MutationResolverInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private Security $security,
        private UserPasswordHasherInterface $userPasswordHasher,
        private JWTTokenManagerInterface $tokenManager,
        private EntityManagerInterface $entityManager,
        private GenerateUserAuthTokens $generateUserAuthTokens,
        private TranslatorInterface $translator
    ) {
    }

    /**
     * @param array{args: array{input: array{username: string, password: string}}} $context
     */
    public function __invoke(?object $item, array $context): object
    {
        $input = $context['args']['input'];
        $user = $this->userRepository->findOneBy([
            'email' => $input['username'],
        ]);
        if ($this->userPasswordHasher->isPasswordValid($user, $input['password'])) {
            $this->security->login($user, 'json_login');
            $user->setToken($this->tokenManager->create($user));
            $refreshToken = ($this->generateUserAuthTokens)($user);

            $this->entityManager->persist($refreshToken);
        } else {
            throw new BadCredentialsException($this->translator->trans('error.login.unauthorized'));
        }

        return $user;
    }
}
