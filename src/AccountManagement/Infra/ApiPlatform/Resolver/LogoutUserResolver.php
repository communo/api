<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\Resolver;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use Symfony\Bundle\SecurityBundle\Security;

class LogoutUserResolver implements MutationResolverInterface
{
    public function __construct(
        private Security $security
    ) {
    }

    /**
     * @param array{args: array<int, mixed>} $context
     */
    public function __invoke(?object $item, array $context): object
    {
        $this->security->logout(false);

        return $item;
    }
}
