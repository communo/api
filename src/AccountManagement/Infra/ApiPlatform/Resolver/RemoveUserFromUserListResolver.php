<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\Resolver;

use ApiPlatform\Api\IriConverterInterface;
use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\AccountManagement\Entity\User\UserList;

final class RemoveUserFromUserListResolver implements MutationResolverInterface
{
    public function __construct(
        private readonly IriConverterInterface $iriConverter,
    ) {
    }

    /**
     * @param UserList|null $item
     * @param array{
     *     args: array{
     *         input: array{
     *             userId: string
     *         }
     *     }
     * } $context
     */
    public function __invoke($item, array $context): ?UserList
    {
        $input = $context['args']['input'];
        assert($item instanceof UserList);
        $user = $this->iriConverter->getResourceFromIri($input['userId']);
        $item->removeUser($user);

        return $item;
    }
}
