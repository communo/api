<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\Resolver;

use ApiPlatform\GraphQl\Resolver\QueryCollectionResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\CirclePermissionType;

class UserCollectionResolver implements QueryCollectionResolverInterface
{
    /**
     * @param iterable<User> $collection
     * @param array{args: array<int, mixed>} $context
     *
     * @return iterable<User>
     */
    public function __invoke(iterable $collection, array $context): iterable
    {
        foreach ($collection as $user) {
            foreach ($user->getCircleMemberships() as $circleMembership) {
                $circleMembership->getCircle()->setActiveUserActions(
                    CirclePermissionType::getAvailableActions($circleMembership->getPermission())
                );
            }
        }

        return $collection;
    }
}
