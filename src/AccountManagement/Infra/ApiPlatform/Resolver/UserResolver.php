<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\Resolver;

use ApiPlatform\GraphQl\Resolver\QueryItemResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\CirclePermissionType;

class UserResolver implements QueryItemResolverInterface
{
    /**
     * @param array{args: array<int, mixed>} $context
     */
    public function __invoke(?object $item, array $context): object
    {
        if ($item instanceof User) {
            foreach ($item->getCircleMemberships() as $circleMembership) {
                $circleMembership->getCircle()->setActiveUserActions(
                    CirclePermissionType::getAvailableActions($circleMembership->getPermission())
                );
            }
        } else {
            $item = new User();
            $item->setFirstname('-');
            $item->setLastname('-');
            $item->setSlug('deleted-user');
        }

        return $item;
    }
}
