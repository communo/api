<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\AccountManagement\Entity\User\Invitation;
use App\AccountManagement\UseCase\AcceptCommunityInvitation;

/**
 * @implements ProcessorInterface<Invitation>
 */
class HandleInvitationProcessor implements ProcessorInterface
{
    /**
     * @param ProcessorInterface<Invitation> $statePersistProcessorDecorated
     */
    public function __construct(
        private ProcessorInterface $statePersistProcessorDecorated,
        private AcceptCommunityInvitation $acceptCommunityInvitation,
    ) {
    }

    /**
     * @param Invitation $data
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Invitation
    {
        assert($data instanceof Invitation);
        /** @var array{
         *     decision: string
         * } $input
         */
        $input = $context['info']->variableValues;

        if ($input['decision'] === 'accept') {
            ($this->acceptCommunityInvitation)(
                invitation: $data,
            );
        }

        return $this->statePersistProcessorDecorated->process($data, $operation, $uriVariables, $context);
    }
}
