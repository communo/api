<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\State;

use ApiPlatform\Api\IriConverterInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\AccountManagement\Infra\ApiPlatform\Output\IsGrantedUserOutput;
use App\AccountManagement\Repository\UserRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

final readonly class IsGrantedUserProvider implements ProviderInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private IriConverterInterface $iriConverter,
        private AccessDecisionManagerInterface $decisionManager,
        private Security $security
    ) {
    }

    /**
     * @param string[] $uriVariables
     * @param array{
     *     uri_variables: array{
     *       slug: string,
     *       attribute: string
     *     },
     *     filters: array{
     *       resource?: string
     *     }
     * } $context
     *
     * @return IsGrantedUserOutput|IsGrantedUserOutput[]|null
     *
     * @phpstan-ignore-next-line
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        $user = $this->userRepository->findOneBy(['slug' => $context['uri_variables']['slug']]);
        if (!$user || !$this->security->isGranted('checkPermission', $user)) {
            throw new AccessDeniedException('cannot checkPermission');
        }
        if (!$context['filters']['attribute']) {
            throw new \Exception('missing required attribute filter');
        }

        $subject = null;
        if (isset($context['filters']['resource'])) {
            $subject = $this->iriConverter->getResourceFromIri($context['filters']['resource']);
        }

        $token = new UsernamePasswordToken($user, 'none', $user->getRoles());

        return new IsGrantedUserOutput(
            isGranted: $this->decisionManager->decide(
                token: $token,
                attributes: [$context['filters']['attribute']],
                object: $subject
            )
        );
    }
}
