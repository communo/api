<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\AccountManagement\Entity\User\Invitation;
use App\AccountManagement\Entity\User\User;
use App\AccountManagement\UseCase\SendCircleInvitationMail;
use Symfony\Bundle\SecurityBundle\Security;

/**
 * @implements ProcessorInterface<Invitation>
 */
class SendCircleInvitationProcessor implements ProcessorInterface
{
    /**
     * @param ProcessorInterface<Invitation> $statePersistProcessorDecorated
     */
    public function __construct(
        private ProcessorInterface $statePersistProcessorDecorated,
        private SendCircleInvitationMail $sendCircleInvitationMail,
        private Security $security
    ) {
    }

    /**
     * @param Invitation $data
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Invitation
    {
        assert($data instanceof Invitation);
        /** @var User $user */
        $user = $this->security->getUser();
        if (!$user) {
            throw new \Exception('user not found');
        }
        /** @var array{
         *     email: string,
         *     firstname: string,
         *     lastname: string,
         *     message: string,
         *     inviteLink: string,
         * } $input
         */
        $input = $context['info']->variableValues['input'];

        ($this->sendCircleInvitationMail)(
            email: $data->getEmail(),
            message: $input['message'],
            firstname: $input['firstname'],
            lastname: $input['lastname'],
            inviteLink: $input['inviteLink'],
            invitedBy: $user->getFullname(byPassSettings: true),
            language: $user->getSettings()->getLanguage()
        );

        return $this->statePersistProcessorDecorated->process($data, $operation, $uriVariables, $context);
    }
}
