<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\AccountManagement\Entity\User\Invitation;
use App\AccountManagement\UseCase\SendInvitationMail;

/**
 * @implements ProcessorInterface<Invitation>
 */
class SendInvitationProcessor implements ProcessorInterface
{
    /**
     * @param ProcessorInterface<Invitation> $statePersistProcessorDecorated
     */
    public function __construct(
        private ProcessorInterface $statePersistProcessorDecorated,
        private SendInvitationMail $sendInvitationMail
    ) {
    }

    /**
     * @param Invitation $data
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Invitation
    {
        assert($data instanceof Invitation);

        ($this->sendInvitationMail)(
            token: $data->getId(),
            email: $data->getEmail(),
            postalCode: $data->getPostalCode()
        );

        return $this->statePersistProcessorDecorated->process($data, $operation, $uriVariables, $context);
    }
}
