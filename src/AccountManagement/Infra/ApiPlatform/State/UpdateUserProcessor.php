<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\AccountManagement\Entity\User\User;
use App\AccountManagement\UseCase\GenerateUserAuthTokens;
use App\AccountManagement\UseCase\UpdateUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @implements ProcessorInterface<User>
 */
class UpdateUserProcessor implements ProcessorInterface
{
    /**
     * @param ProcessorInterface<User> $statePersistProcessorDecorated
     */
    public function __construct(
        private EntityManagerInterface $entityManager,
        private GenerateUserAuthTokens $generateUserAuthTokens,
        private ProcessorInterface $statePersistProcessorDecorated,
        private UpdateUser $updateUser,
    ) {
    }

    /**
     * @param array<string, mixed> $uriVariables
     * @param array<string, mixed> $context
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): User
    {
        if (!$data instanceof User) {
            throw new NotFoundHttpException(sprintf('user %s not found (must be iri)', $data->id));
        }
        /** @var array{
         *     countryCode: int,
         *     nationalNumber: string,
         *     avatar: ?UploadedFile,
         *     removeAvatar: bool,
         *     latitude: string,
         *     longitude: string
         * } $input
         */
        $input = $context['info']->variableValues['input'];
        $user = ($this->updateUser)(
            user: $data,
            nationalNumber: $input['nationalNumber'] ?? null,
            countryCode: $input['countryCode'] ?? null,
            avatarFile: $input['avatar'] ?? null,
            removeAvatar: $input['removeAvatar'] ?? null,
            latitude: $input['latitude'] ?? null,
            longitude: $input['longitude'] ?? null,
        );
        $refreshToken = ($this->generateUserAuthTokens)($user);
        $this->entityManager->persist($refreshToken);
        $this->entityManager->flush();

        return $this->statePersistProcessorDecorated->process($user, $operation, $uriVariables, $context);
    }
}
