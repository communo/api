<?php

declare(strict_types=1);

namespace App\AccountManagement\Infra\ApiPlatform\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\AccountManagement\Entity\User\Avatar;
use App\AccountManagement\Entity\User\Invitation;
use App\AccountManagement\Entity\User\User;
use App\AccountManagement\UseCase\GenerateUserAuthTokens;
use App\AccountManagement\UseCase\RegisterUser;
use App\AccountManagement\UseCase\SendNewUserWelcomeMail;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @implements ProcessorInterface<User>
 */
class UserRegisterWizardProcessor implements ProcessorInterface
{
    public function __construct(
        #[Autowire(service: 'api_platform.doctrine.orm.state.persist_processor')]
        private ProcessorInterface $persistProcessor,
        private RegisterUser $registerUser,
        private EntityManagerInterface $entityManager,
        private JWTTokenManagerInterface $tokenManager,
        private GenerateUserAuthTokens $generateUserAuthTokens,
        private SendNewUserWelcomeMail $sendWelcomeMail,
    ) {
    }

    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): User
    {
        $this->entityManager->beginTransaction();

        try {
            /** @var array{
             *     countryCode: int,
             *     nationalNumber: string,
             *     avatar?: UploadedFile,
             *     latitude: string,
             *     longitude: string,
             *     invitationId?: string
             * } $input
             */
            $input = $context['info']->variableValues['input'];

            /** @var User $user */
            $user = $this->persistProcessor->process($data, $operation, $uriVariables, $context);
            $invitation = array_key_exists('invitationId', $input) ? $this->entityManager->getRepository(Invitation::class)
                ->find($input['invitationId']) : null;

            $user = ($this->registerUser)(
                user: $user,
                password: $data->getPassword(),
                nationalNumber: $input['nationalNumber'],
                countryCode: $input['countryCode'],
                avatarFile: $input['avatar'] ?? null,
                latitude: $input['latitude'],
                longitude: $input['longitude'],
                confirmCode: $code ?? null,
                invitation: $invitation,
            );

            $user->setToken($this->tokenManager->create($user));
            $refreshToken = ($this->generateUserAuthTokens)($user);
            $this->entityManager->persist($refreshToken);
            $this->entityManager->persist($user);

            $this->entityManager->flush();
            ($this->sendWelcomeMail)([
                'language' => $user->getSettings()?->getLanguage(),
                'firstname' => $user->getFirstname(),
                'email' => $user->getEmail(),
            ]);
            $this->entityManager->commit();

            return $user;
        } catch (\Throwable $e) {
            $this->entityManager->rollback();
            throw $e;
        }
    }
}
