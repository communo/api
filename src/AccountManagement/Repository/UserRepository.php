<?php

declare(strict_types=1);

namespace App\AccountManagement\Repository;

use App\AccountManagement\Entity\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[] findAll()
 * @method User[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @extends ServiceEntityRepository<User>
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findOneByEmailOrPhone(string $userEmailOrPhone, string $defaultPhoneCountryCode = '33'): ?User
    {
        $queryBuilder = $this->createQueryBuilder('u');
        $results = $queryBuilder->andWhere(
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->eq('u.email', ':input'),
                $queryBuilder->expr()->eq('u.phoneNumberObject', ':inputWithZeroLeadReplaced')
            )
        )
            ->setParameter('input', $userEmailOrPhone)
            ->setParameter('inputWithZeroLeadReplaced', preg_replace('/^0/', "+$defaultPhoneCountryCode", $userEmailOrPhone))
            ->getQuery()->setMaxResults(1)->execute();

        return $results[0] ?? null;
    }
}
