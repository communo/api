<?php

declare(strict_types=1);

namespace App\AccountManagement\Security\Voter;

use App\AccountManagement\Entity\User\Invitation;
use App\AccountManagement\Entity\User\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class InvitationVoter extends Voter
{
    public const HANDLE_INVITATION = 'handleInvitation';

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::HANDLE_INVITATION], true)
            && $subject instanceof Invitation;
    }

    /**
     * @param Invitation $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();

        return match ($attribute) {
            self::HANDLE_INVITATION => $user->getEmail() === $subject->getEmail(),
        };
    }
}
