<?php

declare(strict_types=1);

namespace App\AccountManagement\Security\Voter;

use App\AccountManagement\Entity\User\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter
{
    public const CHECK_PERMISSION = 'checkPermission';
    public const EDIT = 'edit';

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::EDIT, self::CHECK_PERMISSION], true)
            && $subject instanceof User;
    }

    /**
     * @param User $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::EDIT, self::CHECK_PERMISSION => $token->getUser() === $subject,
        };
    }
}
