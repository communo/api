<?php

declare(strict_types=1);

namespace App\AccountManagement\UseCase;

use App\AccountManagement\Entity\User\Invitation;
use App\Circles\Entity\CircleMembership;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;

class AcceptCommunityInvitation
{
    public function __construct(
        private Security $security,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function __invoke(Invitation $invitation): void
    {
        $invitation->setUser($this->security->getUser());
        $this->entityManager->persist($invitation);
        if ($invitation->getCircle() !== null) {
            $membership = $this->entityManager->getRepository(CircleMembership::class)
                ->findOneBy([
                    'circle' => $invitation->getCircle(),
                    'user' => $invitation->getUser(),
                ]);
            if (!$membership) {
                $membership = new CircleMembership();
                $membership->setUser($invitation->getUser());
                $membership->setCircle($invitation->getCircle());
                $this->entityManager->persist($membership);
            }
            $membership->setStatus(CircleMembership::STATUS_OK);
        }
        $this->entityManager->flush();
    }
}
