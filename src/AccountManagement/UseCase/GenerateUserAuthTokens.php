<?php

declare(strict_types=1);

namespace App\AccountManagement\UseCase;

use App\AccountManagement\Entity\User\User;
use Gesdinet\JWTRefreshTokenBundle\Generator\RefreshTokenGeneratorInterface;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class GenerateUserAuthTokens
{
    public function __construct(
        private JWTTokenManagerInterface $jwtManager,
        private RefreshTokenGeneratorInterface $refreshTokenGenerator,
        private int $refreshTokenTTL
    ) {
    }

    public function __invoke(
        User $user,
    ): RefreshTokenInterface {
        $user->setToken($this->jwtManager->create($user));
        $refreshToken = $this->refreshTokenGenerator->createForUserWithTtl($user, $this->refreshTokenTTL);
        $user->setRefreshToken($refreshToken->getRefreshToken());
        $user->setTokenExpiresAt($this->jwtManager->parse($user->getToken())['exp'] * 1000);

        return $refreshToken;
    }
}
