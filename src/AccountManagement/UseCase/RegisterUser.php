<?php

declare(strict_types=1);

namespace App\AccountManagement\UseCase;

use ApiPlatform\Validator\Exception\ValidationException;
use App\AccountManagement\Entity\User\Avatar;
use App\AccountManagement\Entity\User\Invitation;
use App\AccountManagement\Entity\User\User;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterUser
{
    public function __construct(
        private PhoneNumberUtil $phoneNumberUtil,
        private JWTEncoderInterface $jwtEncoder,
        private UserPasswordHasherInterface $passwordHasher,
        private ValidatorInterface $validator,
    ) {
    }

    public function __invoke(
        User $user,
        string $password,
        string $nationalNumber,
        int $countryCode,
        ?UploadedFile $avatarFile = null,
        ?string $latitude = null,
        ?string $longitude = null,
        ?string $confirmCode = null,
        ?Invitation $invitation = null,
    ): User {
        $user->setPassword($this->passwordHasher->hashPassword($user, $password));

        $user->setPhoneNumberObject($this->phoneNumberUtil->parse(
            $nationalNumber,
            $this->phoneNumberUtil->getRegionCodeForCountryCode($countryCode)
        ));
        if ($avatarFile) {
            $avatar = new Avatar();
            $avatar->setImageFile($avatarFile);
            $user->setAvatar($avatar);
        }
        if ($latitude && $longitude) {
            $user->setLocation("SRID=4326;POINT Z($longitude $latitude 0)");
        }

        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        $user->setConfirmed(!$confirmCode);
        if ($confirmCode) {
            $user->setConfirmationToken(
                $this->jwtEncoder->encode([
                    'code' => $confirmCode,
                    'expireAt' => (new \DateTime())->modify('+1 week'),
                ])
            );
        }
        if ($invitation) {
            $invitation->setEmail($user->getEmail());
            $invitation->setUser($user);
        }

        return $user;
    }

    public static function generateConfirmCode(): string
    {
        return str_pad((string) random_int(0, 9999), 4, '0', STR_PAD_LEFT);
    }
}
