<?php

declare(strict_types=1);

namespace App\AccountManagement\UseCase;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class SendCircleInvitationMail
{
    public function __construct(
        private string $defaultSender,
        private string $defaultLocale,
        private MailerInterface $mailer,
        private TranslatorInterface $translator,
        private string $clientBaseUrl,
    ) {
    }

    public function __invoke(
        string $email,
        string $message,
        string $firstname,
        string $lastname,
        string $inviteLink,
        string $invitedBy,
        ?string $language = null,
    ): void {
        $email = (new TemplatedEmail())
            ->from(new Address($this->defaultSender, 'Communo'))
            ->to(new Address($email))
            ->subject($this->translator->trans('email.invitation.newCircleInvitation.welcome', [
                'name' => $invitedBy,
            ], null, $language ?? $this->defaultLocale))
            ->htmlTemplate('emails/invitation/sendCircleInvitation.html.twig')
            ->textTemplate('emails/invitation/sendCircleInvitation.txt.twig')
            ->context([
                'footer_text' => 'Communo',
            ])
            ->context([
                'language' => $language ?? $this->defaultLocale,
                'instanceUrl' => $this->clientBaseUrl,
                'invite_link' => $inviteLink,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'message' => $message,
                'invitedBy' => $invitedBy,
                'actions' => [],
            ]);
        $this->mailer->send($email);
    }
}
