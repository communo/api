<?php

declare(strict_types=1);

namespace App\AccountManagement\UseCase;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

class SendInvitationMail
{
    public function __construct(
        private string $defaultSender,
        private string $defaultLocale,
        private MailerInterface $mailer,
        private TranslatorInterface $translator,
        private string $clientBaseUrl,
    ) {
    }

    public function __invoke(Uuid $token, string $email, string $postalCode): void
    {
        $email = (new TemplatedEmail())
            ->from(new Address($this->defaultSender, 'Communo'))
            ->to(new Address($email))
            ->subject($this->translator->trans('email.invitation.newInvitation.welcome', [], null, $this->defaultLocale))
            ->htmlTemplate('emails/invitation/sendInvitation.html.twig')
            ->textTemplate('emails/invitation/sendInvitation.txt.twig')
            ->context([
                'footer_text' => 'Communo',
            ])
            ->context([
                'language' => $this->defaultLocale,
                'instanceUrl' => $this->clientBaseUrl,
                'invite_link' => sprintf(
                    '%s/register?token=%s',
                    $this->clientBaseUrl,
                    $token
                ),
                'actions' => [],
            ]);
        $this->mailer->send($email);
    }
}
