<?php

declare(strict_types=1);

namespace App\AccountManagement\UseCase;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class SendNewUserWelcomeMail
{
    public function __construct(
        private string $defaultSender,
        private TranslatorInterface $translator,
        private MailerInterface $mailer,
        private string $clientBaseUrl
    ) {
    }

    /**
     * @param array{
     *     language: string,
     *     firstname: string,
     *     email: string,
     * } $user
     *
     * @throws TransportExceptionInterface
     */
    public function __invoke(
        array $user
    ): void {
        $email = (new TemplatedEmail())
            ->from(new Address($this->defaultSender, 'Communo'))
            ->to(new Address($user['email'], $user['firstname']))
            ->subject(
                $this->translator->trans(
                    id: 'email.user.register.welcome.title',
                    parameters: ['user' => $user['firstname']],
                    locale: $user['language']
                )
            )
            ->htmlTemplate('emails/register/welcome.html.twig')
            ->textTemplate('emails/register/welcome.txt.twig')
            ->context([
                'footer_text' => 'Communo',
            ])
            ->context([
                'language' => $user['language'],
                'user' => $user,
                'actions' => [[
                    'text' => $this->translator->trans(
                        id: 'email.user.register.welcome.action.addMaterial',
                        locale: $user['language']
                    ),
                    'url' => sprintf('%s/pooling/new', $this->clientBaseUrl),
                ]],
            ]);
        $this->mailer->send($email);
    }
}
