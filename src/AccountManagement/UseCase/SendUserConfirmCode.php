<?php

declare(strict_types=1);

namespace App\AccountManagement\UseCase;

use App\AccountManagement\Entity\User\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class SendUserConfirmCode
{
    public function __construct(
        private string $defaultSender,
        private TranslatorInterface $translator,
        private MailerInterface $mailer,
        private string $clientBaseUrl
    ) {
    }

    public function __invoke(
        User $user,
        string $code
    ): string {
        $confirmUrl = sprintf('%s/register/confirm/%s', $this->clientBaseUrl, $user->getConfirmationToken());
        $email = (new TemplatedEmail())
            ->from(new Address($this->defaultSender, 'Communo'))
            ->to(new Address($user->getEmail(), $user->getFirstname()))
            ->subject(
                $this->translator->trans(
                    id: 'email.user.register.confirm.title',
                    parameters: ['user' => $user->getFirstname()],
                    locale: $user->getSettings()?->getLanguage()
                )
            )
            ->htmlTemplate('emails/register/confirm.html.twig')
            ->textTemplate('emails/register/confirm.txt.twig')
            ->context([
                'footer_text' => 'Communo',
            ])
            ->context([
                'language' => $user->getSettings()?->getLanguage(),
                'user' => $user,
                'code' => $code,
                'actions' => [[
                    'text' => $this->translator->trans(
                        id: 'email.user.register.confirm.action.label',
                        locale: $user->getSettings()?->getLanguage()
                    ),
                    'url' => $confirmUrl,
                ]],
            ]);
        $this->mailer->send($email);

        return $confirmUrl;
    }
}
