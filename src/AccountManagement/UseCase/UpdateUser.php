<?php

declare(strict_types=1);

namespace App\AccountManagement\UseCase;

use ApiPlatform\Validator\Exception\ValidationException;
use App\AccountManagement\Entity\User\Avatar;
use App\AccountManagement\Entity\User\User;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UpdateUser
{
    public function __construct(
        private PhoneNumberUtil $phoneNumberUtil,
        private ValidatorInterface $validator,
    ) {
    }

    public function __invoke(
        User $user,
        ?string $nationalNumber = null,
        ?int $countryCode = null,
        ?UploadedFile $avatarFile = null,
        ?bool $removeAvatar = false,
        ?string $latitude = null,
        ?string $longitude = null,
    ): User {
        if ($avatarFile) {
            $avatar = new Avatar();
            $avatar->setImageFile($avatarFile);
            $user->setAvatar($avatar);
        } elseif ($removeAvatar) {
            $user->setAvatar(null);
        }
        if ($nationalNumber && $countryCode) {
            $user->setPhoneNumberObject($this->phoneNumberUtil->parse(
                $nationalNumber,
                $this->phoneNumberUtil->getRegionCodeForCountryCode($countryCode)
            ));
        }
        if ($latitude && $longitude) {
            $user->setLocation("SRID=4326;POINTZ($longitude $latitude 0)");
        }

        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        return $user;
    }
}
