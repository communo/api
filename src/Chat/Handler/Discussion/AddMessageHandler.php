<?php

declare(strict_types=1);

namespace App\Chat\Handler\Discussion;

use App\AccountManagement\Entity\User\User;
use App\Chat\Entity\Discussion\Discussion;
use App\Chat\Entity\Discussion\Message;

class AddMessageHandler
{
    /**
     * @param array<string, string> $parameters
     */
    public function __invoke(Discussion $discussion, string $content, array $parameters = [], ?User $user = null): void
    {
        $message = new Message();
        $message->setContent($content);
        $message->setAuthor($user);
        $parametersJson = json_encode($parameters);
        $message->setParameters($parametersJson === false ? '[]' : $parametersJson);
        $discussion->addMessage($message);
    }
}
