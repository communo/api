<?php

declare(strict_types=1);

namespace App\Chat\Resolver\Discussion;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\Chat\Entity\Discussion\Discussion;
use App\Chat\Handler\Discussion\AddMessageHandler;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class AddMessageDiscussionResolver implements MutationResolverInterface
{
    public function __construct(
        private readonly TokenStorageInterface $tokenStorage,
        private readonly AddMessageHandler $addMessage,
    ) {
    }

    /**
     * @param Discussion|null $item
     * @param array{
     *     args: array{
     *         input: array{
     *             message: string
     *         }
     *     }
     * } $context
     */
    public function __invoke($item, array $context): ?Discussion
    {
        $input = $context['args']['input'];
        if (array_key_exists('message', $input) && $input['message'] !== '' && $this->tokenStorage->getToken()) {
            /** @var User $user */
            $user = $this->tokenStorage->getToken()->getUser();
            ($this->addMessage)(
                discussion: $item,
                content: $input['message'],
                user: $user
            );

            return $item;
        }

        return null;
    }
}
