<?php

declare(strict_types=1);

namespace App\Chat\Security\Voter;

use App\Chat\Entity\Discussion\Discussion;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class DiscussionVoter extends Voter
{
    public const ACCESS = 'access';
    public const CHAT = 'chat';

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::ACCESS, self::CHAT])
            && $subject instanceof Discussion;
    }

    /**
     * @param Discussion $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        return $subject->getUsers()->contains($token->getUser());
    }
}
