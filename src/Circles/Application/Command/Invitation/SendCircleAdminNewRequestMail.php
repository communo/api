<?php

declare(strict_types=1);

namespace App\Circles\Application\Command\Invitation;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class SendCircleAdminNewRequestMail
{
    public function __construct(
        private string $defaultSender,
        private string $defaultLocale,
        private MailerInterface $mailer,
        private TranslatorInterface $translator,
        private string $clientBaseUrl
    ) {
    }

    /**
     * @param array<int, Address> $to
     * @param array<int, Address> $cc
     */
    public function __invoke(array $to, Address $replyTo, string $circleLink, string $message, array $cc = []): void
    {
        $email = (new TemplatedEmail())
            ->from(new Address($this->defaultSender, 'Communo'))
            ->to(...$to)
            ->cc(...$cc)
            ->replyTo($replyTo)
            ->subject(
                $this->translator->trans(
                    'email.invitation.newRequest.title',
                    ['name' => $replyTo->getName()],
                    null,
                    $this->defaultLocale
                )
            )
            ->htmlTemplate('emails/invitation/newRequest.html.twig')
            ->textTemplate('emails/invitation/newRequest.txt.twig')
            ->context([
                'footer_text' => 'Communo',
            ])
            ->context([
                'language' => $this->defaultLocale,
                'name' => $replyTo->getName(),
                'message' => $message,
                'actions' => [[
                    'url' => sprintf('%s%s', $this->clientBaseUrl, $circleLink),
                    'text' => $this->translator->trans(
                        'email.invitation.newRequest.manage',
                        [],
                        null,
                        $this->defaultLocale
                    ),
                ]],
            ]);
        $this->mailer->send($email);
    }
}
