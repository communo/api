<?php

declare(strict_types=1);

namespace App\Circles\Application\Security\Voter;

use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\CircleImage;
use App\Circles\Entity\CirclePermissionType;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CircleImageVoter extends Voter
{
    public const DELETE = 'delete';
    public const EDIT = 'edit';

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [
            self::EDIT,
            self::DELETE,
        ])
            && $subject instanceof CircleImage;
    }

    /**
     * @param CircleImage $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }
        // if the image is associated to a circle, we check membership
        if ($circle = $subject->getCircle()) {
            if (!$membership = $circle->getMembershipByUser($user)) {
                return false;
            }

            return in_array(
                needle: CircleVoter::EDIT,
                haystack: CirclePermissionType::getAvailableActions($membership->getPermission()),
                strict: true
            );
        }  // if image is not yet associated to a circle, allow only author

        return $subject->getCreatedBy() === $user;
    }
}
