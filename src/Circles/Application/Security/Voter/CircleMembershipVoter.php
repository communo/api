<?php

declare(strict_types=1);

namespace App\Circles\Application\Security\Voter;

use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\CircleMembership;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CircleMembershipVoter extends Voter
{
    public const ACCEPT = 'accept';
    public const REFUSE = 'refuse';

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [
            self::ACCEPT,
            self::REFUSE,
        ])
            && $subject instanceof CircleMembership;
    }

    /**
     * @param CircleMembership $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }

        return $subject->getUser() === $user;
    }
}
