<?php

declare(strict_types=1);

namespace App\Circles\Application\Security\Voter;

use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\Circle;
use App\Circles\Entity\CircleMembership;
use App\Circles\Entity\CirclePermissionType;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CircleVoter extends Voter
{
    public const DELETE = 'delete';
    public const EDIT = 'edit';
    public const ACCEPT_USER = 'acceptUser';
    public const REJECT_USER = 'rejectUser';
    public const PROMOTE_TO_OWNER = CircleMembership::TRANSITION_PROMOTE_TO_OWNER;
    public const PROMOTE_TO_ADMIN = CircleMembership::TRANSITION_PROMOTE_TO_ADMIN;
    public const DEMOTE_TO_MEMBER = CircleMembership::TRANSITION_DEMOTE_TO_MEMBER;

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [
            ...self::getAdminPermissions(),
        ])
            && $subject instanceof Circle;
    }

    /**
     * @param Circle $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }
        if (!$membership = $subject->getMembershipByUser($user)) {
            return false;
        }
        $availableActions = CirclePermissionType::getAvailableActions($membership->getPermission());

        return in_array(
            needle: $attribute,
            haystack: $availableActions,
            strict: true
        );
    }

    /**
     * @return string[]
     */
    private static function getAdminPermissions(): array
    {
        return [
            self::EDIT,
            self::DELETE,
            self::ACCEPT_USER,
            self::REJECT_USER,
            self::PROMOTE_TO_OWNER,
            self::PROMOTE_TO_ADMIN,
            self::DEMOTE_TO_MEMBER,
        ];
    }
}
