<?php

declare(strict_types=1);

namespace App\Circles\Entity;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\DeleteMutation;
use ApiPlatform\Metadata\GraphQl\Mutation;
use ApiPlatform\Metadata\GraphQl\Query;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use App\AccountManagement\Entity\User\Invitation;
use App\AccountManagement\Entity\User\User;
use App\Circles\Infra\ApiPlatform\Filter\IndexableFilter;
use App\Circles\Infra\ApiPlatform\Filter\OnlyIManageFilter;
use App\Circles\Infra\ApiPlatform\Filter\SearchTermFilter;
use App\Circles\Infra\ApiPlatform\Resolver\CircleCollectionResolver;
use App\Circles\Infra\ApiPlatform\Resolver\CircleMutationResolver;
use App\Circles\Infra\ApiPlatform\Resolver\CircleResolver;
use App\Circles\Infra\ApiPlatform\State\Processor\CircleNewProcessor;
use App\Circles\Infra\ApiPlatform\State\Processor\UpdateCircleProcessor;
use App\Circles\Repository\CircleRepository;
use App\Pooling\Domain\Model\Material\MaterialPricing;
use App\Pooling\Domain\Model\Material\Ownership\CircleMaterialOwnership;
use App\Subscription\Domain\Model\Subscription;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ReadableCollection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Jsor\Doctrine\PostGIS\Types\PostGISType;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [],
    graphQlOperations: [
        new Query(
            resolver: CircleResolver::class,
            name: 'item_query'
        ),
        new QueryCollection(
            resolver: CircleCollectionResolver::class,
            extraArgs: [
                'searchTerms' => ['type' => 'String'],
            ],
            filters: [
                IndexableFilter::class,
                SearchTermFilter::class,
            ],
            name: 'collection_query'
        ),
        new QueryCollection(
            resolver: CircleCollectionResolver::class,
            filters: [
                OnlyIManageFilter::class,
            ],
            name: 'my'
        ),
        new Mutation(
            extraArgs: [
                'id' => ['type' => 'ID!'],
                'logo' => ['type' => 'Upload'],
                'removeLogo' => ['type' => 'Boolean'],
                'cover' => ['type' => 'Upload'],
                'removeCover' => ['type' => 'Boolean'],
            ],
            denormalizationContext: ['groups' => ['update']],
            security: "is_granted('edit', object)",
            name: 'update',
            processor: UpdateCircleProcessor::class
        ),
        new Mutation(
            resolver: CircleMutationResolver::class,
            extraArgs: [
                'logo' => ['type' => 'Upload'],
                'cover' => ['type' => 'Upload'],
            ],
            denormalizationContext: ['groups' => ['create']],
            security: 'is_authenticated()',
            name: 'create',
            processor: CircleNewProcessor::class
        ),
        new DeleteMutation(
            security: "is_granted('delete', object)",
            name: 'delete',
        ),
    ]
)]
#[ORM\Entity(repositoryClass: CircleRepository::class)]
#[ApiFilter(filterClass: SearchFilter::class, properties: [
    'name' => 'ipartial',
    'description' => 'ipartial',
    'city' => 'ipartial',
    'explicitLocationLabel' => 'ipartial',
    'invitations.email' => 'exact',
])]
#[ApiFilter(BooleanFilter::class, properties: ['indexable'])]
#[ApiFilter(RangeFilter::class, properties: ['latitude', 'longitude'])]
class Circle implements TimestampableInterface
{
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    #[ApiProperty(identifier: true)]
    #[ORM\Column(type: 'string', length: 100)]
    #[Gedmo\Slug(fields: ['name'])]
    protected string $slug;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    #[ApiProperty(identifier: false)]
    private ?Uuid $id = null;

    #[ORM\Column(type: 'string', length: 255, nullable: false)]
    #[Groups(['create', 'update'])]
    private string $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['create', 'update'])]
    private ?string $website;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'children')]
    private ?Circle $parent = null;

    /**
     * @var Collection<int, Circle>|null
     */
    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class)]
    private ?Collection $children;

    /**
     * @var Collection<int, CircleMembership>
     */
    #[ORM\OneToMany(mappedBy: 'circle', targetEntity: CircleMembership::class)]
    private Collection $memberships;

    /**
     * @var Collection<int, Invitation>
     */
    #[ORM\OneToMany(mappedBy: 'circle', targetEntity: Invitation::class)]
    private Collection $invitations;

    /**
     * @var Collection<int, CircleInvitationLink>
     */
    #[ORM\OneToMany(mappedBy: 'circle', targetEntity: CircleInvitationLink::class, cascade: ['persist', 'remove'])]
    private Collection $invitationLinks;

    /**
     * @var Collection<int, CircleMembership>
     */
    private Collection $pendingMemberships;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['create', 'update'])]
    private ?string $description;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['create', 'update'])]
    private ?string $address;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['create', 'update'])]
    private ?string $city;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['create'])]
    private ?string $explicitLocationLabel;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['create', 'update'])]
    private ?string $country;

    #[ORM\Column(type: 'string', length: 10, nullable: true)]
    #[Groups(['create', 'update'])]
    private ?string $postalCode;

    #[ORM\Column(type: 'string', length: 30, nullable: true)]
    #[Groups(['create', 'update'])]
    private ?string $latitude;

    #[ORM\Column(type: 'string', length: 30, nullable: true)]
    #[Groups(['create', 'update'])]
    private ?string $longitude;

    #[ORM\Column(type: PostGISType::GEOMETRY, options: ['geometry_type' => 'POINTZ', 'srid' => 4326], nullable: true)]
    #[Groups(['create'])]
    private ?string $location = null;

    #[ApiProperty(writable: false)]
    private ?float $distance = null;

    /**
     * @var Collection<int, MaterialPricing>
     */
    #[ORM\OneToMany(mappedBy: 'circle', targetEntity: MaterialPricing::class)]
    #[Groups(['create'])]
    private Collection $pricings;

    #[ORM\OneToOne(inversedBy: 'circle', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private ?CircleLogo $logo = null;

    #[ORM\OneToOne(inversedBy: 'circle', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private ?CircleCover $cover = null;

    /**
     * Not persisted property, used to define what can do current logged user;
     * set by CircleResolver.
     *
     * @var string[]
     */
    #[ApiProperty(writable: false)]
    private ?array $activeUserActions = [];

    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => true])]
    #[Groups(['create', 'update'])]
    private bool $indexable = true;

    /**
     * @var Collection<int, CircleMaterialOwnership>
     */
    #[ORM\OneToMany(mappedBy: 'circle', targetEntity: CircleMaterialOwnership::class)]
    private Collection $circleOwnerships;

    #[ORM\OneToOne(mappedBy: 'circle', cascade: ['persist', 'remove'])]
    private ?Subscription $subscription;

    public function __construct()
    {
        $this->memberships = new ArrayCollection();
        $this->invitations = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->pricings = new ArrayCollection();
        $this->circleOwnerships = new ArrayCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Circle>
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * @param array<int, Circle> $children
     */
    public function setChildren(array $children): self
    {
        foreach ($children as $child) {
            $this->addChild($child);
        }

        return $this;
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children->add($child);
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getLogo(): ?CircleImage
    {
        return $this->logo;
    }

    public function setLogo(?CircleImage $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getCover(): ?CircleImage
    {
        return $this->cover;
    }

    public function setCover(?CircleImage $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): void
    {
        $this->website = $website;
    }

    public function getMembershipByUser(User $user): CircleMembership|false
    {
        return $this->getMemberships()->filter(function (CircleMembership $membership) use ($user) {
            return $membership->getUser() === $user;
        })->first();
    }

    /**
     * @return Collection<int, CircleMembership>
     */
    public function getMemberships(): Collection
    {
        return $this->memberships;
    }

    /**
     * @param Collection<int, CircleMembership> $memberships
     */
    public function setMemberships(Collection $memberships): void
    {
        $this->memberships = $memberships;
    }

    /**
     * @return Collection<int, Invitation>
     */
    public function getInvitations(): Collection
    {
        return $this->invitations;
    }

    /**
     * @param Collection<int, Invitation> $invitations
     */
    public function setInvitations(Collection $invitations): void
    {
        $this->invitations = $invitations;
    }

    /**
     * @return Collection<int, CircleMembership>
     */
    public function getPendingMemberships(): Collection
    {
        return $this->pendingMemberships;
    }

    /**
     * @param Collection<int, CircleMembership> $pendingMemberships
     */
    public function setPendingMemberships(Collection $pendingMemberships): void
    {
        $this->pendingMemberships = $pendingMemberships;
    }

    /**
     * @return ReadableCollection<int, User>
     */
    public function getAdmins(bool $withOwner = true): ReadableCollection
    {
        $permissions = [
            CirclePermissionType::Administrator,
        ];
        if ($withOwner) {
            $permissions[] = CirclePermissionType::Owner;
        }

        return $this->getUserByPermissions($permissions);
    }

    /**
     * @param array<int, CirclePermissionType> $permissions
     *
     * @return ReadableCollection<int, User>
     */
    public function getUserByPermissions(array $permissions): ReadableCollection
    {
        return $this->getMemberships()->filter(function (CircleMembership $membership) use ($permissions) {
            return in_array($membership->getPermission(), $permissions);
        })->map(function (CircleMembership $membership) {
            return $membership->getUser();
        });
    }

    /**
     * @return ReadableCollection<int, User>
     */
    public function getOwners(): ReadableCollection
    {
        return $this->getUserByPermissions([CirclePermissionType::Owner]);
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getActiveUserActions(): ?array
    {
        return $this->activeUserActions;
    }

    /**
     * @param array<int, string> $activeUserActions
     */
    public function setActiveUserActions(?array $activeUserActions = []): void
    {
        $this->activeUserActions = $activeUserActions;
    }

    /**
     * @return Collection<int, MaterialPricing>
     */
    public function getPricings(): Collection
    {
        return $this->pricings;
    }

    /**
     * @param Collection<int, MaterialPricing> $pricings
     */
    public function setPricings(Collection $pricings): void
    {
        $this->pricings = $pricings;
    }

    public function isIndexable(): bool
    {
        return $this->indexable;
    }

    public function setIndexable(bool $indexable): void
    {
        $this->indexable = $indexable;
    }

    public function getExplicitLocationLabel(): ?string
    {
        return $this->explicitLocationLabel;
    }

    public function setExplicitLocationLabel(?string $explicitLocationLabel): void
    {
        $this->explicitLocationLabel = $explicitLocationLabel;
    }

    /**
     * @return Collection<int, CircleMaterialOwnership>
     */
    public function getCircleOwnerships(): Collection
    {
        return $this->circleOwnerships;
    }

    public function addCircleOwnership(CircleMaterialOwnership $circleOwnership): static
    {
        if (!$this->circleOwnerships->contains($circleOwnership)) {
            $this->circleOwnerships->add($circleOwnership);
            $circleOwnership->setCircle($this);
        }

        return $this;
    }

    public function removeCircleOwnership(CircleMaterialOwnership $circleOwnership): static
    {
        if ($this->circleOwnerships->removeElement($circleOwnership)) {
            // set the owning side to null (unless already changed)
            if ($circleOwnership->getCircle() === $this) {
                $circleOwnership->setCircle(null);
            }
        }

        return $this;
    }

    public function getSubscription(): ?Subscription
    {
        return $this->subscription;
    }

    public function setSubscription(?Subscription $subscription): void
    {
        $this->subscription = $subscription;
    }

    public function getDistance(): ?float
    {
        return $this->distance;
    }

    public function setDistance(?float $distance): void
    {
        $this->distance = $distance;
    }

    public function getInvitationLinks(): Collection
    {
        return $this->invitationLinks;
    }

    public function setInvitationLinks(Collection $invitationLinks): void
    {
        foreach ($invitationLinks as $invitationLink) {
            $invitationLink->setCircle($this);
        }
        $this->invitationLinks = $invitationLinks;
    }
}
