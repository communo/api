<?php

declare(strict_types=1);

namespace App\Circles\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Mutation;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[Vich\Uploadable]
#[ApiResource(
    types: ['https://schema.org/MediaObject'],
    operations: [],
    graphQlOperations: [
        new Mutation(
            args: [
                'imageFile' => [
                    'type' => 'Upload!',
                    'description' => 'The file to upload',
                ],
                'circle' => [
                    'type' => 'ID!',
                ],
            ],
            name: 'create'
        ),
    ]
)]
#[ORM\Entity]
class CircleCover extends CircleImage
{
    #[ORM\OneToOne(mappedBy: 'cover')]
    private ?Circle $circle = null;

    public function getCircle(): ?Circle
    {
        return $this->circle;
    }

    public function setCircle(?Circle $circle): self
    {
        // unset the owning side of the relation if necessary
        if ($circle === null && $this->circle !== null) {
            $this->circle->setCover(null);
        }

        $this->circle = $circle;

        return $this;
    }
}
