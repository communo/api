<?php

declare(strict_types=1);

namespace App\Circles\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\DeleteMutation;
use App\AccountManagement\Entity\User\User;
use App\Shared\Entity\Image;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[Vich\Uploadable]
#[ApiResource(
    types: ['https://schema.org/MediaObject'],
    operations: [],
    graphQlOperations: [
        new DeleteMutation(
            security: "is_granted('delete', object)",
            name: 'delete',
        ),
    ]
)]
#[MappedSuperclass]
abstract class CircleImage extends Image
{
    #[Vich\UploadableField(
        mapping: 'circle_image',
        fileNameProperty: 'imageName',
        size: 'size',
        mimeType: 'mime',
        originalName: 'originalName',
        dimensions: 'dimensions'
    )]
    protected ?File $imageFile = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    #[Gedmo\Blameable(on: 'create')]
    private ?User $createdBy;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: true)]
    #[Gedmo\Blameable(on: 'update')]
    private ?User $updatedBy;

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }
}
