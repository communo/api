<?php

declare(strict_types=1);

namespace App\Circles\Entity;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Query;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [],
    paginationEnabled: false,
    graphQlOperations: [
        new Query(
            name: 'item_query'
        ),
    ]
)]
#[ApiFilter(BooleanFilter::class, properties: ['enabled'])]
#[ORM\Entity]
class CircleInvitationLink implements TimestampableInterface
{
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    public ?string $_id = null;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    private ?Uuid $id = null;

    #[ORM\Column(type: Types::INTEGER, options: ['default' => 0])]
    private ?int $numberOfUses = 0;

    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => true])]
    private bool $enabled = true;

    public function __construct(
        #[ORM\ManyToOne(targetEntity: Circle::class, inversedBy: 'invitationLinks')]
        #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
        public Circle $circle,
    ) {
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): void
    {
        $this->id = $id;
    }

    public function getCircle(): Circle
    {
        return $this->circle;
    }

    public function setCircle(Circle $circle): void
    {
        $this->circle = $circle;
    }

    public function getNumberOfUses(): ?int
    {
        return $this->numberOfUses;
    }

    public function setNumberOfUses(?int $numberOfUses): void
    {
        $this->numberOfUses = $numberOfUses;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }
}
