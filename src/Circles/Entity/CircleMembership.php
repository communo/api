<?php

declare(strict_types=1);

namespace App\Circles\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Mutation;
use ApiPlatform\Metadata\GraphQl\Query;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use App\AccountManagement\Entity\User\User;
use App\Circles\Infra\ApiPlatform\Resolver\CircleMembership\AskToJoinCircleResolver;
use App\Circles\Infra\ApiPlatform\Resolver\CircleMembership\ChangePermissionResolver;
use App\Circles\Infra\ApiPlatform\Resolver\CircleMembership\ChangeStatusResolver;
use App\Circles\Infra\ApiPlatform\Resolver\CircleMembership\CircleMembershipCollectionResolver;
use App\Circles\Infra\ApiPlatform\Resolver\CircleMembership\JoinCircleWithInvitationLinkResolver;
use App\Circles\Infra\Doctrine\Type\PermissionType;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [],
    graphQlOperations: [
        new Query(),
        new QueryCollection(
            resolver: CircleMembershipCollectionResolver::class
        ),
        new Mutation(
            resolver: AskToJoinCircleResolver::class,
            args: [
                'circle' => ['type' => 'String', 'description' => 'Circle IRI'],
                'message' => ['type' => 'String', 'description' => 'Is there a message to attach to circle application ?'],
            ],
            name: 'askToJoin'
        ),
        new Mutation(
            resolver: JoinCircleWithInvitationLinkResolver::class,
            args: [
                'invitationLinkId' => ['type' => 'String'],
            ],
            name: 'join'
        ),
        new Mutation(
            resolver: ChangeStatusResolver::class,
            args: [
                'id' => ['type' => 'ID!'],
                'transition' => ['type' => 'String', 'description' => 'What transition do you want to run ?'],
                'message' => ['type' => 'String', 'description' => 'Is there a message to attach ?'],
            ],
            name: 'changeStatus'
        ),
        new Mutation(
            resolver: ChangePermissionResolver::class,
            args: [
                'id' => ['type' => 'ID!'],
                'transition' => ['type' => 'String', 'description' => 'What transition do you want to run ?'],
                'message' => ['type' => 'String', 'description' => 'Is there a message to attach ?'],
            ],
            name: 'changePermission'
        ),
    ]
)]
#[ORM\Entity]
#[ApiFilter(filterClass: SearchFilter::class, properties: [
    'user.slug' => 'exact',
    'circle.slug' => 'exact',
    'status' => 'exact',
])]
#[ApiFilter(filterClass: OrderFilter::class, properties: [
    'permission',
    'user.lastname',
    'user.firstname',
    'createdAt',
])]
class CircleMembership implements TimestampableInterface
{
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    public const STATUS_PENDING = 'pending';
    public const STATUS_INVITED = 'invited';
    public const STATUS_OK = 'ok';
    public const STATUS_REJECTED = 'rejected';
    public const STATUS_REFUSED = 'refused';
    public const STATUS_KICKED = 'kicked';
    public const PERMISSION_OWNER = CirclePermissionType::Owner;
    public const PERMISSION_ADMINISTRATOR = CirclePermissionType::Administrator;
    public const PERMISSION_MEMBER = CirclePermissionType::Member;
    public const TRANSITION_ACCEPT = 'accept';
    public const TRANSITION_ACCEPT_INVITATION = 'acceptInvitation';
    public const TRANSITION_REFUSE_INVITATION = 'refuseInvitation';
    public const TRANSITION_RE_INVITE = 'reInvite';
    public const TRANSITION_REJECT = 'reject';
    public const TRANSITION_KICK = 'kick';
    public const TRANSITION_PROMOTE_TO_OWNER = 'promoteToOwner';
    public const TRANSITION_PROMOTE_TO_ADMIN = 'promoteToAdmin';
    public const TRANSITION_DEMOTE_TO_MEMBER = 'demoteToMember';

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    #[ApiProperty(identifier: true)]
    private ?Uuid $id = null;

    #[ORM\ManyToOne(inversedBy: 'memberships')]
    #[ORM\JoinColumn(onDelete: 'cascade', nullable: false)]
    private Circle $circle;

    #[ORM\ManyToOne(inversedBy: 'circleMemberships')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'cascade')]
    private User $user;

    #[ORM\Column(type: PermissionType::NAME, nullable: false)]
    private CirclePermissionType $permission = CirclePermissionType::Member;

    /**
     * @var array<int, string>|null
     */
    private ?array $statusTransitionAvailables = [];

    /**
     * @var array<int, string>|null
     */
    private ?array $permissionTransitionAvailables = [];

    #[ORM\Column(type: 'string', length: 15, options: ['default' => self::STATUS_PENDING])]
    private string $status = self::STATUS_PENDING;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $message;

    public function getCircle(): Circle
    {
        return $this->circle;
    }

    public function setCircle(Circle $circle): void
    {
        $this->circle = $circle;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getPermission(): CirclePermissionType
    {
        return $this->permission;
    }

    public function setPermission(CirclePermissionType $permission): void
    {
        $this->permission = $permission;
    }

    public function getPermissionMarking(): string
    {
        return $this->permission->value;
    }

    public function setPermissionMarking(string $marking): void
    {
        $this->permission = CirclePermissionType::from($marking);
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): void
    {
        $this->id = $id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getStatusTransitionAvailables(): ?array
    {
        return $this->statusTransitionAvailables;
    }

    /**
     * @param array<int, string> $enabledTransitions
     *
     * @return void
     */
    public function setStatusTransitionAvailables(array $enabledTransitions)
    {
        $this->statusTransitionAvailables = $enabledTransitions;
    }

    /**
     * @return string[]|null
     */
    public function getPermissionTransitionAvailables(): ?array
    {
        return $this->permissionTransitionAvailables;
    }

    /**
     * @param array<int, string> $enabledTransitions
     *
     * @return void
     */
    public function setPermissionTransitionAvailables(array $enabledTransitions)
    {
        $this->permissionTransitionAvailables = $enabledTransitions;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }
}
