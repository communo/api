<?php

declare(strict_types=1);

namespace App\Circles\Entity;

enum CirclePermissionType: string
{
    case Member = 'Member';
    case Administrator = 'Administrator';
    case Owner = 'Owner';

    /**
     * @return string[]
     */
    public static function getAvailableActions(?self $permissionType): array
    {
        $actions = [];
        if ($permissionType === self::Owner) {
            $actions = array_merge($actions, [
                CircleMembership::TRANSITION_DEMOTE_TO_MEMBER, 'delete', 'listMembers']);
        }
        if (in_array($permissionType, [self::Owner, self::Administrator], true)) {
            $actions = array_merge($actions, [
                'edit',
                'acceptUser',
                'rejectUser',
                'kickUser',
                CircleMembership::TRANSITION_PROMOTE_TO_ADMIN,
            ]);
        }
        if (in_array($permissionType, [self::Owner], true)) {
            $actions = array_merge($actions, [
                CircleMembership::TRANSITION_PROMOTE_TO_OWNER,
            ]);
        }
        if (in_array($permissionType, [self::Administrator, self::Member], true)) {
            $actions = array_merge($actions, ['quit', 'listMembers']);
        }

        return $actions;
    }
}
