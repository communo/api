<?php

declare(strict_types=1);

namespace App\Circles\Infra\ApiPlatform\Filter;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;

class IndexableFilter extends AbstractFilter
{
    /**
     * @param array<string, mixed> $context
     */
    public function apply(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        ?Operation $operation = null,
        array $context = []
    ): void {
        $rootAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->andWhere(sprintf('%s.indexable = :indexable', $rootAlias));
        $queryBuilder->setParameter('indexable', true);
    }

    /**
     * @return array<int, string>
     */
    public function getDescription(string $resourceClass): array
    {
        return [];
    }

    /**
     * @param bool $value
     * @param array<string, mixed> $context
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        // Not needed for this filter
    }
}
