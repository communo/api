<?php

declare(strict_types=1);

namespace App\Circles\Infra\ApiPlatform\Filter;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\CirclePermissionType;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class OnlyIManageFilter extends AbstractFilter
{
    public function __construct(
        private TokenStorageInterface $tokenStorage,
        ManagerRegistry $managerRegistry,
    ) {
        parent::__construct($managerRegistry);
    }

    /**
     * @param array<mixed> $context
     */
    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        /** @var User|null $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        $alias = $queryBuilder->getRootAliases()[0];

        $queryBuilder
            ->leftJoin($alias.'.memberships', 'm')
            ->andWhere('m.user = :user')
            ->setParameter('user', $user->getId())
            ->andWhere('m.permission in (:permission)')
            ->setParameter('permission', [CirclePermissionType::Owner, CirclePermissionType::Administrator]);
    }

    /** @return array<string, string> */
    public function getDescription(string $resourceClass): array
    {
        return [];
    }

    /**
     * @param array<string, mixed> $context
     * @param bool $value
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        // TODO: Implement filterProperty() method.
    }
}
