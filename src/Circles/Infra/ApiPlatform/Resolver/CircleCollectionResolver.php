<?php

declare(strict_types=1);

namespace App\Circles\Infra\ApiPlatform\Resolver;

use ApiPlatform\GraphQl\Resolver\QueryCollectionResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\Circle;
use App\Circles\Entity\CirclePermissionType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CircleCollectionResolver implements QueryCollectionResolverInterface
{
    public function __construct(
        private TokenStorageInterface $tokenStorage,
    ) {
    }

    /**
     * @param iterable<Circle> $collection
     * @param array{args: array<int, mixed>} $context
     *
     * @return iterable<Circle>
     */
    public function __invoke(iterable $collection, array $context): iterable
    {
        foreach ($collection as &$item) {
            if (is_array($item)) {
                /** @var Circle $circle */
                $circle = $item[0];

                $distance = $item['distance'] ?? null;
                if ($distance !== null) {
                    $circle->setDistance((float) $distance);
                }
            } elseif ($item instanceof Circle) {
                $circle = $item;
            } else {
                continue;
            }
            $user = $this->tokenStorage->getToken()?->getUser();
            if ($user instanceof User) {
                if ($membership = $circle->getMembershipByUser($user)) {
                    $circle->setActiveUserActions(CirclePermissionType::getAvailableActions($membership->getPermission()));
                }
            }

            $item = $circle;
        }

        return $collection;
    }
}
