<?php

declare(strict_types=1);

namespace App\Circles\Infra\ApiPlatform\Resolver;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\Circles\Entity\CircleImage;

class CircleImageResolver implements MutationResolverInterface
{
    /**
     * @param ?CircleImage $item
     * @param array{args: array<int, mixed>} $context
     */
    public function __invoke($item, array $context): object
    {
        assert($item instanceof CircleImage);

        return $item;
    }
}
