<?php

declare(strict_types=1);

namespace App\Circles\Infra\ApiPlatform\Resolver\CircleMembership;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\Circles\Application\Command\Invitation\SendCircleAdminNewRequestMail;
use App\Circles\Entity\CircleMembership;
use App\Circles\Repository\CircleMembershipRepository;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AskToJoinCircleResolver implements MutationResolverInterface
{
    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private CircleMembershipRepository $circleMembershipRepository,
        private SendCircleAdminNewRequestMail $sendCircleAdminNewRequestMail,
    ) {
    }

    /**
     * @param CircleMembership $item
     * @param array{args: array{input: array{circle: string, message?: string}}} $context
     */
    public function __invoke($item, array $context): ?CircleMembership
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        $existingItem = $this->circleMembershipRepository->findOneBy([
            'circle' => $item->getCircle(),
            'user' => $user,
        ]);
        if ($existingItem instanceof CircleMembership) {
            throw new \RuntimeException(sprintf('a membership already exists for user %s (status %s)', $user->getEmail(), $existingItem->getStatus()));
        }
        $item->setUser($user);
        $this->sendMailToAdmin($item);

        return $item;
    }

    private function sendMailToAdmin(CircleMembership $circleMembership): void
    {
        $ownersAddresses = [];
        foreach ($circleMembership->getCircle()->getOwners() as $admin) {
            $ownersAddresses[] = new Address($admin->getEmail(), $admin->getFullname(byPassSettings: true));
        }
        $adminAddresses = [];
        foreach ($circleMembership->getCircle()->getAdmins(withOwner: false) as $admin) {
            $adminAddresses[] = new Address($admin->getEmail(), $admin->getFullname(byPassSettings: true));
        }

        ($this->sendCircleAdminNewRequestMail)(
            to: $ownersAddresses,
            replyTo: new Address($circleMembership->getUser()->getEmail(), $circleMembership->getUser()->getFullname(byPassSettings: true)),
            circleLink: sprintf('/communities/%s', $circleMembership->getCircle()->getSlug()),
            message: $circleMembership->getMessage(),
            cc: $adminAddresses,
        );
    }
}
