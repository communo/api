<?php

declare(strict_types=1);

namespace App\Circles\Infra\ApiPlatform\Resolver\CircleMembership;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\Circles\Entity\CircleMembership;
use Symfony\Component\DependencyInjection\Attribute\Target;
use Symfony\Component\Workflow\WorkflowInterface;

final class ChangePermissionResolver implements MutationResolverInterface
{
    public function __construct(
        #[Target('community_membershipPermission')]
        private WorkflowInterface $workflow
    ) {
    }

    /**
     * @param CircleMembership|null $item
     * @param array{args: array{input: array{transition: string, message?: string}}} $context
     */
    public function __invoke($item, array $context): ?CircleMembership
    {
        $input = $context['args']['input'];
        $transition = $input['transition'];
        if ($this->workflow->can($item, $transition)) {
            $this->workflow->apply($item, $transition);

            return $item;
        }

        return null;
    }
}
