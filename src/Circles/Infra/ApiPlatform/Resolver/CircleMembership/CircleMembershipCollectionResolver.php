<?php

declare(strict_types=1);

namespace App\Circles\Infra\ApiPlatform\Resolver\CircleMembership;

use ApiPlatform\GraphQl\Resolver\QueryCollectionResolverInterface;
use App\Circles\Entity\CircleMembership;
use Symfony\Component\DependencyInjection\Attribute\Target;
use Symfony\Component\Workflow\WorkflowInterface;

class CircleMembershipCollectionResolver implements QueryCollectionResolverInterface
{
    public function __construct(
        #[Target('circle_membership')]
        private WorkflowInterface $statusWorkflow,
        #[Target('community_membershipPermission')]
        private WorkflowInterface $permissionWorkflow
    ) {
    }

    /**
     * @param iterable<CircleMembership> $collection
     * @param array{args: array<int, mixed>} $context
     *
     * @return iterable<CircleMembership>
     */
    public function __invoke(iterable $collection, array $context): iterable
    {
        $circles = [];
        foreach ($collection as $item) {
            $transitions = [];
            foreach ($this->statusWorkflow->getEnabledTransitions($item) as $transition) {
                $transitions[] = $transition->getName();
            }
            $item->setStatusTransitionAvailables($transitions);
            $permissionTransitions = [];
            foreach ($this->permissionWorkflow->getEnabledTransitions($item) as $transition) {
                $permissionTransitions[] = $transition->getName();
            }
            $item->setPermissionTransitionAvailables($permissionTransitions);
            $circles[] = $item->getCircle();
        }
        foreach ($circles as $circle) {
            $circle->setPendingMemberships($circle->getMemberships()->filter(function (CircleMembership $membership) {
                return $membership->getStatus() === CircleMembership::STATUS_PENDING;
            }));
        }

        return $collection;
    }
}
