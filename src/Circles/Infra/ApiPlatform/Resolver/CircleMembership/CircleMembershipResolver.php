<?php

declare(strict_types=1);

namespace App\Circles\Infra\ApiPlatform\Resolver\CircleMembership;

use ApiPlatform\GraphQl\Resolver\QueryItemResolverInterface;
use App\Circles\Entity\CircleMembership;
use Symfony\Component\DependencyInjection\Attribute\Target;
use Symfony\Component\Workflow\WorkflowInterface;

class CircleMembershipResolver implements QueryItemResolverInterface
{
    public function __construct(
        #[Target('circle_membership')]
        private WorkflowInterface $workflow
    ) {
    }

    /**
     * @param CircleMembership $item
     * @param array{args: array<int, mixed>} $context
     */
    public function __invoke($item, array $context): object
    {
        assert($item instanceof CircleMembership);
        $transitions = [];
        foreach ($this->workflow->getEnabledTransitions($item) as $transition) {
            $transitions[] = $transition->getName();
        }
        $item->setStatusTransitionAvailables($transitions);

        return $item;
    }
}
