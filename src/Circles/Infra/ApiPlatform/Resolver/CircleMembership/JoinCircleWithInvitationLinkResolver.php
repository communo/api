<?php

declare(strict_types=1);

namespace App\Circles\Infra\ApiPlatform\Resolver\CircleMembership;

use ApiPlatform\Api\IriConverterInterface;
use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\CircleInvitationLink;
use App\Circles\Entity\CircleMembership;
use App\Circles\Repository\CircleMembershipRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class JoinCircleWithInvitationLinkResolver implements MutationResolverInterface
{
    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private IriConverterInterface $iriConverter,
        private CircleMembershipRepository $circleMembershipRepository,
    ) {
    }

    /**
     * @param CircleMembership $item
     * @param array{args: array{input: array{invitationLinkId: string}}} $context
     */
    public function __invoke($item, array $context): ?CircleMembership
    {
        $input = $context['args']['input'];
        /** @var User $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        /** @var CircleInvitationLink $invitationLink */
        $invitationLink = $this->iriConverter->getResourceFromIri($input['invitationLinkId']);
        assert($invitationLink instanceof CircleInvitationLink);
        $existingItem = $this->circleMembershipRepository->findOneBy([
            'circle' => $invitationLink->getCircle(),
            'user' => $user,
        ]);
        if ($existingItem instanceof CircleMembership) {
            throw new \RuntimeException(sprintf('a membership already exists for user %s (status %s)', $user->getEmail(), $existingItem->getStatus()));
        }
        $membership = new CircleMembership();
        $membership->setUser($user);
        $membership->setCircle($invitationLink->getCircle());
        $membership->setStatus(CircleMembership::STATUS_OK);
        $this->circleMembershipRepository->save($membership);

        return $membership;
    }
}
