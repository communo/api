<?php

declare(strict_types=1);

namespace App\Circles\Infra\ApiPlatform\Resolver;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\Circles\Entity\Circle;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CircleMutationResolver implements MutationResolverInterface
{
    /**
     * @param Circle $item
     * @param array{args: array{input: array{
     *     id: string,
     *     name: string,
     *     address: string,
     *     city: string,
     *     latitude: string,
     *     longitude: string,
     *     postalCode: string,
     *     country: string,
     *     description: string,
     *     website?: string,
     *     logoFile?: UploadedFile,
     *     removeLogo?: bool,
     *     coverFile?: UploadedFile,
     *     removeCover?: bool
     * }}} $context
     */
    public function __invoke(
        $item,
        array $context
    ): object {
        assert($item instanceof Circle);

        return $item;
    }
}
