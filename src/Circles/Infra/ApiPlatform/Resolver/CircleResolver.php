<?php

declare(strict_types=1);

namespace App\Circles\Infra\ApiPlatform\Resolver;

use ApiPlatform\GraphQl\Resolver\QueryItemResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\Circle;
use App\Circles\Entity\CircleInvitationLink;
use App\Circles\Entity\CircleMembership;
use App\Circles\Entity\CirclePermissionType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CircleResolver implements QueryItemResolverInterface
{
    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * @param ?Circle $item
     * @param array{args: array<int, mixed>} $context
     */
    public function __invoke($item, array $context): object
    {
        assert($item instanceof Circle);
        $user = $this->tokenStorage->getToken()?->getUser();
        if ($user instanceof User) {
            if ($membership = $item->getMembershipByUser($user)) {
                $item->setActiveUserActions(CirclePermissionType::getAvailableActions($membership->getPermission()));
            }
        }
        if ($item->getInvitationLinks()->isEmpty()) {
            $invitationLink = new CircleInvitationLink(circle: $item);
            $item->setInvitationLinks(new ArrayCollection([$invitationLink]));
            $this->entityManager->persist($invitationLink);
            $this->entityManager->flush();
        }
        $item->setPendingMemberships($item->getMemberships()->filter(function (CircleMembership $membership) {
            return $membership->getStatus() === CircleMembership::STATUS_PENDING;
        }));

        return $item;
    }
}
