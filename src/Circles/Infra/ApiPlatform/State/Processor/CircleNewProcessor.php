<?php

declare(strict_types=1);

namespace App\Circles\Infra\ApiPlatform\State\Processor;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\Circle;
use App\Circles\UseCase\CreateCircleOwner;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @implements ProcessorInterface<Circle>
 */
class CircleNewProcessor implements ProcessorInterface
{
    /**
     * @param ProcessorInterface<Circle> $statePersistProcessorDecorated
     */
    public function __construct(
        private CreateCircleOwner $createCircleOwner,
        private ProcessorInterface $statePersistProcessorDecorated,
        private TokenStorageInterface $tokenStorage,
    ) {
    }

    /**
     * @param Circle $data
     * @param array<string, mixed> $uriVariables
     * @param array<string, mixed> $context
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Circle
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        ($this->createCircleOwner)(user: $user, circle: $data);

        return $this->statePersistProcessorDecorated->process($data, $operation, $uriVariables, $context);
    }
}
