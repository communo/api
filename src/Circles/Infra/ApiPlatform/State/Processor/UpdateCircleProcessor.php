<?php

declare(strict_types=1);

namespace App\Circles\Infra\ApiPlatform\State\Processor;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\Circle;
use App\Circles\UseCase\UpdateCircle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @implements ProcessorInterface<User>
 */
class UpdateCircleProcessor implements ProcessorInterface
{
    /**
     * @param ProcessorInterface<User> $statePersistProcessorDecorated
     */
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ProcessorInterface $statePersistProcessorDecorated,
        private UpdateCircle $updateCircle,
    ) {
    }

    /**
     * @param array<string, mixed> $uriVariables
     * @param array<string, mixed> $context
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Circle
    {
        if (!$data instanceof Circle) {
            throw new NotFoundHttpException(sprintf('circle %s not found (must be iri)', $data->id));
        }
        /** @var array{
         *     logo: ?UploadedFile,
         *     removeLogo: bool,
         *     cover: ?UploadedFile,
         *     removeCover: bool,
         * } $input
         */
        $input = $context['info']->variableValues['input'];
        $circle = ($this->updateCircle)(
            circle: $data,
            logoFile: $input['logo'] ?? null,
            removeLogo: $input['removeLogo'] ?? null,
            coverFile: $input['cover'] ?? null,
            removeCover: $input['removeCover'] ?? null,
        );
        $this->entityManager->flush();

        return $this->statePersistProcessorDecorated->process($circle, $operation, $uriVariables, $context);
    }
}
