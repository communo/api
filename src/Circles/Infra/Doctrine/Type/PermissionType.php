<?php

declare(strict_types=1);

namespace App\Circles\Infra\Doctrine\Type;

use App\Circles\Entity\CirclePermissionType;
use App\Shared\Infra\Doctrine\Type\AbstractEnumType;

class PermissionType extends AbstractEnumType
{
    public const NAME = 'circlePermissionType';

    public static function getEnumsClass(): string
    {
        return CirclePermissionType::class;
    }

    public function getName(): string
    {
        return self::NAME;
    }
}
