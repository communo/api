<?php

declare(strict_types=1);

namespace App\Circles\Repository;

use App\Circles\Entity\CircleMembership;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CircleMembership|null find($id, $lockMode = null, $lockVersion = null)
 * @method CircleMembership|null findOneBy(array $criteria, array $orderBy = null)
 * @method CircleMembership[] findAll()
 * @method CircleMembership[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @extends ServiceEntityRepository<CircleMembership>
 */
class CircleMembershipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CircleMembership::class);
    }

    public function save(CircleMembership $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
