<?php

declare(strict_types=1);

namespace App\Circles\UseCase;

use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\Circle;
use App\Circles\Entity\CircleMembership;
use App\Circles\Entity\CirclePermissionType;
use Doctrine\ORM\EntityManagerInterface;

class CreateCircleOwner
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function __invoke(
        User $user,
        Circle $circle
    ): CircleMembership {
        $membership = new CircleMembership();
        $membership->setUser($user);
        $membership->setCircle($circle);
        $membership->setStatus(CircleMembership::STATUS_OK);
        $membership->setPermission(CirclePermissionType::Owner);
        $this->entityManager->persist($membership);

        return $membership;
    }
}
