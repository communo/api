<?php

declare(strict_types=1);

namespace App\Circles\UseCase;

use App\Circles\Entity\CircleMembership;
use App\Circles\Repository\CircleMembershipRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Workflow\Event\CompletedEvent;

class PromoteToOwner
{
    public function __construct(
        private Security $security,
        private CircleMembershipRepository $circleMembershipRepository
    ) {
    }

    public function onWorkflowCompleted(CompletedEvent $event)
    {
        $membership = $event->getSubject();
        assert($membership instanceof CircleMembership);
        $currentUserMembership = $this->circleMembershipRepository->findOneBy([
            'circle' => $membership->getCircle(),
            'user' => $this->security->getUser(),
        ]);
        $currentUserMembership->setPermission(CircleMembership::PERMISSION_ADMINISTRATOR);
        $this->circleMembershipRepository->save($currentUserMembership);
    }
}
