<?php

declare(strict_types=1);

namespace App\Circles\UseCase;

use ApiPlatform\Validator\Exception\ValidationException;
use App\Circles\Entity\Circle;
use App\Circles\Entity\CircleCover;
use App\Circles\Entity\CircleLogo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UpdateCircle
{
    public function __construct(
        private ValidatorInterface $validator,
    ) {
    }

    public function __invoke(
        Circle $circle,
        ?UploadedFile $logoFile = null,
        ?bool $removeLogo = false,
        ?UploadedFile $coverFile = null,
        ?bool $removeCover = false,
    ): Circle {
        if ($logoFile) {
            $logo = new CircleLogo();
            $logo->setImageFile($logoFile);
            $circle->setLogo($logo);
        } elseif ($removeLogo) {
            $circle->setLogo(null);
        }
        if ($coverFile) {
            $cover = new CircleCover();
            $cover->setImageFile($coverFile);
            $circle->setCover($cover);
        } elseif ($removeCover) {
            $circle->setCover(null);
        }
        if ($circle->getLatitude() && $circle->getLongitude()) {
            $circle->setLocation(sprintf(
                'SRID=4326;POINTZ(%s %s 0)',
                $circle->getLongitude(),
                $circle->getLatitude()
            ));
        }

        $errors = $this->validator->validate($circle);
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        return $circle;
    }
}
