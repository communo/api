<?php

declare(strict_types=1);

namespace App\Pooling\Application\EventSubscriber\Material\Booking;

use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Notifier\ChatterInterface;
use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Contracts\Translation\TranslatorInterface;

class ApplyWorkflowSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private string $defaultSender,
        private MailerInterface $mailer,
        private TranslatorInterface $translator,
        private string $clientBaseUrl,
        private ChatterInterface $chatter
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.material_booking.completed.apply' => 'onCompleted',
        ];
    }

    public function onCompleted(Event $event): void
    {
        /** @var MaterialBooking $materialBooking */
        $materialBooking = $event->getSubject();
        $mainOwner = $materialBooking->getMaterial()->getMainOwner();
        $owners = $materialBooking->getMaterial()?->getOwnerUserList();
        $applicant = $materialBooking->getUser();
        if ($applicant) {
            foreach ($owners as $owner) {
                $language = $owner->getSettings()?->getLanguage();
                $subject = $this->translator->trans(
                    id: 'email.material.booking.applied.subject',
                    parameters: [
                        'materialName' => $materialBooking->getMaterial()?->getName(),
                    ],
                    locale: $language
                );
                $email = (new TemplatedEmail())
                    ->from(new Address($this->defaultSender, 'Communo'))
                    ->replyTo(new Address($applicant->getEmail(), $applicant->getFullname(byPassSettings: true)))
                    ->to(new Address($owner->getEmail(), $owner->getFullname(byPassSettings: true)))
                    ->subject($subject)
                    ->htmlTemplate('emails/materials/booking/applied.html.twig')
                    ->textTemplate('emails/materials/booking/applied.txt.twig')
                    ->context([
                        'applicant' => $applicant,
                        'firstname' => $owner->getFirstname(),
                        'booking' => $materialBooking,
                        'language' => $language,
                        'material' => $materialBooking->getMaterial(),
                        'actions' => [[
                            'text' => $this->translator->trans(
                                id: 'email.material.booking.applied.action.label',
                                locale: $language
                            ),
                            'url' => sprintf('%s/my/bookings/%s', $this->clientBaseUrl, $materialBooking->getSlug()),
                        ]],
                    ]);
                $this->mailer->send($email);
            }
            $chatMessage = new ChatMessage(sprintf(
                '%s > %s : %s',
                $applicant->getFullname(),
                $owner->getFullname(),
                $subject
            ));
            $this->chatter->send($chatMessage);
        }
    }
}
