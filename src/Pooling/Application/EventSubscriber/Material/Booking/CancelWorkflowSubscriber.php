<?php

declare(strict_types=1);

namespace App\Pooling\Application\EventSubscriber\Material\Booking;

use App\AccountManagement\Entity\User\User;
use App\Chat\Handler\Discussion\AddMessageHandler;
use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Contracts\Translation\TranslatorInterface;

class CancelWorkflowSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly string $defaultSender,
        private readonly string $defaultLocale,
        private MailerInterface $mailer,
        private TranslatorInterface $translator,
        private TokenStorageInterface $tokenStorage,
        private AddMessageHandler $addMessage
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.material_booking.completed.cancel' => 'onCompleted',
        ];
    }

    public function onCompleted(Event $event): void
    {
        /** @var MaterialBooking $materialBooking */
        $materialBooking = $event->getSubject();
        $applicant = $materialBooking->getUser();

        if ($applicant) {
            /** @var User $initiatorUser */
            $initiatorUser = $this->tokenStorage->getToken()?->getUser();
            foreach ($materialBooking->getMaterial()?->getOwnerUserList() as $owner) {
                $recipient = $applicant;
                $canceledBy = 'Owner';
                if ($initiatorUser === $applicant) {
                    $recipient = $owner;
                    $canceledBy = 'Applicant';
                }
                $language = $owner->getSettings()?->getLanguage();
                $dateFormatter = new \IntlDateFormatter(
                    locale: $recipient->getSettings()?->getLanguage() ?? $this->defaultLocale,
                    dateType: \IntlDateFormatter::SHORT,
                    timeType: \IntlDateFormatter::NONE
                );
                $email = (new TemplatedEmail())
                    ->from(new Address($this->defaultSender, 'Communo'))
                    ->replyTo(new Address($initiatorUser->getEmail(), $initiatorUser->getFullname(byPassSettings: true)))
                    ->to(new Address($recipient->getEmail(), $recipient->getFullname(byPassSettings: true)))
                    ->subject($this->translator->trans(
                        id: sprintf('email.material.booking.canceledBy%s.subject', $canceledBy),
                        parameters: [
                            'bookingDate' => $dateFormatter->format($materialBooking->getStartDate()),
                            'userName' => $initiatorUser->getFirstname(),
                            'materialName' => $materialBooking->getMaterial()?->getName(),
                        ],
                        locale: $language
                    ))
                    ->htmlTemplate('emails/materials/booking/canceled.html.twig')
                    ->textTemplate('emails/materials/booking/canceled.txt.twig')
                    ->context([
                        'booking' => $materialBooking,
                        'canceledBy' => $canceledBy,
                        'language' => $language,
                        'material' => $materialBooking->getMaterial(),
                        'recipient' => $recipient,
                        'initiatorUser' => $initiatorUser,
                    ]);
                $this->mailer->send($email);
                ($this->addMessage)(
                    discussion: $materialBooking->getDiscussion(),
                    content: 'pooling.booking.discussion.automaticMessages.canceled',
                    parameters: [
                        'userName' => $initiatorUser->getFirstname(),
                        'materialName' => $materialBooking->getMaterial()?->getName(),
                        'context' => $initiatorUser->getGender(),
                    ]
                );
            }
        }
    }
}
