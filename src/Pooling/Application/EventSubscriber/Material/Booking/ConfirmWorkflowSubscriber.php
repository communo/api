<?php

declare(strict_types=1);

namespace App\Pooling\Application\EventSubscriber\Material\Booking;

use App\AccountManagement\Entity\User\User;
use App\Chat\Handler\Discussion\AddMessageHandler;
use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConfirmWorkflowSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private AddMessageHandler $addMessage,
        private string $defaultSender,
        private MailerInterface $mailer,
        private TranslatorInterface $translator,
        private TokenStorageInterface $tokenStorage
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.material_booking.completed.confirm' => 'onCompleted',
        ];
    }

    public function onCompleted(Event $event): void
    {
        /** @var MaterialBooking $materialBooking */
        $materialBooking = $event->getSubject();
        $owners = $materialBooking->getMaterial()?->getOwnerUserList();
        $applicant = $materialBooking->getUser();
        /** @var User $initiatorUser */
        $initiatorUser = $this->tokenStorage->getToken()?->getUser();

        if (count($owners) > 0 && $applicant) {
            foreach ($owners as $owner) {
                $language = $owner->getSettings()?->getLanguage();
                $email = (new TemplatedEmail())
                    ->from(new Address($this->defaultSender, 'Communo'))
                    ->replyTo(new Address($owner->getEmail(), $owner->getFullname(byPassSettings: true)))
                    ->to(new Address($applicant->getEmail(), $applicant->getFullname(byPassSettings: true)))
                    ->subject($this->translator->trans(
                        id: 'email.material.booking.confirmed.subject',
                        parameters: [
                            'materialName' => $materialBooking->getMaterial()?->getName(),
                        ],
                        locale: $language
                    ))
                    ->htmlTemplate('emails/materials/booking/confirmed.html.twig')
                    ->textTemplate('emails/materials/booking/confirmed.txt.twig')
                    ->context([
                        'owner' => $initiatorUser,
                        'applicant' => $applicant,
                        'booking' => $materialBooking,
                        'language' => $language,
                        'material' => $materialBooking->getMaterial(),
                    ]);
                $this->mailer->send($email);
                ($this->addMessage)(
                    discussion: $materialBooking->getDiscussion(),
                    content: 'pooling.booking.discussion.automaticMessages.confirmed',
                    parameters: [
                        'userName' => $owner->getFirstname(),
                        'materialName' => $materialBooking->getMaterial()?->getName(),
                        'context' => $owner->getGender(),
                        'period' => sprintf(
                            '%s => %s',
                            $materialBooking->getStartDate()->format('d/m/y'),
                            $materialBooking->getEndDate()->format('d/m/Y'),
                        ),
                    ],
                );
            }
        }
    }
}
