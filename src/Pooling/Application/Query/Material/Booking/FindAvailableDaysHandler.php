<?php

declare(strict_types=1);

namespace App\Pooling\Application\Query\Material\Booking;

use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use App\Pooling\Domain\Model\Material\Material;
use App\Pooling\Infra\Doctrine\Repository\MaterialBookingRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class FindAvailableDaysHandler
{
    public function __construct(private MaterialBookingRepository $bookingRepository)
    {
    }

    /**
     * @return array<string, \DateTime>
     */
    public function __invoke(Material $material, \DateTimeInterface $startDate, \DateTimeInterface $endDate, ?MaterialBooking $materialBooking): array
    {
        $existingBookings = $this->bookingRepository->findByMaterialBetweenDates(
            $material->getId(),
            $startDate,
            $endDate,
            $materialBooking
        );

        $availableDates = [];
        $from = new \DateTime($startDate->format('Y-m-d'));
        while ($from <= $endDate) {
            $availableDates[$from->format('Y-m-d')] = clone $from;
            $from->modify('+1day');
        }

        foreach ($existingBookings as $existingBooking) {
            $from = new \DateTime($existingBooking['start_date']);
            $to = new \DateTime($existingBooking['end_date']);
            while ($from <= $to) {
                if (array_key_exists($from->format('Y-m-d'), $availableDates)) {
                    unset($availableDates[$from->format('Y-m-d')]);
                }
                $from->modify('+1day');
            }
        }

        return $availableDates;
    }
}
