<?php

declare(strict_types=1);

namespace App\Pooling\Application\Query\Material\Booking;

use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use App\Pooling\Domain\Model\Material\Booking\MaterialBookingDatePeriod;

class FindPeriodsHandler
{
    /**
     * @param array<int, \DateTime> $dates
     *
     * @return array<int, MaterialBookingDatePeriod>
     */
    public function __invoke(MaterialBooking $materialBooking, array $dates): array
    {
        $datePeriods = [];
        $streak = 0;
        $firstDayOfPeriod = null;
        foreach ($dates as $date) {
            if ($streak === 0) {
                $firstDayOfPeriod = $date;
            }
            $streak++;
            if (!in_array(self::tomorrow($date), $dates, false)) { // tomorrow is not in array
                $datePeriods[] = new MaterialBookingDatePeriod($materialBooking, $firstDayOfPeriod, $date);
                $streak = 0;
            }
        }

        return $datePeriods;
    }

    private static function tomorrow(\DateTime $today): \DateTime
    {
        $tomorrow = clone $today;
        $tomorrow->modify('+ 1 day');

        return $tomorrow;
    }
}
