<?php

declare(strict_types=1);

namespace App\Pooling\Application\Query\Material\Pricing;

use App\AccountManagement\Entity\User\User;
use App\Pooling\Application\Query\Material\Booking\FindAvailableDaysHandler;
use App\Pooling\Application\Query\Material\Booking\FindPeriodsHandler;
use App\Pooling\Application\Security\Voter\MaterialVoter;
use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use App\Pooling\Domain\Model\Material\Material;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class EstimatePriceHandler
{
    public function __construct(
        private FindAvailableDaysHandler $availableDaysHandler,
        private FindPeriodsHandler $findPeriodHandler,
        private FindPricingHandler $findPricingHandler,
        private MaterialVoter $materialVoter
    ) {
    }

    public function __invoke(MaterialBooking $materialBooking, Material $material, \DateTimeInterface $startDate, \DateTimeInterface $endDate, User $user): float
    {
        $price = 0.0;
        $availableDays = ($this->availableDaysHandler)($material, $startDate, $endDate, $materialBooking);
        if (count($availableDays) === 0) {
            throw new \Exception('not available at this date');
        }

        $periods = ($this->findPeriodHandler)($materialBooking, array_values($availableDays));
        $pricing = $this->materialVoter->isOwner($user, $material) ? null : ($this->findPricingHandler)($material, $user);
        foreach ($periods as $period) {
            $materialBooking->addPeriod($period);
            $numberOfDays = $period->getStartDate()->diff($period->getEndDate())->days + 1;
            $period->setPrice($pricing ? $numberOfDays * $pricing->getValue() : 0);
            $price += $period->getPrice();
        }

        return $price;
    }
}
