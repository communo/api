<?php

declare(strict_types=1);

namespace App\Pooling\Application\Query\Material\Pricing;

use App\AccountManagement\Entity\User\User;
use App\Pooling\Domain\Model\Material\Material;
use App\Pooling\Domain\Model\Material\MaterialPricing;
use App\Pooling\Infra\Doctrine\Repository\PricingRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class FindPricingHandler
{
    public function __construct(
        private PricingRepository $pricingRepository,
    ) {
    }

    public function __invoke(Material $material, ?User $user): ?MaterialPricing
    {
        $pricings = $this->pricingRepository->findForMaterialAndUser($material, $user);
        $bestPrice = new MaterialPricing();
        $bestPrice->setValue($material->getPrice() ?? null);
        foreach ($pricings as $pricing) {
            if (!$bestPrice || $bestPrice->getValue() > $pricing->getValue()) {
                $bestPrice = $pricing;
            }
            if ($bestPrice->getValue() === 0.0) {
                break;
            }
        }

        return $bestPrice;
    }
}
