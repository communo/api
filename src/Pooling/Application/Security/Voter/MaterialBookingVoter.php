<?php

declare(strict_types=1);

namespace App\Pooling\Application\Security\Voter;

use App\AccountManagement\Entity\User\User;
use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use App\Pooling\Domain\Model\Material\Material;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MaterialBookingVoter extends Voter
{
    public const ANY_PART = 'BOOKING_ANY_PART';
    public const OWNER = 'BOOKING_OWNER';
    public const APPLICANT = 'BOOKING_APPLICANT';

    public function __construct(private MaterialVoter $materialVoter)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::OWNER, self::APPLICANT, self::ANY_PART])
            && $subject instanceof MaterialBooking;
    }

    /**
     * @param MaterialBooking $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        \assert($subject instanceof MaterialBooking);
        $material = $subject->getMaterial();
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User || !$material instanceof Material) {
            return false;
        }
        $canManage = $this->materialVoter->isOwner($user, $material);
        $isApplicant = $subject->getUser()?->getId() === $user->getId();

        return match ($attribute) {
            self::OWNER => $canManage,
            self::APPLICANT => $isApplicant,
            self::ANY_PART => $canManage || $isApplicant,
            default => false,
        };
    }
}
