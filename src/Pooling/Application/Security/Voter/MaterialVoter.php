<?php

declare(strict_types=1);

namespace App\Pooling\Application\Security\Voter;

use ApiPlatform\Api\IriConverterInterface;
use App\AccountManagement\Entity\User\User;
use App\Pooling\Domain\Model\Material\Material;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MaterialVoter extends Voter
{
    public const BORROW = 'borrow';
    public const EDIT = 'edit';
    public const DELETE = 'delete';

    public function __construct(private IriConverterInterface $iriConverter)
    {
    }

    /**
     * A user can manage a Material if he/she :
     * - has a UserMaterialOwnership or
     * - is Admin or Owner of a circle which has a CircleMaterialOwnership
     * - or is mainOwner
     */
    public function isOwner(User $user, Material $material)
    {
        $userGrantedOwnerIds = [$this->iriConverter->getIriFromResource($user)];
        foreach ($user->getCircleMemberships() as $circleMembership) {
            $userGrantedOwnerIds[] = $this->iriConverter->getIriFromResource($circleMembership->getCircle());
        }
        foreach ([...$material->getOwnerships(), $material->getMainOwnership()] as $ownership) {
            if (in_array($ownership?->getOwnerIRI(), $userGrantedOwnerIds)) {
                return true;
            }
        }

        return false;
    }

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::EDIT, self::DELETE, self::BORROW], true)
            && $subject instanceof Material;
    }

    /**
     * @param Material $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }

        return match ($attribute) {
            self::EDIT, self::DELETE => $this->isOwner($user, $subject),
            self::BORROW => $this->canBorrow($user, $subject),
            default => false,
        };
    }

    /**
     * A user can borrow a Material if :
     * - material is published
     * - he/she is not the mainOwner
     * - he/she is allowed by one of the following :
     *   - material allows public borrow
     *   - he/she is in a circle allowed by a MaterialPricing
     */
    private function canBorrow(User $user, Material $material): bool
    {
        if (!$material->isPublished()) {
            return false;
        }
        // if user is direct mainOwner : non-sense
        if ($material->getMainOwnershipUser()?->getId() === $user->getId()) {
            return false;
        }

        // if user is coOwner : ok
        if ($this->isOwner($user, $material)) {
            return true;
        }

        $pricings = $material->getPricings();
        if ($pricings->count() === 0) {
            return true;
        }

        // check circle pricings
        foreach ($pricings as $pricing) {
            if ($pricing->getCircle() === null) { // material allows public borrow: ok
                return true;
            }
            if ($pricing->getCircle()->getMembershipByUser($user)) {
                return true;
            }
        }

        return false;
    }
}
