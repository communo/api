<?php

declare(strict_types=1);

namespace App\Pooling\Domain\Model\Material\Booking;

use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\DeleteMutation;
use ApiPlatform\Metadata\GraphQl\Mutation;
use ApiPlatform\Metadata\GraphQl\Query;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use App\AccountManagement\Entity\User\User;
use App\Chat\Entity\Discussion\Discussion;
use App\Pooling\Domain\Model\Material\Material;
use App\Pooling\Infra\ApiPlatform\Filter\Material\Booking\OnlyMinesFilter;
use App\Pooling\Infra\ApiPlatform\Resolver\Material\Booking\ChangeDateResolver;
use App\Pooling\Infra\ApiPlatform\Resolver\Material\Booking\ChangeStatusResolver;
use App\Pooling\Infra\ApiPlatform\Resolver\Material\Booking\EstimateResolver;
use App\Pooling\Infra\ApiPlatform\Resolver\Material\Booking\MaterialBookingResolver;
use App\Pooling\Infra\ApiPlatform\Resolver\Material\Booking\UnavailableResolver;
use App\Pooling\Infra\Doctrine\Repository\MaterialBookingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Sluggable\Handler\RelativeSlugHandler;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [],
    graphQlOperations: [
        new Query(
            resolver: MaterialBookingResolver::class,
            security: 'is_granted(\'BOOKING_ANY_PART\', object)',
            name: 'item_query'
        ),
        new QueryCollection(
            name: 'collection_query'
        ),
        new QueryCollection(
            filters: [
                OnlyMinesFilter::class,
            ],
            name: 'asOwner',
        ),
        new Mutation(name: 'create'),
        new Mutation(security: 'is_granted(\'BOOKING_ANY_PART\', object)', name: 'update'),
        new DeleteMutation(security: 'is_granted(\'BOOKING_OWNER\', object)', name: 'delete'),
        new Mutation(
            resolver: ChangeStatusResolver::class,
            args: [
                'id' => ['type' => 'ID!'],
                'transition' => ['type' => 'String', 'description' => 'What transition do you want to run ?'],
                'message' => ['type' => 'String', 'description' => 'Is there a message to attach ?'],
            ],
            name: 'changeStatus'
        ),
        new Mutation(
            resolver: ChangeDateResolver::class,
            args: [
                'id' => ['type' => 'ID!'],
                'startDate' => ['type' => 'String'],
                'endDate' => ['type' => 'String'],
            ],
            name: 'changeDate'
        ),
        new Mutation(
            resolver: EstimateResolver::class,
            args: [
                'materialId' => ['type' => 'String'],
                'startDate' => ['type' => 'String'],
                'endDate' => ['type' => 'String'],
            ],
            name: 'estimate'
        ),
        new Mutation(
            resolver: UnavailableResolver::class,
            args: [
                'materialId' => ['type' => 'String'],
                'startDate' => ['type' => 'String'],
                'endDate' => ['type' => 'String'],
            ],
            name: 'unavailable'
        ),
        new Mutation(
            args: [
                'id' => ['type' => 'ID!'],
                'comment' => ['type' => 'String!'],
            ],
            name: 'comment'
        ),
    ],
)]
#[ORM\Entity(repositoryClass: MaterialBookingRepository::class)]
#[ApiFilter(filterClass: SearchFilter::class, properties: [
    'user.slug' => 'exact',
    'material.slug' => 'exact',
    'status' => 'exact',
])]
#[ApiFilter(DateFilter::class, properties: ['endDate', 'startDate'])]
class MaterialBooking implements TimestampableInterface
{
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    public const STATUS_ESTIMATING = 'estimating';
    public const STATUS_PENDING = 'pending';
    public const STATUS_CANCELED = 'canceled';
    public const STATUS_CONFIRMED = 'confirmed';
    public const STATUS_CLOSED = 'closed';
    public const TRANSITION_APPLY = 'apply';
    public const TRANSITION_CANCEL = 'cancel';
    public const TRANSITION_CONFIRM = 'confirm';
    public const TRANSITION_CLOSE = 'close';

    #[ApiProperty(identifier: true)]
    #[ORM\Column(type: 'string', length: 255)]
    #[Gedmo\Slug(fields: ['slug', 'startDate'], dateFormat: 'd-m-Y', separator: '-')]
    #[Gedmo\SlugHandler(class: RelativeSlugHandler::class, options: [
        'relationField' => 'material',
        'relationSlugField' => 'slug',
        'separator' => '-',
    ])]
    protected string $slug;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    #[ApiProperty(identifier: false)]
    private ?Uuid $id = null;

    #[ORM\ManyToOne(targetEntity: Material::class, inversedBy: 'bookings')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private ?Material $material;

    #[ORM\Column(type: 'date')]
    private \DateTimeInterface $startDate;

    #[ORM\Column(type: 'date')]
    private \DateTimeInterface $endDate;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'materialBookings')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private User $user;

    #[ORM\Column(type: 'string', length: 15, options: ['default' => self::STATUS_ESTIMATING])]
    private string $status = self::STATUS_ESTIMATING;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $comment;

    /**
     * @var Collection<int, MaterialBookingDatePeriod>
     */
    #[ORM\OneToMany(
        mappedBy: 'booking',
        targetEntity: MaterialBookingDatePeriod::class,
        cascade: ['persist', 'remove'],
        fetch: 'EAGER',
        orphanRemoval: true
    )]
    private Collection $periods;

    #[ORM\Column(type: 'float', nullable: true)]
    private float $price;

    /**
     * @var Collection<int, Rating>
     */
    #[ORM\OneToMany(mappedBy: 'booking', targetEntity: Rating::class, orphanRemoval: true)]
    private Collection $ratings;

    /**
     * @var array<int, string>|null
     */
    private ?array $statusTransitionAvailables = [];

    #[ORM\OneToOne(targetEntity: Discussion::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    private ?Discussion $discussion;

    public function __construct(?Material $material = null)
    {
        $this->material = $material;
        $this->periods = new ArrayCollection();
    }

    /**
     * @return string[]
     */
    public static function getTransitions(): array
    {
        return [self::TRANSITION_APPLY, self::TRANSITION_CANCEL, self::TRANSITION_CONFIRM, self::TRANSITION_CLOSE];
    }

    /**
     * @return string[]
     */
    public static function getTerminatedStatuses(): array
    {
        return [self::STATUS_CANCELED, self::STATUS_CLOSED];
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getMaterial(): ?Material
    {
        return $this->material;
    }

    public function setMaterial(?Material $material): self
    {
        $this->material = $material;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function getStartDate(): \DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function isPast(): bool
    {
        return $this->getEndDate() > new \DateTime();
    }

    public function getEndDate(): \DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return MaterialBookingDatePeriod[]
     */
    public function getPeriods(): array
    {
        return $this->periods->getValues();
    }

    public function addPeriod(MaterialBookingDatePeriod $period): self
    {
        if (!$this->periods->contains($period)) {
            $this->periods[] = $period;
            $period->setBooking($this);
        }

        return $this;
    }

    public function removePeriod(MaterialBookingDatePeriod $period): self
    {
        if ($this->periods->removeElement($period)) {
            // set the owning side to null (unless already changed)
            if ($period->getBooking() === $this) {
                $period->setBooking(null);
            }
        }

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return array<int, string>
     */
    public function getStatusTransitionAvailables(): array
    {
        return $this->statusTransitionAvailables;
    }

    /**
     * @param array<int, string> $enabledTransitions
     *
     * @return void
     */
    public function setStatusTransitionAvailables(array $enabledTransitions)
    {
        $this->statusTransitionAvailables = $enabledTransitions;
    }

    public function getDiscussion(): ?Discussion
    {
        return $this->discussion;
    }

    public function setDiscussion(Discussion $discussion): self
    {
        $this->discussion = $discussion;

        return $this;
    }

    /**
     * @return Collection<int, Rating>
     */
    public function getRatings(): Collection
    {
        return $this->ratings;
    }

    /**
     * @param Collection<int, Rating> $ratings
     */
    public function setRatings(Collection $ratings): void
    {
        $this->ratings = $ratings;
    }
}
