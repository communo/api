<?php

declare(strict_types=1);

namespace App\Pooling\Domain\Model\Material\Booking;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Query;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [],
    paginationEnabled: false,
    graphQlOperations: [
        new Query(),
        new QueryCollection(),
    ],
)]
#[ORM\Entity]
class MaterialBookingDatePeriod
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    private ?Uuid $id = null;

    #[Groups(['read'])]
    #[ORM\Column(type: 'date')]
    private \DateTimeInterface $startDate;

    #[Groups(['read'])]
    #[ORM\Column(type: 'date')]
    private \DateTimeInterface $endDate;

    #[ORM\ManyToOne(targetEntity: MaterialBooking::class, inversedBy: 'periods')]
    #[ORM\JoinColumn(nullable: false)]
    private MaterialBooking $booking;

    #[Groups(['read'])]
    #[ORM\Column(type: 'float', nullable: true)]
    private ?float $price;

    public function __construct(MaterialBooking $booking, \DateTime $startDate, \DateTime $endDate)
    {
        $this->booking = $booking;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getStartDate(): \DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): \DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getBooking(): MaterialBooking
    {
        return $this->booking;
    }

    public function setBooking(?MaterialBooking $booking): self
    {
        $this->booking = $booking;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }
}
