<?php

declare(strict_types=1);

namespace App\Pooling\Domain\Model\Material\Booking;

use ApiPlatform\Metadata\ApiResource;
use App\AccountManagement\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [],
)]
#[ORM\Entity]
class Rating implements TimestampableInterface
{
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    private ?Uuid $id = null;

    #[ORM\Column(type: 'smallint')]
    private int $value;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'ratingsSent')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private ?User $author;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'ratings')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private User $user;

    #[ORM\ManyToOne(targetEntity: MaterialBooking::class, inversedBy: 'ratings')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private MaterialBooking $booking;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBooking(): MaterialBooking
    {
        return $this->booking;
    }

    public function setBooking(MaterialBooking $booking): self
    {
        $this->booking = $booking;

        return $this;
    }
}
