<?php

declare(strict_types=1);

namespace App\Pooling\Domain\Model\Material;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\DeleteMutation;
use ApiPlatform\Metadata\GraphQl\Mutation;
use ApiPlatform\Metadata\GraphQl\Query;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use App\AccountManagement\Entity\User\User;
use App\AccountManagement\Entity\User\UserList;
use App\Circles\Entity\Circle;
use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use App\Pooling\Domain\Model\Material\Ownership\AbstractMaterialOwnership;
use App\Pooling\Domain\Model\Material\Ownership\CircleMaterialOwnership;
use App\Pooling\Domain\Model\Material\Ownership\UserMaterialOwnership;
use App\Pooling\Infra\ApiPlatform\Filter\Material\OnlyMinesFilter;
use App\Pooling\Infra\ApiPlatform\Filter\Material\OwnerFilter;
use App\Pooling\Infra\ApiPlatform\Filter\Material\PublishedFilter;
use App\Pooling\Infra\ApiPlatform\Filter\Material\SearchTermFilter;
use App\Pooling\Infra\ApiPlatform\Filter\Material\UserFilter;
use App\Pooling\Infra\ApiPlatform\Resolver\Material\ChangeStatusResolver;
use App\Pooling\Infra\ApiPlatform\Resolver\Material\MaterialCollectionResolver;
use App\Pooling\Infra\ApiPlatform\Resolver\Material\MaterialMutationResolver;
use App\Pooling\Infra\ApiPlatform\Resolver\Material\MaterialResolver;
use App\Pooling\Infra\ApiPlatform\State\Processor\Material\CreateMaterialProcessor;
use App\Pooling\Infra\ApiPlatform\State\Processor\Material\DeleteMaterialProcessor;
use App\Pooling\Infra\ApiPlatform\State\Processor\Material\ForkMaterialProcessor;
use App\Pooling\Infra\Doctrine\Repository\MaterialRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Jsor\Doctrine\PostGIS\Types\PostGISType;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [],
    graphQlOperations: [
        new Query(
            resolver: MaterialResolver::class,
            name: 'item_query'
        ),
        new QueryCollection(
            resolver: MaterialCollectionResolver::class,
            filters: [
                UserFilter::class,
            ],
            name: 'collection_query',
        ),
        new QueryCollection(
            resolver: MaterialCollectionResolver::class,
            extraArgs: [
                'searchTerms' => ['type' => 'String!'],
            ],
            filters: [
                SearchTermFilter::class,
                PublishedFilter::class,
                UserFilter::class,
            ],
            name: 'search',
        ),
        new QueryCollection(
            filters: [
                OnlyMinesFilter::class,
            ],
            name: 'my'
        ),
        new QueryCollection(
            extraArgs: [
                'owner' => ['type' => 'ID!'],
            ],
            filters: [
                OwnerFilter::class,
                PublishedFilter::class,
            ],
            name: 'owner'
        ),
        new Mutation(
            extraArgs: [
                'mainOwnership' => ['type' => 'MaterialOwnershipType'],
                'ownerships' => ['type' => '[MaterialOwnershipType]'],
            ],
            normalizationContext: ['groups' => ['material:read']],
            denormalizationContext: ['groups' => ['material:write']],
            security: 'is_authenticated()',
            name: 'create',
            processor: CreateMaterialProcessor::class
        ),
        new Mutation(
            resolver: MaterialMutationResolver::class,
            extraArgs: [
                'id' => ['type' => 'ID!'],
                'mainOwnership' => ['type' => 'MaterialOwnershipType'],
                'ownerships' => ['type' => '[MaterialOwnershipType]'],
            ],
            normalizationContext: ['groups' => ['material:read']],
            denormalizationContext: ['groups' => ['material:write']],
            security: "is_granted('edit', object)",
            name: 'update',
        ),
        new Mutation(
            args: [
                'id' => ['type' => 'ID!'],
            ],
            security: 'is_authenticated()',
            name: 'fork',
            processor: ForkMaterialProcessor::class
        ),
        new Mutation(
            resolver: ChangeStatusResolver::class,
            args: [
                'id' => ['type' => 'ID!'],
                'transition' => ['type' => 'String!'],
            ],
            security: "is_granted('edit', object)",
            name: 'changeStatus'
        ),
        new DeleteMutation(
            security: "is_granted('delete', object)",
            name: 'delete',
            processor: DeleteMaterialProcessor::class
        ),
    ]
)]
#[ORM\Entity(repositoryClass: MaterialRepository::class)]
#[ApiFilter(
    filterClass: OrderFilter::class,
    properties: [
        'distance' => 'ASC',
        'updatedAt' => 'DESC',
        'createdAt' => 'DESC',
        'name' => 'ASC',
    ]
)
]
#[ApiFilter(filterClass: SearchFilter::class, properties: [
    'name' => 'ipartial',
    'brand' => 'ipartial',
    'reference' => 'ipartial',
    'model' => 'ipartial',
    'status' => 'exact',
    'description' => 'ipartial',
    'category.name' => 'ipartial',
])]
class Material implements TimestampableInterface
{
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    public const TRANSITION_PUBLISH = 'publish';
    public const TRANSITION_PAUSE = 'pause';
    public const STATUS_DRAFT = 'draft';
    public const STATUS_PUBLISHED = 'published';
    public const STATUS_PAUSED = 'paused';
    public const SCOPE_PUBLIC = 'public';
    public const SCOPE_INTERNAL = 'internal';

    #[ApiProperty(writable: false, identifier: true)]
    #[ORM\Column(type: 'string', length: 255)]
    #[Gedmo\Slug(fields: ['name'])]
    protected string $slug;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    #[ApiProperty(identifier: false)]
    private ?Uuid $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $brand;

    /**
     * @var Collection<int, MaterialImage>
     */
    #[ORM\OneToMany(targetEntity: MaterialImage::class, mappedBy: 'material', orphanRemoval: true, cascade: ['persist'])]
    private Collection $images;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $reference;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $model;

    #[ORM\ManyToOne(targetEntity: MaterialCategory::class, inversedBy: 'materials')]
    #[ORM\JoinColumn(nullable: true)]
    private ?MaterialCategory $category;

    #[ORM\ManyToOne(targetEntity: Material::class, inversedBy: 'forks')]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private Material $forkedFrom;

    /**
     * @var Collection<int, Material>
     *                                When a material is "forked", it is cloned and the original material is set as forkedFrom
     */
    #[ORM\OneToMany(targetEntity: Material::class, mappedBy: 'forkedFrom')]
    private Collection $forks;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'materials')]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    /** @deprecated use mainOwnership */
    private User $mainOwner;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $description;

    #[ORM\Column(type: 'float', options: ['default' => 0])]
    private float $price;

    /**
     * @var Collection<int, MaterialPricing>
     */
    #[ORM\OneToMany(
        mappedBy: 'material',
        targetEntity: MaterialPricing::class,
        cascade: ['persist', 'remove'],
        orphanRemoval: true,
    )]
    private Collection $pricings;

    /**
     * @var Collection<int, UserList>
     */
    #[ORM\ManyToMany(targetEntity: UserList::class, inversedBy: 'materials')]
    private Collection $userLists;

    /**
     * @var Collection<int, MaterialBooking>
     */
    #[ORM\OneToMany(mappedBy: 'material', targetEntity: MaterialBooking::class, orphanRemoval: true)]
    private Collection $bookings;

    private ?float $bestPriceForUser = null;

    #[ApiProperty(writable: false)]
    #[Groups(['material:read'])]
    private ?float $distance = null;

    #[ORM\Column(type: 'string', length: 15, options: ['default' => self::STATUS_DRAFT])]
    #[ApiProperty(writable: false)]
    private string $status = self::STATUS_DRAFT;

    #[ORM\Column(type: 'string', length: 15, options: ['default' => self::SCOPE_PUBLIC])]
    private string $shareScope = self::SCOPE_PUBLIC;

    #[ORM\Column(type: PostGISType::GEOMETRY, options: ['geometry_type' => 'POINTZ', 'srid' => 4326], nullable: true)]
    private ?string $location;

    /**
     * @var Collection<int, AbstractMaterialOwnership>
     */
    #[ORM\OneToMany(
        mappedBy: 'material',
        targetEntity: AbstractMaterialOwnership::class,
        cascade: ['persist', 'remove'],
        orphanRemoval: true
    )]
    private Collection $ownerships;

    #[ORM\OneToOne(
        inversedBy: 'materialMainOwned',
        targetEntity: AbstractMaterialOwnership::class,
        cascade: ['persist', 'remove'],
        orphanRemoval: true
    )]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?AbstractMaterialOwnership $mainOwnership;

    /**
     * @var string[]|null
     */
    private ?array $userActionAvailables = [];

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->pricings = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->ownerships = new ArrayCollection();
        $this->userLists = new ArrayCollection();
        $this->mainOwnership = null;
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(?string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return Collection<int, MaterialImage>|MaterialImage[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(MaterialImage $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setMaterial($this);
        }

        return $this;
    }

    public function removeImage(MaterialImage $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getMaterial() === $this) {
                $image->setMaterial(null);
            }
        }

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(?string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getCategory(): ?MaterialCategory
    {
        return $this->category;
    }

    public function setCategory(?MaterialCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, MaterialPricing>|MaterialPricing[]
     */
    public function getPricings(): Collection
    {
        return $this->pricings;
    }

    public function addPricing(MaterialPricing $pricing): self
    {
        if (!$this->pricings->contains($pricing)) {
            $this->pricings[] = $pricing;
            $pricing->setMaterial($this);
        }

        return $this;
    }

    public function removePricing(MaterialPricing $pricing): self
    {
        $this->pricings->removeElement($pricing);

        return $this;
    }

    public function addBooking(MaterialBooking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setMaterial($this);
        }

        return $this;
    }

    public function removeBooking(MaterialBooking $booking): self
    {
        if ($this->bookings->removeElement($booking)) {
            // set the owning side to null (unless already changed)
            if ($booking->getMaterial() === $this) {
                $booking->setMaterial(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, MaterialBooking>|MaterialBooking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function getBestPriceForUser(): ?float
    {
        return $this->bestPriceForUser;
    }

    public function setBestPriceForUser(?float $bestPriceForUser): void
    {
        $this->bestPriceForUser = $bestPriceForUser;
    }

    /**
     * Return a user weither mainOwnership is a User or Circle MaterialOwnership type.
     */
    public function getMainOwner(): ?User
    {
        if ($this->mainOwnership instanceof UserMaterialOwnership) {
            return $this->mainOwnership->getUser();
        } elseif ($this->mainOwnership instanceof CircleMaterialOwnership) {
            return $this->mainOwnership->getCircle()->getOwners()->first();
        }

        return null;
    }

    /** @deprecated */
    public function setMainOwner(User $mainOwner): void
    {
        $this->mainOwner = $mainOwner;
    }

    public function getMainOwnership(): ?AbstractMaterialOwnership
    {
        return $this->mainOwnership;
    }

    public function setMainOwnership(?AbstractMaterialOwnership $mainOwnership): void
    {
        $this->mainOwnership = $mainOwnership;
        $mainOwnership->setMaterialMainOwned($this);
    }

    /**
     * @return Collection<int, AbstractMaterialOwnership>
     */
    public function getOwnerships(): Collection
    {
        return $this->ownerships;
    }

    /**
     * Return users from mainOwner and other ownerships for both user and circle ownership types.
     *
     * @return User[]>
     */
    public function getOwnerUserList(): array
    {
        $owners = [];
        foreach ([$this->mainOwnership, ...$this->ownerships] as $ownership) {
            if ($ownership instanceof UserMaterialOwnership) {
                $owners[] = $ownership->getUser();
                continue;
            }
            if ($ownership instanceof CircleMaterialOwnership) {
                $owners = [
                    ...$owners,
                    ...$ownership->getCircle()->getOwners(),
                ];
            }
        }

        return $owners;
    }

    /**
     * Return strictly main owner user.
     * Can be null if mainOwnership is not a UserMaterialOwnership (circle).
     */
    public function getMainOwnershipUser(): ?User
    {
        if ($this->mainOwnership instanceof UserMaterialOwnership) {
            return $this->mainOwnership->getUser();
        }

        return null;
    }

    /**
     * Return strictly mainOwner circle.
     * Can be null if mainOwnership is not a CircleMaterialOwnership (user).
     */
    public function getMainOwnershipCircle(): ?Circle
    {
        if ($this->mainOwnership instanceof CircleMaterialOwnership) {
            return $this->mainOwnership->getCircle();
        }

        return null;
    }

    public function addMaterialOwnership(AbstractMaterialOwnership $materialOwnership): static
    {
        if (!$this->ownerships->contains($materialOwnership)) {
            $this->ownerships->add($materialOwnership);
            $materialOwnership->setMaterial($this);
        }

        return $this;
    }

    public function removeMaterialOwnership(AbstractMaterialOwnership $materialOwnership): static
    {
        if ($this->ownerships->removeElement($materialOwnership)) {
            // set the owning side to null (unless already changed)
            if ($materialOwnership->getMaterial() === $this) {
                $materialOwnership->setMaterial(null);
            }
        }

        return $this;
    }

    public function getForkedFrom(): Material
    {
        return $this->forkedFrom;
    }

    public function setForkedFrom(Material $forkedFrom): void
    {
        $this->forkedFrom = $forkedFrom;
    }

    /**
     * @return Collection<int, Material>
     */
    public function getForks(): Collection
    {
        return $this->forks;
    }

    /**
     * @param Collection<int, Material> $forks
     */
    public function setForks(Collection $forks): void
    {
        $this->forks = $forks;
    }

    /**
     * @return string[]|null
     */
    public function getUserActionAvailables(): ?array
    {
        return $this->userActionAvailables;
    }

    /**
     * @param string[]|null $actions
     */
    public function setUserActionAvailables(?array $actions): void
    {
        $this->userActionAvailables = $actions;
    }

    public function isPublished(): bool
    {
        return $this->getStatus() === self::STATUS_PUBLISHED;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return Collection<int, UserList>
     */
    public function getUserLists(): Collection
    {
        return $this->userLists;
    }

    public function setUserLists(Collection $userLists): void
    {
        $this->userLists = $userLists;
    }

    public function addUserList(UserList $userList): self
    {
        if (!$this->userLists->contains($userList)) {
            $this->userLists[] = $userList;
        }

        return $this;
    }

    public function removeUserList(UserList $userList): self
    {
        $this->userLists->removeElement($userList);

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getLatitude(): ?float
    {
        if ($this->location) {
            return (float) explode(' ', substr($this->location, strpos($this->location, '(') + 1, -1))[1];
        }

        return null;
    }

    public function getLongitude(): ?float
    {
        if ($this->location) {
            return (float) explode(' ', substr($this->location, strpos($this->location, '(') + 1, -1))[0];
        }

        return null;
    }

    public function getDistance(): ?float
    {
        return $this->distance;
    }

    public function setDistance(?float $distance): void
    {
        $this->distance = $distance;
    }
}
