<?php

declare(strict_types=1);

namespace App\Pooling\Domain\Model\Material;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\DeleteMutation;
use ApiPlatform\Metadata\GraphQl\Mutation;
use App\Pooling\Infra\ApiPlatform\Resolver\Material\MaterialImageMutationResolver;
use App\Pooling\Infra\Doctrine\Repository\MaterialImageRepository;
use App\Shared\Entity\Image;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[Vich\Uploadable]
#[ApiResource(
    operations: [],
    graphQlOperations: [
        new Mutation(
            resolver: MaterialImageMutationResolver::class,
            args: [
                'material' => [
                    'type' => 'String!',
                ],
                'file' => [
                    'type' => 'Upload!',
                ],
                'dimensions' => [
                    'type' => 'Iterable',
                ],
                'imageName' => [
                    'type' => 'String',
                ],
                'mime' => [
                    'type' => 'String',
                ],
                'size' => [
                    'type' => 'Int',
                ],
            ],
            name: 'create',
        ),
        new Mutation(
            resolver: MaterialImageMutationResolver::class,
            args: [
                'id' => [
                    'type' => 'ID!',
                ],
                'file' => [
                    'type' => 'Upload!',
                ],
                'dimensions' => [
                    'type' => 'Iterable',
                ],
                'imageName' => [
                    'type' => 'String',
                ],
                'mime' => [
                    'type' => 'String',
                ],
                'size' => [
                    'type' => 'Int',
                ],
            ],
            name: 'update',
        ),
        new DeleteMutation(
            security: "is_granted('edit', object.getMaterial())",
            name: 'delete',
        ),
    ]
)]
#[ORM\Entity(repositoryClass: MaterialImageRepository::class)]
class MaterialImage extends Image
{
    #[Vich\UploadableField(
        mapping: 'pooling_image',
        fileNameProperty: 'imageName',
        size: 'size',
        mimeType: 'mime',
        originalName: 'originalName',
        dimensions: 'dimensions'
    )]
    protected ?File $imageFile = null;

    #[ORM\ManyToOne(targetEntity: Material::class, inversedBy: 'images')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Material $material;

    public function getMaterial(): ?Material
    {
        return $this->material;
    }

    public function setMaterial(?Material $material): self
    {
        $this->material = $material;

        return $this;
    }
}
