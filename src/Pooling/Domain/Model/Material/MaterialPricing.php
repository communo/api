<?php

declare(strict_types=1);

namespace App\Pooling\Domain\Model\Material;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Circles\Entity\Circle;
use App\Pooling\Infra\Doctrine\Repository\PricingRepository;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(operations: [])]
#[ORM\Entity(repositoryClass: PricingRepository::class)]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['value' => 'ASC'])]
class MaterialPricing implements TimestampableInterface
{
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    private ?Uuid $id = null;

    #[ORM\Column(type: 'float', nullable: true)]
    private ?float $value;

    #[ORM\ManyToOne(
        targetEntity: Material::class,
        inversedBy: 'pricings'
    )]
    #[ORM\JoinColumn(nullable: false)]
    private Material $material;

    #[ORM\ManyToOne(
        targetEntity: Circle::class,
        inversedBy: 'pricings'
    )]
    #[ORM\JoinColumn(nullable: true, onDelete: 'CASCADE')]
    private ?Circle $circle;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getMaterial(): ?Material
    {
        return $this->material;
    }

    public function setMaterial(?Material $material): self
    {
        $this->material = $material;

        return $this;
    }

    public function getCircle(): ?Circle
    {
        return $this->circle;
    }

    public function setCircle(?Circle $circle): self
    {
        $this->circle = $circle;

        return $this;
    }
}
