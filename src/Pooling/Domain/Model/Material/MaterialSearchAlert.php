<?php

declare(strict_types=1);

namespace App\Pooling\Domain\Model\Material;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\DeleteMutation;
use ApiPlatform\Metadata\GraphQl\Mutation;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use App\AccountManagement\Entity\User\User;
use App\Pooling\Infra\ApiPlatform\Filter\Material\SearchAlert\UserFilter;
use App\Pooling\Infra\ApiPlatform\State\Processor\Material\CreateMaterialSearchAlertProcessor;
use App\Pooling\Infra\Doctrine\Repository\MaterialSearchAlertRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: MaterialSearchAlertRepository::class)]
#[ApiResource(
    operations: [],
    graphQlOperations: [
        new Mutation(
            args: [
                'name' => ['type' => 'String!'],
            ],
            name: 'create',
            processor: CreateMaterialSearchAlertProcessor::class,
        ),
        new DeleteMutation(name: 'delete'),
        new QueryCollection(
            args: [
                'name' => ['type' => 'String'],
                'first' => ['type' => 'Int'],
                'last' => ['type' => 'Int'],
                'before' => ['type' => 'String'],
                'after' => ['type' => 'String'],
            ],
            filters: [
                UserFilter::class,
            ],
            name: 'collection_query',
        ),
    ]
)]
#[ApiFilter(filterClass: SearchFilter::class, properties: [
    'name' => 'iexact',
])]
class MaterialSearchAlert
{
    public ?string $_id = null;
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    private ?Uuid $id = null;
    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'materialSearchAlerts')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private User $user;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
