<?php

declare(strict_types=1);

namespace App\Pooling\Domain\Model\Material\Ownership;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Mutation;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use App\Pooling\Domain\Model\Material\Material;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorMap([
    'user' => UserMaterialOwnership::class,
    'circle' => CircleMaterialOwnership::class,
])]
#[ORM\Table(name: 'material_ownership')]
#[ApiResource(
    operations: [],
    paginationEnabled: false,
    graphQlOperations: [
        new QueryCollection(
            name: 'collection_query'
        ),
        new Mutation(
            name: 'create'
        ),
        new Mutation(
            name: 'update'
        ),
    ]
)]
abstract class AbstractMaterialOwnership
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    private ?Uuid $id = null;

    #[ORM\ManyToOne(inversedBy: 'ownerships')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Material $material = null;

    #[ORM\OneToOne(mappedBy: 'mainOwnership')]
    private ?Material $materialMainOwned = null;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getMaterial(): ?Material
    {
        return $this->material;
    }

    public function setMaterial(?Material $material): static
    {
        $this->material = $material;

        return $this;
    }

    public function getMaterialMainOwned(): ?Material
    {
        return $this->materialMainOwned;
    }

    public function setMaterialMainOwned(?Material $materialMainOwned): static
    {
        $this->materialMainOwned = $materialMainOwned;

        return $this;
    }

    abstract public function getOwnerIRI(): string;

    abstract public function getOwnerLabel(): string;
}
