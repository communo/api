<?php

declare(strict_types=1);

namespace App\Pooling\Domain\Model\Material\Ownership;

use ApiPlatform\Metadata\ApiResource;
use App\Circles\Entity\Circle;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'material_ownership_circle')]
#[ApiResource(operations: [])]
class CircleMaterialOwnership extends AbstractMaterialOwnership
{
    #[ORM\ManyToOne(inversedBy: 'circleOwnerships')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Circle $circle = null;

    public function getCircle(): ?Circle
    {
        return $this->circle;
    }

    public function setCircle(?Circle $circle): static
    {
        $this->circle = $circle;

        return $this;
    }

    public function getOwnerIRI(): string
    {
        return sprintf('/circles/%s', $this->circle->getSlug());
    }

    public function getOwnerLabel(): string
    {
        return $this->circle->getName();
    }
}
