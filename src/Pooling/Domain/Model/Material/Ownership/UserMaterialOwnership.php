<?php

declare(strict_types=1);

namespace App\Pooling\Domain\Model\Material\Ownership;

use ApiPlatform\Metadata\ApiResource;
use App\AccountManagement\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'material_ownership_user')]
#[ApiResource(
    operations: []
)]
class UserMaterialOwnership extends AbstractMaterialOwnership
{
    #[ORM\ManyToOne(inversedBy: 'userOwnerships')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?User $user = null;

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getOwnerIRI(): string
    {
        return sprintf('/users/%s', $this->user->getSlug());
    }

    public function getOwnerLabel(): string
    {
        return $this->user->getFullName();
    }
}
