<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Filter\Extension;

use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\Circle;
use App\Pooling\Domain\Model\Material\Material;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\SecurityBundle\Security;

class DistanceOrderExtension implements QueryCollectionExtensionInterface
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        ?Operation $operation = null,
        array $context = []
    ): void {
        if (!in_array($resourceClass, [Material::class, Circle::class]) || !in_array(
            $operation?->getName(),
            ['search', 'collection_query']
        )) {
            return;
        }

        /** @var User|null $user */
        $user = $this->security->getUser();
        if ($user && $user->getLocation()) {
            $rootAlias = $queryBuilder->getRootAliases()[0];

            $userLocationParam = $queryNameGenerator->generateParameterName('userLocation');
            $queryBuilder->addSelect(sprintf(
                'ST_DistanceSphere(%s.location, ST_GeometryFromText(:%s)) AS distance',
                $rootAlias,
                $userLocationParam
            ));
            $userLocationWKT = $user->getLocation();

            $queryBuilder->setParameter($userLocationParam, $userLocationWKT);
            $queryBuilder->addOrderBy('distance', 'asc');
        }
    }
}
