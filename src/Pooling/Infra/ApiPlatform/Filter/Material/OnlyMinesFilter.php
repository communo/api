<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Filter\Material;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\CirclePermissionType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class OnlyMinesFilter extends AbstractFilter
{
    use UserOwnershipFilterTrait;

    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private EntityManagerInterface $entityManager,
        ManagerRegistry $managerRegistry,
    ) {
        parent::__construct($managerRegistry);
    }

    /**
     * @param array<mixed> $context
     */
    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        /** @var User|null $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        $alias = $queryBuilder->getRootAliases()[0];

        $queryBuilder
            ->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->exists(self::userOwnerShipSubQuery($alias, $this->entityManager)),
                    $queryBuilder->expr()->exists(self::circleOwnerShipSubQuery($alias, $this->entityManager)),
                )
            )
            ->setParameter('user', $user)
            ->setParameter('circlePermissions', [
                CirclePermissionType::Owner,
                CirclePermissionType::Administrator,
            ]);
    }
}
