<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Filter\Material;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\IriConverterInterface;
use ApiPlatform\Metadata\Operation;
use App\AccountManagement\Entity\User\User;
use App\AccountManagement\Entity\User\UserList;
use App\Circles\Entity\CirclePermissionType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class OwnerFilter extends AbstractFilter
{
    use UserOwnershipFilterTrait;

    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private EntityManagerInterface $entityManager,
        ManagerRegistry $managerRegistry,
        private IriConverterInterface $iriConverter,
    ) {
        parent::__construct($managerRegistry);
    }

    /**
     * @param array<mixed> $context
     */
    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        $alias = $queryBuilder->getRootAliases()[0];
        $userIri = $context['filters']['owner'] ?? null;

        if (!$userIri || !($user = $this->iriConverter->getResourceFromIri($userIri))) {
            throw new \InvalidArgumentException('Owner filter requires an owner IRI');
        }

        $queryBuilder
            ->leftJoin($alias.'.pricings', 'pricing')
            ->leftJoin($alias.'.mainOwnership', 'mainOwnership')
            ->leftJoin($alias.'.ownerships', 'ownership')
            ->leftJoin('pricing.circle', 'circle')
            ->leftJoin('circle.memberships', 'membership')
            ->leftJoin('membership.user', 'circleUser')
            ->andWhere(
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->orX(
                        $queryBuilder->expr()->exists(self::userOwnerShipSubQuery($alias, $this->entityManager)),
                        $queryBuilder->expr()->exists(self::circleOwnerShipSubQuery($alias, $this->entityManager)),
                    ),
                    $queryBuilder->expr()->orX(
                        $queryBuilder->expr()->andX(
                            $queryBuilder->expr()->orX(
                                $queryBuilder->expr()->isNull('pricing'), // no circle restriction
                                $queryBuilder->expr()->isNull('pricing.circle'),
                            ),
                            $this->checkUserListSubQuery($queryBuilder, 'a') // current user is in an authorized List
                        ),
                        $queryBuilder->expr()->andX(
                            $queryBuilder->expr()->isNotNull('pricing.circle'), // circle restriction
                            $queryBuilder->expr()->eq('circleUser', ':currentUser'),// currentuser is circle member
                            $this->checkUserListSubQuery($queryBuilder, 'b') // current user is in an authorized List
                        ),
                    )
                )
            )
            ->setParameter('user', $user)
            ->setParameter('currentUser', $this->tokenStorage->getToken()?->getUser())
            ->setParameter('circlePermissions', [
                CirclePermissionType::Owner,
                CirclePermissionType::Administrator,
            ]);
    }

    private function checkUserListSubQuery(QueryBuilder $queryBuilder, string $prefix)
    {
        return $queryBuilder->expr()->orX(
            $queryBuilder->expr()->eq('('.self::userListCountSubQuery($queryBuilder->getRootAliases()[0], $prefix.'ulCount').')', 0),
            $queryBuilder->expr()->exists(self::userListExistsSubQuery($queryBuilder->getRootAliases()[0], $prefix.'ul', $prefix.'ulUser'))
        );
    }

    private function userListCountSubQuery(string $rootAlias, string $alias): string
    {
        return $this->entityManager->createQueryBuilder()
            ->select('COUNT('.$alias.'.id)')
            ->from(UserList::class, $alias)
            ->andWhere($alias.' MEMBER OF '.$rootAlias.'.userLists')
            ->getDQL();
    }

    private function userListExistsSubQuery(string $rootAlias, string $alias): string
    {
        $userAlias = 'ulUser'.$alias;

        return $this->entityManager->createQueryBuilder()
            ->select('1')
            ->from(UserList::class, $alias)
            ->leftJoin($alias.'.users', $userAlias)
            ->where($userAlias.' = :currentUser')
            ->andWhere($alias.' MEMBER OF '.$rootAlias.'.userLists')
            ->getDQL();
    }
}
