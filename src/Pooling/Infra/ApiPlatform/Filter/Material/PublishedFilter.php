<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Filter\Material;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Pooling\Domain\Model\Material\Material;
use Doctrine\ORM\QueryBuilder;

final class PublishedFilter extends AbstractFilter
{
    /**
     * @param array{filters?: array{searchTerms: string}} $context
     */
    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        $alias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->andWhere(sprintf('%s.status = :status', $alias))
            ->setParameter('status', Material::STATUS_PUBLISHED);
    }

    /** @return array<string, string> */
    public function getDescription(string $resourceClass): array
    {
        return [];
    }

    /**
     * @param array<string, mixed> $context
     * @param bool $value
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        // nothing to do here
    }
}
