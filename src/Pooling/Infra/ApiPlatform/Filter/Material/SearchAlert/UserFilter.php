<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Filter\Material\SearchAlert;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\Symfony\Security\Exception\AccessDeniedException;
use App\AccountManagement\Entity\User\User;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class UserFilter extends AbstractFilter
{
    public function __construct(
        private TokenStorageInterface $tokenStorage,
        ManagerRegistry $managerRegistry,
    ) {
        parent::__construct($managerRegistry);
    }

    /**
     * @param array<mixed> $context
     */
    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        if (!isset($context['filters']) || !\is_array($context['filters'])) {
            parent::apply($queryBuilder, $queryNameGenerator, $resourceClass, $operation, $context);

            return;
        }

        /** @var User|null $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        if (!$user instanceof User) {
            throw new AccessDeniedException();
        }
        $alias = $queryBuilder->getRootAliases()[0];

        $queryBuilder
            ->leftJoin($alias.'.user', 'user')
            ->andWhere($alias.'.user = :user')
            ->setParameter('user', $user)
        ;
    }

    public function getDescription(string $resourceClass): array
    {
        return [];
    }

    /**
     * @param array<string, mixed> $context
     * @param bool $value
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        // TODO: Implement filterProperty() method.
    }
}
