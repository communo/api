<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Filter\Material;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

final class SearchTermFilter extends AbstractFilter
{
    public function __construct(private readonly RequestStack $requestStack, protected ManagerRegistry $managerRegistry, ?LoggerInterface $logger = null, protected ?array $properties = null, protected ?NameConverterInterface $nameConverter = null)
    {
        parent::__construct($managerRegistry, $logger, $properties, $nameConverter);
    }

    /**
     * @param array{filters?: array{searchTerms: string}} $context
     */
    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        $request = $this->requestStack->getCurrentRequest();
        $searchTerms = array_key_exists('filters', $context) ? $context['filters']['searchTerms'] : null;
        if ($searchTerms === null || $searchTerms === '') {
            return;
        }

        // Parse search terms
        preg_match_all('/"(?:\\\\.|[^\\\\"])*"|\S+/', $searchTerms, $matches);
        $parsedTerms = array_diff($matches[0], StopWords::byLocale($request->getLocale()));

        $alias = $queryBuilder->getRootAliases()[0];

        $globalConditions = [];

        foreach ($parsedTerms as $key => $term) {
            $term = trim($term, '"');
            $conditions = [];
            foreach (['brand', 'name', 'reference', 'model', 'description'] as $property) {
                $conditions[] = $queryBuilder->expr()->like(sprintf('LOWER(%s.%s)', $alias, $property), ':term'.$key);
            }
            $globalConditions[] = $queryBuilder->expr()->orX(...$conditions);
            $queryBuilder->setParameter('term'.$key, sprintf('%%%s%%', strtolower($term)));
        }

        if (!empty($globalConditions)) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->andX(
                    ...$globalConditions
                )
            );
        }
    }

    /** @return array<string, string> */
    public function getDescription(string $resourceClass): array
    {
        return [];
    }

    /**
     * @param array<string, mixed> $context
     * @param bool $value
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        // nothing to do here
    }
}
