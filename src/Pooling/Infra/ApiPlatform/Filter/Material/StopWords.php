<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Filter\Material;

class StopWords
{
    public const en = ['the', 'from', 'of', 'to', 'a', 'an', 'in', 'and', 'or', 'is', 'it', 'this', 'that', 'for'];
    public const fr = ['le', 'la', 'les', 'du', 'de', 'des', 'à', 'au', 'aux', 'un', 'une', 'en', 'et', 'pour'];

    /**
     * @return string[]
     */
    public static function byLocale(string $locale): array
    {
        return match ($locale) {
            'fr' => self::fr,
            default => self::en,
        };
    }
}
