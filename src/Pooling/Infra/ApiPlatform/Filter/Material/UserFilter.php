<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Filter\Material;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\AccountManagement\Entity\User\User;
use App\AccountManagement\Entity\User\UserList;
use App\Circles\Entity\CirclePermissionType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class UserFilter extends AbstractFilter
{
    use UserOwnershipFilterTrait;

    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private EntityManagerInterface $entityManager,
        ManagerRegistry $managerRegistry,
    ) {
        parent::__construct($managerRegistry);
    }

    /**
     *  This method enhances the provided QueryBuilder with multiple joins and conditions to filter data based on user ownership, pricing circles, and user lists.
     *  It performs the following operations:
     *  - Left joins the main owner of the alias.
     *  - Inner joins the pricings and ownerships related to the alias.
     *  - Left joins the pricing circle and its memberships, further joining the users of these memberships.
     *  - Applies conditional logic to include records based on:
     *    - The absence of a pricing circle.
     *    - Existence of a user or circle ownership sub-query.
     *    - The user being a member of the pricing circle.
     *    - If UserLists are defined, the user must also be in one of the UserLists.
     *  - Sets parameters for the user and circle permissions.
     *
     * @param array<mixed> $context
     */
    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        if (!isset($context['filters']) || !\is_array($context['filters'])) {
            parent::apply($queryBuilder, $queryNameGenerator, $resourceClass, $operation, $context);

            return;
        }

        /** @var User|null $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        $alias = $queryBuilder->getRootAliases()[0];

        $queryBuilder
            ->leftJoin($alias.'.pricings', 'pricing')
            ->leftJoin($alias.'.mainOwnership', 'mainOwnership')
            ->leftJoin($alias.'.ownerships', 'ownership')
            ->leftJoin('pricing.circle', 'circle')
            ->leftJoin('circle.memberships', 'membership')
            ->leftJoin('membership.user', 'circleUser')
            ->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->orX( // user is owner
                        $queryBuilder->expr()->exists(self::userOwnerShipSubQuery($alias, $this->entityManager)),
                        $queryBuilder->expr()->exists(self::circleOwnerShipSubQuery($alias, $this->entityManager)),
                    ),
                    $queryBuilder->expr()->orX( // or no pricing / circle ownership is required
                        $queryBuilder->expr()->andX(
                            $queryBuilder->expr()->orX(// No pricing circle
                                $queryBuilder->expr()->isNull('pricing'),
                                $queryBuilder->expr()->isNull('pricing.circle'),
                            ),
                            $this->checkUserListSubQuery($queryBuilder, 'a')
                        ),// or user is a circle member
                        $queryBuilder->expr()->andX(
                            $queryBuilder->expr()->isNotNull('pricing.circle'),
                            $queryBuilder->expr()->eq('circleUser', ':user'),
                            $this->checkUserListSubQuery($queryBuilder, 'b')
                        )
                    )
                )
            )
            ->setParameter('user', $user)
            ->setParameter('circlePermissions', [
                CirclePermissionType::Owner,
                CirclePermissionType::Administrator,
            ]);
    }

    private function checkUserListSubQuery(QueryBuilder $queryBuilder, string $prefix)
    {
        return $queryBuilder->expr()->orX(
            $queryBuilder->expr()->eq('('.self::userListCountSubQuery($queryBuilder->getRootAliases()[0], $prefix.'ulCount').')', 0), // No UserList, just check the circle membership
            $queryBuilder->expr()->exists(self::userListExistsSubQuery($queryBuilder->getRootAliases()[0], $prefix.'ul', $prefix.'ulUser')) // User must be in a UserList if it exists
        );
    }

    /**
     * Generate the subquery to check if there are any user lists associated with the material.
     */
    private function userListCountSubQuery(string $rootAlias, string $alias): string
    {
        return $this->entityManager->createQueryBuilder()
            ->select('COUNT('.$alias.'.id)')
            ->from(UserList::class, $alias)
            ->andWhere($alias.' MEMBER OF '.$rootAlias.'.userLists')
            ->getDQL();
    }

    /**
     * Generate the subquery to check if the user is in any of the user lists associated with the material.
     */
    private function userListExistsSubQuery(string $rootAlias, string $alias): string
    {
        $userAlias = 'ulUser'.$alias;

        return $this->entityManager->createQueryBuilder()
            ->select('1')
            ->from(UserList::class, $alias)
            ->leftJoin($alias.'.users', $userAlias)
            ->where($userAlias.' = :user')
            ->andWhere($alias.' MEMBER OF '.$rootAlias.'.userLists')
            ->getDQL();
    }
}
