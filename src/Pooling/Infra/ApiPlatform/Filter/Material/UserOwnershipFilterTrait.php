<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Filter\Material;

use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Pooling\Domain\Model\Material\Ownership\CircleMaterialOwnership;
use App\Pooling\Domain\Model\Material\Ownership\UserMaterialOwnership;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

trait UserOwnershipFilterTrait
{
    /** @return array<string, string> */
    public function getDescription(string $resourceClass): array
    {
        return [];
    }

    /**
     * This method constructs a subquery to identify user ownership.
     */
    protected static function userOwnerShipSubQuery(string $materialRootAlias, EntityManagerInterface $entityManager): QueryBuilder
    {
        $subQueryBuilder = $entityManager->getRepository(UserMaterialOwnership::class)
            ->createQueryBuilder('user_material_ownership')
            ->innerJoin('user_material_ownership.materialMainOwned', 'main_owned_material')
            ->andWhere('user_material_ownership.user = :user')
            ->andWhere(sprintf(
                '(user_material_ownership.material = %s.id OR main_owned_material = %s)',
                $materialRootAlias,
                $materialRootAlias
            ));

        return $subQueryBuilder;
    }

    /**
     * This method constructs a subquery aimed at identifying ownership within a circle. It assesses whether a particular entity,
     * associated with the provided alias, is part of a circle in which the user has ownership rights.
     */
    protected static function circleOwnerShipSubQuery(string $materialRootAlias, EntityManagerInterface $entityManager): QueryBuilder
    {
        return $entityManager->getRepository(CircleMaterialOwnership::class)
            ->createQueryBuilder('circle_material_ownership')
            ->innerJoin('circle_material_ownership.circle', 'c2')
            ->innerJoin('circle_material_ownership.materialMainOwned', 'mom2')
            ->innerJoin('c2.memberships', 'cm2')
            ->andWhere('cm2.user = :user')
            ->andWhere('cm2.permission IN (:circlePermissions)')
            ->andWhere(sprintf(
                '(circle_material_ownership.material = %s.id OR mom2 = %s)',
                $materialRootAlias,
                $materialRootAlias,
            ))
        ;
    }

    /**
     * @param array<string, mixed> $context
     * @param bool $value
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        // TODO: Implement filterProperty() method.
    }
}
