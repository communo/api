<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Resolver\Material\Booking;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\Pooling\Application\Query\Material\Pricing\EstimatePriceHandler;
use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class ChangeDateResolver implements MutationResolverInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private TokenStorageInterface $tokenStorage,
        private EstimatePriceHandler $estimatePriceHandle
    ) {
    }

    /**
     * @param MaterialBooking|null $item
     * @param array{args: array{input: array{startDate?: string, endDate?: string}}} $context
     *
     * @throws \Exception
     */
    public function __invoke($item, array $context): ?MaterialBooking
    {
        $input = $context['args']['input'];

        /** @var User $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        $startDate = isset($input['startDate']) ? new \DateTime($input['startDate']) : $item->getStartDate();
        $endDate = isset($input['endDate']) ? new \DateTime($input['endDate']) : $item->getEndDate();

        foreach ($item->getPeriods() as $period) {
            $this->entityManager->remove($period);
        }
        $this->entityManager->flush();
        $item->setPrice(
            ($this->estimatePriceHandle)(
                materialBooking: $item,
                material: $item->getMaterial(),
                startDate: $startDate,
                endDate: $endDate,
                user: $user
            )
        );
        $this->entityManager->flush();

        return $item;
    }
}
