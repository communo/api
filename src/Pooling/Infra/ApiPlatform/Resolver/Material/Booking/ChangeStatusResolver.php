<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Resolver\Material\Booking;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\Chat\Handler\Discussion\AddMessageHandler;
use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use Symfony\Component\DependencyInjection\Attribute\Target;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Workflow\WorkflowInterface;

final class ChangeStatusResolver implements MutationResolverInterface
{
    public function __construct(
        #[Target('material_booking')]
        private WorkflowInterface $workflow,
        private TokenStorageInterface $tokenStorage,
        private AddMessageHandler $addMessage
    ) {
    }

    /**
     * @param MaterialBooking|null $item
     * @param array{args: array{input: array{transition: string, message?: string}}} $context
     */
    public function __invoke($item, array $context): ?MaterialBooking
    {
        $input = $context['args']['input'];
        $transition = $input['transition'];
        if ($this->workflow->can($item, $transition)) {
            $this->workflow->apply($item, $transition);
            if (array_key_exists('message', $input) && $input['message'] !== '' && $this->tokenStorage->getToken()) {
                /** @var User $user */
                $user = $this->tokenStorage->getToken()->getUser();
                ($this->addMessage)(
                    discussion: $item->getDiscussion(),
                    content: $input['message'],
                    user: $user
                );
            }

            return $item;
        }

        return null;
    }
}
