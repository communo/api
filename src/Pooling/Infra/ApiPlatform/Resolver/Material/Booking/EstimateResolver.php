<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Resolver\Material\Booking;

use ApiPlatform\Api\IriConverterInterface;
use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\Chat\Entity\Discussion\Discussion;
use App\Pooling\Application\Query\Material\Pricing\EstimatePriceHandler;
use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use App\Pooling\Domain\Model\Material\Material;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class EstimateResolver implements MutationResolverInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private IriConverterInterface $iriConverter,
        private TokenStorageInterface $tokenStorage,
        private EstimatePriceHandler $estimatePriceHandle
    ) {
    }

    /**
     * @param MaterialBooking|null $item
     * @param array{args: array{input: array{startDate: string, endDate: string, materialId: string}}} $context
     */
    public function __invoke($item, array $context): ?MaterialBooking
    {
        $input = $context['args']['input'];

        /** @var User $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        $startDate = new \DateTime($input['startDate']);
        $endDate = new \DateTime($input['endDate']);

        /** @var Material $material */
        $material = $this->iriConverter->getResourceFromIri($input['materialId']);
        /** @var MaterialBooking $booking */
        $booking = $this->entityManager->getRepository(MaterialBooking::class)->findOneBy([
            'material' => $material,
            'user' => $user,
            'status' => MaterialBooking::STATUS_ESTIMATING,
        ]);

        if (!$booking instanceof MaterialBooking) {
            $booking = new MaterialBooking($material);
            $booking->setUser($user);
            $material->addBooking($booking);
            $booking->setDiscussion(new Discussion());
            $this->entityManager->persist($booking);
        } else {
            foreach ($booking->getPeriods() as $period) {
                $this->entityManager->remove($period);
            }
        }
        $booking->setStartDate($startDate);
        $booking->setEndDate($endDate);
        $booking->setPrice(
            ($this->estimatePriceHandle)(
                materialBooking: $booking,
                material: $material,
                startDate: $startDate,
                endDate: $endDate,
                user: $user
            )
        );
        $this->entityManager->flush();

        return $booking;
    }
}
