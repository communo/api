<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Resolver\Material\Booking;

use ApiPlatform\GraphQl\Resolver\QueryItemResolverInterface;
use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use Symfony\Component\DependencyInjection\Attribute\Target;
use Symfony\Component\Workflow\WorkflowInterface;

class MaterialBookingResolver implements QueryItemResolverInterface
{
    public function __construct(
        #[Target('material_booking')]
        private WorkflowInterface $workflow,
    ) {
    }

    /**
     * @param MaterialBooking $item
     * @param array{args: array<int, mixed>} $context
     */
    public function __invoke($item, array $context): object
    {
        assert($item instanceof MaterialBooking);
        $transitions = [];
        foreach ($this->workflow->getEnabledTransitions($item) as $transition) {
            $transitions[] = $transition->getName();
        }
        $item->setStatusTransitionAvailables($transitions);

        return $item;
    }
}
