<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Resolver\Material\Booking;

use ApiPlatform\Api\IriConverterInterface;
use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\Pooling\Application\Query\Material\Pricing\EstimatePriceHandler;
use App\Pooling\Application\Security\Voter\MaterialVoter;
use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use App\Pooling\Domain\Model\Material\Material;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

final class UnavailableResolver implements MutationResolverInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private IriConverterInterface $iriConverter,
        private TokenStorageInterface $tokenStorage,
        private EstimatePriceHandler $estimatePriceHandle,
        private Security $security
    ) {
    }

    /**
     * @param MaterialBooking|null $item
     * @param array{args: array{input: array{startDate: string, endDate: string, materialId: string}}} $context
     *
     * @throws \Exception
     */
    public function __invoke($item, array $context): ?MaterialBooking
    {
        $input = $context['args']['input'];

        /** @var User $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        $startDate = new \DateTime($input['startDate']);
        $endDate = new \DateTime($input['endDate']);

        /** @var Material $material */
        $material = $this->iriConverter->getResourceFromIri($input['materialId']);
        if (!$this->security->isGranted(MaterialVoter::EDIT, $material)) {
            throw new AccessDeniedException();
        }
        $booking = new MaterialBooking($material);
        $booking->setUser($user);
        $booking->setStatus(MaterialBooking::STATUS_CONFIRMED);
        $material->addBooking($booking);
        $booking->setStartDate($startDate);
        $booking->setEndDate($startDate);
        $booking->setPrice(
            ($this->estimatePriceHandle)(
                materialBooking: $booking,
                material: $material,
                startDate: $startDate,
                endDate: $endDate,
                user: $user
            )
        );
        $this->entityManager->persist($booking);
        $this->entityManager->flush();

        return $booking;
    }
}
