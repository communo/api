<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Resolver\Material;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\Pooling\Domain\Model\Material\Material;
use Symfony\Component\DependencyInjection\Attribute\Target;
use Symfony\Component\Workflow\WorkflowInterface;

final class ChangeStatusResolver implements MutationResolverInterface
{
    public function __construct(
        #[Target('material')]
        private WorkflowInterface $workflow,
    ) {
    }

    /**
     * @param Material|null $item
     * @param array{args: array{input: array{transition: string, message?: string}}} $context
     */
    public function __invoke($item, array $context): ?Material
    {
        $input = $context['args']['input'];
        $transition = $input['transition'];
        $this->workflow->apply($item, $transition);

        return $item;
    }
}
