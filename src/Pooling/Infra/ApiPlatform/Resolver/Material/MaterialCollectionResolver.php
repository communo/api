<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Resolver\Material;

use ApiPlatform\GraphQl\Resolver\QueryCollectionResolverInterface;
use App\Pooling\Application\Query\Material\Pricing\FindPricingHandler;
use App\Pooling\Domain\Model\Material\Material;
use Symfony\Bundle\SecurityBundle\Security;

class MaterialCollectionResolver implements QueryCollectionResolverInterface
{
    public function __construct(
        private Security $security,
        private FindPricingHandler $findPricingHandler
    ) {
    }

    /**
     * @param iterable<Material> $collection
     * @param array<int, string> $context
     *
     * @return iterable<Material>
     */
    public function __invoke(iterable $collection, array $context): iterable
    {
        $user = $this->security->getUser();
        foreach ($collection as &$item) {
            if (is_array($item)) {
                /** @var Material $material */
                $material = $item[0];

                $distance = $item['distance'] ?? null;
                if ($distance !== null) {
                    $material->setDistance((float) $distance);
                }
            } elseif ($item instanceof Material) {
                $material = $item;
            } else {
                continue;
            }

            if ($bestPricing = ($this->findPricingHandler)($material, $user)) {
                $material->setBestPriceForUser(
                    $material->getPrice() ? min($bestPricing->getValue(), $material->getPrice()) : $bestPricing->getValue()
                );
            }

            $item = $material;
        }

        return $collection;
    }
}
