<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Resolver\Material;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\Pooling\Domain\Model\Material\MaterialImage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MaterialImageMutationResolver implements MutationResolverInterface
{
    /**
     * @param MaterialImage $item
     * @param array{
     *     args: array{
     *       input: array{
     *         id: string,
     *         material: string,
     *         dimensions: string[],
     *         imageName: string,
     *         size: int,
     *         file?: UploadedFile,
     * }}} $context
     */
    public function __invoke(
        $item,
        array $context
    ): object {
        assert($item instanceof MaterialImage);
        if (isset($context['args']['input']['file'])) {
            $item->setImageFile($context['args']['input']['file']);
        }

        return $item;
    }
}
