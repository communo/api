<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Resolver\Material;

use ApiPlatform\GraphQl\Resolver\MutationResolverInterface;
use App\Pooling\Domain\Model\Material\Material;
use App\Pooling\Domain\Model\Material\MaterialPricing;
use App\Pooling\Domain\Model\Material\Ownership\AbstractMaterialOwnership;
use App\Pooling\Domain\Model\Material\Ownership\CircleMaterialOwnership;
use App\Pooling\Domain\Model\Material\Ownership\UserMaterialOwnership;
use Doctrine\ORM\EntityManagerInterface;

class MaterialMutationResolver implements MutationResolverInterface
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /**
     * @param Material $item
     * @param array{
     *     args: array{
     *       input: array{
     *         id: string,
     *         name?: string,
     *         model?: string,
     *         brand?: string,
     *         reference?: string,
     *         category?: string,
     *         pricings?: array<int, MaterialPricing>,
     *         mainOwnership?: UserMaterialOwnership|CircleMaterialOwnership,
     *         ownerships?: array<int, UserMaterialOwnership|CircleMaterialOwnership>
     * }}} $context
     */
    public function __invoke(
        $item,
        array $context
    ): object {
        assert($item instanceof Material);
        foreach ($item->getOwnerships() as $ownership) {
            $this->entityManager->remove($ownership);
        }

        if (isset($context['args']['input']['mainOwnership'])) {
            /** @var AbstractMaterialOwnership $mainOwnership */
            $mainOwnership = $context['args']['input']['mainOwnership'];
            $item->setMainOwnership($mainOwnership);
            $this->entityManager->persist($mainOwnership);
        }
        if (isset($context['args']['input']['ownerships'])) {
            /** @var AbstractMaterialOwnership $ownership */
            foreach ($context['args']['input']['ownerships'] as $ownership) {
                $ownership->setMaterial($item);
                $this->entityManager->persist($ownership);
            }
        }
        $item->setStatus(Material::STATUS_PUBLISHED);

        return $item;
    }
}
