<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Resolver\Material;

use ApiPlatform\GraphQl\Resolver\QueryItemResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\Pooling\Application\Query\Material\Pricing\FindPricingHandler;
use App\Pooling\Domain\Model\Material\Material;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MaterialResolver implements QueryItemResolverInterface
{
    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private FindPricingHandler $findPricingHandler
    ) {
    }

    /**
     * @param Material $item
     * @param array{args: array<int, mixed>} $context
     */
    public function __invoke($item, array $context): object
    {
        assert($item instanceof Material);
        /** @var User $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        $item->setBestPriceForUser(($this->findPricingHandler)($item, $user)?->getValue() ?? $item->getPrice() ?? 0);

        return $item;
    }
}
