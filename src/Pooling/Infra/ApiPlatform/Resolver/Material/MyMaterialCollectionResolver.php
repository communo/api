<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Resolver\Material;

use ApiPlatform\GraphQl\Resolver\QueryCollectionResolverInterface;
use App\AccountManagement\Entity\User\User;
use App\Pooling\Application\Query\Material\Pricing\FindPricingHandler;
use App\Pooling\Domain\Model\Material\Material;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MyMaterialCollectionResolver implements QueryCollectionResolverInterface
{
    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private FindPricingHandler $findPricingHandler
    ) {
    }

    /**
     * @param iterable<Material> $collection
     * @param array<int, string> $context
     *
     * @return iterable<Material>
     */
    public function __invoke(iterable $collection, array $context): iterable
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        foreach ($collection as $item) {
            if ($bestPricing = ($this->findPricingHandler)($item, $user)) {
                $item->setBestPriceForUser($bestPricing);
            }
        }

        return $collection;
    }
}
