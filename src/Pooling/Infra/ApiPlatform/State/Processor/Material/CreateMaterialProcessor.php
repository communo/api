<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\State\Processor\Material;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\AccountManagement\Entity\User\User;
use App\Pooling\Domain\Model\Material\Material;
use App\Pooling\Domain\Model\Material\Ownership\AbstractMaterialOwnership;
use App\Pooling\Domain\Model\Material\Ownership\UserMaterialOwnership;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @implements ProcessorInterface<Material>
 */
class CreateMaterialProcessor implements ProcessorInterface
{
    /**
     * @param ProcessorInterface<Material> $statePersistProcessorDecorated
     */
    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private ProcessorInterface $statePersistProcessorDecorated
    ) {
    }

    /**
     * @param Material $data
     * @param array<string, mixed> $uriVariables
     * @param array<string, mixed> $context
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Material
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()?->getUser();

        if (isset($context['args']['input']['mainOwnership'])) {
            /** @var AbstractMaterialOwnership $mainOwnership */
            $mainOwnership = $context['args']['input']['mainOwnership'];
        } else {
            $mainOwnership = new UserMaterialOwnership();
            $mainOwnership->setUser($user);
            $mainOwnership->setMaterialMainOwned($data);
        }
        $data->setMainOwnership($mainOwnership);

        return $this->statePersistProcessorDecorated->process($data, $operation, $uriVariables, $context);
    }
}
