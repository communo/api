<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\State\Processor\Material;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\AccountManagement\Entity\User\User;
use App\Pooling\Domain\Model\Material\MaterialSearchAlert;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @implements ProcessorInterface<MaterialSearchAlert>
 */
class CreateMaterialSearchAlertProcessor implements ProcessorInterface
{
    /**
     * @param ProcessorInterface<MaterialSearchAlert> $statePersistProcessorDecorated
     */
    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private ProcessorInterface $statePersistProcessorDecorated
    ) {
    }

    /**
     * @param MaterialSearchAlert $data
     * @param array<string, mixed> $uriVariables
     * @param array<string, mixed> $context
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): MaterialSearchAlert
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        \assert($user instanceof User);
        $data->setUser($user);

        return $this->statePersistProcessorDecorated->process($data, $operation, $uriVariables, $context);
    }
}
