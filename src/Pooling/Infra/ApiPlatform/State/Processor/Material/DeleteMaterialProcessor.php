<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\State\Processor\Material;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use ApiPlatform\Symfony\Security\Exception\AccessDeniedException;
use App\AccountManagement\Entity\User\User;
use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use App\Pooling\Domain\Model\Material\Material;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @implements ProcessorInterface<Material>
 */
class DeleteMaterialProcessor implements ProcessorInterface
{
    /**
     * @param ProcessorInterface<Material> $stateRemoveProcessor
     */
    public function __construct(
        private TranslatorInterface $translator,
        private MailerInterface $mailer,
        private string $defaultSender,
        private ProcessorInterface $stateRemoveProcessor,
        private TokenStorageInterface $tokenStorage
    ) {
    }

    /**
     * @param Material $data
     * @param array<string, mixed> $uriVariables
     * @param array<string, mixed> $context
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Material
    {
        $connectedUser = $this->tokenStorage->getToken()->getUser();
        if (!$connectedUser instanceof User) {
            throw new AccessDeniedException();
        }
        foreach ($data->getBookings() as $booking) {
            if ($booking->getStartDate() > new \DateTime()) {
                if (in_array($booking->getStatus(), MaterialBooking::getTerminatedStatuses(), true)) {
                    $this->notifyApplicant($booking, $connectedUser);
                }
                $booking->setStatus(MaterialBooking::STATUS_CANCELED);
            }
        }

        $this->stateRemoveProcessor->process($data, $operation, $uriVariables, $context);

        return $data;
    }

    private function notifyApplicant(MaterialBooking $booking, User $userConnected): void
    {
        $user = $booking->getUser();
        if (!$user instanceof User) {
            return;
        }
        $email = (new TemplatedEmail())
            ->from(new Address($this->defaultSender, 'Communo'))
            ->to(new Address($user->getEmail(), $user->getFirstname()))
            ->subject($this->translator->trans('email.material.booking.canceledByMaterialDeletion.subject', [
                'materialName' => $booking->getMaterial()?->getName(),
            ]))
            ->htmlTemplate('emails/materials/deleted/bookingCanceled.html.twig')
            ->textTemplate('emails/materials/deleted/bookingCanceled.txt.twig')
            ->context([
                'user' => $user,
                'deletedBy' => $userConnected,
                'booking' => $booking,
            ]);
        $this->mailer->send($email);
    }
}
