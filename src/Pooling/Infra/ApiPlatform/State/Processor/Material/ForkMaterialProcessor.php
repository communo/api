<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\State\Processor\Material;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\AccountManagement\Entity\User\User;
use App\Pooling\Domain\Model\Material\Material;
use App\Pooling\Domain\Model\Material\MaterialPricing;
use App\Pooling\Domain\Model\Material\Ownership\UserMaterialOwnership;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @implements ProcessorInterface<Material>
 */
class ForkMaterialProcessor implements ProcessorInterface
{
    /**
     * @param ProcessorInterface<Material> $statePersistProcessorDecorated
     */
    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private ProcessorInterface $statePersistProcessorDecorated,
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * @param Material $data
     * @param array<string, mixed> $uriVariables
     * @param array<string, mixed> $context
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Material
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()?->getUser();
        $forkedMaterial = clone $data;
        $forkedMaterial->setStatus(Material::STATUS_DRAFT);
        $forkedMaterial->setForkedFrom($data);
        $forkedMaterial->setSlug(self::incrementString($data->getSlug()));
        foreach ($data->getImages() as $image) {
            $clonedImage = clone $image;
            $forkedMaterial->addImage($clonedImage);
            $globalCirclePricing = new MaterialPricing();
            $forkedMaterial->addPricing($globalCirclePricing);
        }

        $mainOwnership = new UserMaterialOwnership();
        $mainOwnership->setMaterialMainOwned($forkedMaterial);
        $mainOwnership->setUser($user);
        $forkedMaterial->setMainOwnership($mainOwnership);
        $this->entityManager->persist($mainOwnership);

        return $this->statePersistProcessorDecorated->process($forkedMaterial, $operation, $uriVariables, $context);
    }

    private static function incrementString(string $str): string
    {
        // check if string finish by a number
        if (preg_match('/(.*?)-(\d+)$/', $str, $matches)) {
            // $matches[1] contains str before number
            // $matches[2] contains number
            return $matches[1].'-'.(++$matches[2]);
        }

        return $str.'-1';
    }
}
