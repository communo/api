<?php

declare(strict_types=1);

namespace App\Pooling\Infra\ApiPlatform\Type\Definition;

use ApiPlatform\Api\IriConverterInterface;
use ApiPlatform\GraphQl\Type\Definition\TypeInterface;
use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\Circle;
use App\Pooling\Domain\Model\Material\Ownership\CircleMaterialOwnership;
use App\Pooling\Domain\Model\Material\Ownership\UserMaterialOwnership;
use GraphQL\Type\Definition\ScalarType;

final class MaterialOwnershipType extends ScalarType implements TypeInterface
{
    public function __construct(
        private readonly IriConverterInterface $iriConverter,
    ) {
        $this->name = 'MaterialOwnershipType';
        $this->description = 'The `MaterialOwnershipType` scalar type represents an Question Translation.';

        parent::__construct();
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     *
     * @param UserMaterialOwnership|CircleMaterialOwnership $value
     *
     * @throws \JsonException
     */
    public function serialize($value): string
    {
        if ($value instanceof UserMaterialOwnership) {
            $owner = $value->getUser();
            $ownership = 'user';
        } elseif ($value instanceof CircleMaterialOwnership) {
            $owner = $value->getCircle();
            $ownership = 'circle';
        } else {
            throw new \Exception('Not implemented yet');
        }

        return json_encode([
            $ownership => \is_null($owner) ? null : $this->iriConverter->getIriFromResource($owner),
        ], JSON_THROW_ON_ERROR);
    }

    /**
     * @param array{id: ?string, owner: ?string} $value
     */
    public function parseValue($value): UserMaterialOwnership|CircleMaterialOwnership|null
    {
        $owner = $this->iriConverter->getResourceFromIri($value['owner']);
        switch (true) {
            case $owner instanceof User:
                $ownership = new UserMaterialOwnership();
                $ownership->setUser($owner);
                break;
            case $owner instanceof Circle:
                $ownership = new CircleMaterialOwnership();
                $ownership->setCircle($owner);
                break;
            default:
                throw new \Exception('Not implemented yet');
        }

        return $ownership;
    }

    /**
     * {@inheritdoc}
     */
    public function parseLiteral($valueNode, ?array $variables = null)
    {
        throw new \Exception('Not implemented yet');
    }
}
