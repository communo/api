<?php

declare(strict_types=1);

namespace App\Pooling\Infra\Doctrine\Repository;

use App\Pooling\Domain\Model\Material\Booking\MaterialBooking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

/**
 * @method MaterialBooking|null find($id, $lockMode = null, $lockVersion = null)
 * @method MaterialBooking|null findOneBy(array $criteria, array $orderBy = null)
 * @method MaterialBooking[] findAll()
 * @method MaterialBooking[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @extends ServiceEntityRepository<MaterialBooking>
 */
class MaterialBookingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MaterialBooking::class);
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public function findByMaterialBetweenDates(Uuid $materialId, \DateTimeInterface $startDate, \DateTimeInterface $endDate, ?MaterialBooking $materialBooking): array
    {
        $startDateStr = $startDate->format('Y-m-d H:i:s');
        $endDateStr = $endDate->format('Y-m-d H:i:s');
        $query = "SELECT mb.*
                FROM material_booking mb
                WHERE mb.material_id = '{$materialId}'
                AND (mb.start_date, mb.end_date) OVERLAPS (DATE '{$startDateStr}', DATE '{$endDateStr}')
                AND mb.status NOT IN ('estimating', 'canceled', 'closed')
                ";
        if ($materialBooking instanceof MaterialBooking && $materialBookingId = $materialBooking->getId()) {
            $query .= " AND mb.id != '{$materialBookingId}'";
        }

        $bookings = $this->getEntityManager()->getConnection()->fetchAllAssociative(
            $query
        );

        return $bookings;
    }

    public function countAvailabilitiesBetweenDates(string $materialId, \DateTime $startDate, \DateTime $endDate): int
    {
        $startDateStr = $startDate->format('Y-m-d');
        $endDateStr = $endDate->format('Y-m-d');

        return $this->getEntityManager()->getConnection()->fetchOne(
            "SELECT count(mb.*)
                FROM material_booking mb
                WHERE mb.material_id = '{$materialId}'
                AND daterange(mb.start_date, mb.end_date, '[]') && daterange('{$startDateStr}', '{$endDateStr}', '[]')
                "
        );
    }
}
