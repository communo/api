<?php

declare(strict_types=1);

namespace App\Pooling\Infra\Doctrine\Repository;

use App\Pooling\Domain\Model\Material\MaterialSearchAlert;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MaterialSearchAlert>
 */
class MaterialSearchAlertRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MaterialSearchAlert::class);
    }
}
