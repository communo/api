<?php

declare(strict_types=1);

namespace App\Pooling\Infra\Doctrine\Repository;

use App\AccountManagement\Entity\User\User;
use App\Pooling\Domain\Model\Material\Material;
use App\Pooling\Domain\Model\Material\MaterialPricing;
use App\Shared\Repository\StateFullRepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MaterialPricing|null find($id, $lockMode = null, $lockVersion = null)
 * @method MaterialPricing|null findOneBy(array $criteria, array $orderBy = null)
 * @method MaterialPricing[] findAll()
 * @method MaterialPricing[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @extends ServiceEntityRepository<MaterialPricing>
 */
class PricingRepository extends ServiceEntityRepository
{
    use StateFullRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MaterialPricing::class);
    }

    /**
     * @return array<int, MaterialPricing>
     */
    public function findForMaterialAndUser(Material $material, ?User $user): array
    {
        return $this->withMaterial($material)->withUser($user)->run();
    }

    protected function withUser(?User $user): self
    {
        $this->getInstance('pricing')
            ->leftJoin('pricing.circle', 'circle')
            ->leftJoin('circle.memberships', 'membership')
            ->leftJoin('membership.user', 'user')
            ->addSelect(['circle', 'membership', 'user']);

        if ($user) {
            $this->getInstance('pricing')
                ->andWhere('user.id = :userId')
                ->setParameter('userId', $user->getId());
        } else {
            $this->getInstance('pricing')
                ->andWhere('user.id IS NULL');
        }

        return $this;
    }

    protected function withMaterial(Material $material): self
    {
        $this->getInstance('pricing')
            ->leftJoin('pricing.material', 'material')
            ->addSelect('material')
            ->where('material.id = :materialId')
            ->setParameter('materialId', $material->getId())
            ->orderBy('pricing.value', 'ASC');

        return $this;
    }
}
