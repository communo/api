<?php

declare(strict_types=1);

namespace App\Pooling\Infra\Serializer;

use App\Pooling\Application\Security\Voter\MaterialVoter;
use App\Pooling\Domain\Model\Material\Material;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\Attribute\Target;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Workflow\WorkflowInterface;

final class MaterialNormalizer implements NormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;
    private const ALREADY_CALLED = 'MATERIAL_OBJECT_NORMALIZER_ALREADY_CALLED';

    public function __construct(
        #[Target('material')]
        private WorkflowInterface $workflow,
        private Security $security,
    ) {
    }

    public function normalize($material, $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED] = true;
        $actions = [];
        foreach ($this->workflow->getEnabledTransitions($material) as $transition) {
            $actions[] = $transition->getName();
        }

        if ($this->security->isGranted(MaterialVoter::EDIT, $material)) {
            $actions[] = 'edit';
        }
        if ($this->security->isGranted(MaterialVoter::BORROW, $material)) {
            $actions[] = 'borrow';
        }
        $material->setUserActionAvailables($actions);

        return $this->normalizer->normalize($material, $format, $context);
    }

    /**
     * @param array{MATERIAL_OBJECT_NORMALIZER_ALREADY_CALLED?: bool} $context
     */
    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof Material;
    }
}
