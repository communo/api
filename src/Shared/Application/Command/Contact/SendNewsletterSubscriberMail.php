<?php

declare(strict_types=1);

namespace App\Shared\Application\Command\Contact;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Notifier\ChatterInterface;
use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Contracts\Translation\TranslatorInterface;

class SendNewsletterSubscriberMail
{
    public function __construct(
        private string $defaultSender,
        private string $defaultLocale,
        private MailerInterface $mailer,
        private TranslatorInterface $translator,
        private string $clientBaseUrl,
        private ChatterInterface $chatter
    ) {
    }

    public function __invoke(string $email): void
    {
        $mail = (new TemplatedEmail())
            ->from(new Address($this->defaultSender, 'Communo'))
            ->to(new Address($this->defaultSender, 'Communo'))
            ->replyTo(new Address($email))
            ->subject(
                $this->translator->trans(
                    'email.contact.title',
                    ['name' => $email],
                    null,
                    $this->defaultLocale
                )
            )
            ->htmlTemplate('emails/contact/contact.html.twig')
            ->textTemplate('emails/contact/contact.txt.twig')
            ->context([
                'footer_text' => 'Communo',
            ])
            ->context([
                'language' => $this->defaultLocale,
                'contact' => [
                    'email' => $email,
                    'newsletterOptIn' => true,
                ],
                'instanceUrl' => $this->clientBaseUrl,
                'actions' => [],
            ]);
        $this->mailer->send($mail);
        $chatMessage = new ChatMessage(sprintf('%s wants to subscribe our newsletter', $email));
        $this->chatter->send($chatMessage);
    }
}
