<?php

declare(strict_types=1);

namespace App\Shared\Controller\Test\Fixtures;

use App\Kernel;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/reload', name: 'test_reload_fixtures', methods: ['GET'])]
#[AsController]
class ReloadFixturesController
{
    public function __invoke(Request $request, Kernel $kernel): JsonResponse
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'hautelook:fixtures:load',
            '--no-interaction' => true,
        ]);

        // You can add extra parameters if you need to
        // $input->setArgument('username', $request->request->get('username'));

        $output = new BufferedOutput();
        $application->run($input, $output);

        return new JsonResponse([
            'success' => true,
            'message' => $output->fetch(),
        ]);
    }
}
