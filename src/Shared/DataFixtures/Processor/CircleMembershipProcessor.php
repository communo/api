<?php

declare(strict_types=1);

namespace App\Shared\DataFixtures\Processor;

use App\Circles\Entity\CircleMembership;
use Fidry\AliceDataFixtures\ProcessorInterface;
use Symfony\Component\Uid\Uuid;

class CircleMembershipProcessor implements ProcessorInterface
{
    /**
     * @param CircleMembership $object
     */
    public function preProcess(string $id, object $object): void
    {
        if (!$object instanceof CircleMembership) {
            return;
        }
        $object->setId(Uuid::v6());
    }

    /**
     * @param CircleMembership $object
     */
    public function postProcess(string $id, object $object): void
    {
    }
}
