<?php

declare(strict_types=1);

namespace App\Shared\DataFixtures\Processor;

use App\Shared\Entity\Image;
use Fidry\AliceDataFixtures\ProcessorInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageProcessor implements ProcessorInterface
{
    public function __construct(private string $projectDir)
    {
    }

    /**
     * @param Image $object
     */
    public function preProcess(string $id, object $object): void
    {
        if (!$object instanceof Image) {
            return;
        }

        $object->setImageFile(new UploadedFile(
            path: "{$this->projectDir}/fixtures/assets/{$object->getImageName()}",
            originalName: $object->getImageName()
        ));
    }

    /**
     * @param Image $object
     */
    public function postProcess(string $id, object $object): void
    {
    }
}
