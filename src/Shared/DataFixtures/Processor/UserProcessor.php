<?php

declare(strict_types=1);

namespace App\Shared\DataFixtures\Processor;

use App\AccountManagement\Entity\User\User;
use Fidry\AliceDataFixtures\ProcessorInterface;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Uid\Uuid;

class UserProcessor implements ProcessorInterface
{
    public function __construct(private UserPasswordHasherInterface $passwordHasher, private PhoneNumberUtil $phoneNumberUtil)
    {
    }

    /**
     * @param User $object
     */
    public function preProcess(string $id, object $object): void
    {
        if (!$object instanceof User) {
            return;
        }
        $object->setId(Uuid::v6());

        $object->setPhoneNumberObject($this->phoneNumberUtil->parse($object->phoneNumber, 'FR'));

        $password = $this->passwordHasher->hashPassword($object, $object->getPassword());
        $object->setPassword($password);
    }

    /**
     * @param User $object
     */
    public function postProcess(string $id, object $object): void
    {
    }
}
