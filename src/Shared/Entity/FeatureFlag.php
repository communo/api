<?php

declare(strict_types=1);

namespace App\Shared\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Query;
use App\Shared\Infra\ApiPlatform\Resolver\FeatureFlagResolver;

#[ApiResource(
    operations: [],
    graphQlOperations: [
        new Query(
            resolver: FeatureFlagResolver::class,
            args: [
                'featureName' => ['type' => 'String!'],
            ],
            description: 'request the status of the feature flag, false if not exist',
            name: 'get'
        ),
    ]
)]
readonly class FeatureFlag
{
    public function __construct(
        #[ApiProperty(identifier: true)]
        private string $featureName,
        private bool $isEnabled = false
    ) {
    }

    public function getFeatureName(): string
    {
        return $this->featureName;
    }

    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }
}
