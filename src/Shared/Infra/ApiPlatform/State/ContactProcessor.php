<?php

declare(strict_types=1);

namespace App\Shared\Infra\ApiPlatform\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Shared\Application\Command\Contact\SendContactMail;
use App\Shared\Entity\Contact;

/**
 * @implements ProcessorInterface<Contact>
 */
class ContactProcessor implements ProcessorInterface
{
    /**
     * @param ProcessorInterface<Contact> $statePersistProcessorDecorated
     */
    public function __construct(
        private ProcessorInterface $statePersistProcessorDecorated,
        private SendContactMail $sendContactMail
    ) {
    }

    /**
     * @param Contact $data
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Contact
    {
        assert($data instanceof Contact);

        $result = $this->statePersistProcessorDecorated->process($data, $operation, $uriVariables, $context);
        ($this->sendContactMail)(
            email: $data->getEmail(),
            name: $data->getName(),
            message: $data->getMessage(),
            newsletterOptIn: $data->isNewsletterOptIn()
        );

        return $result;
    }
}
