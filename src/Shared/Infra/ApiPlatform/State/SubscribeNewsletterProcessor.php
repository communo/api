<?php

declare(strict_types=1);

namespace App\Shared\Infra\ApiPlatform\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Shared\Application\Command\Contact\SendNewsletterSubscriberMail;
use App\Shared\Entity\Contact;

/**
 * @implements ProcessorInterface<Contact>
 */
class SubscribeNewsletterProcessor implements ProcessorInterface
{
    /**
     * @param ProcessorInterface<Contact> $statePersistProcessorDecorated
     */
    public function __construct(
        private ProcessorInterface $statePersistProcessorDecorated,
        private SendNewsletterSubscriberMail $sendNewsletterSubscriberMail
    ) {
    }

    /**
     * @param Contact $data
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Contact
    {
        assert($data instanceof Contact);
        $data->setNewsletterOptIn(true);

        $result = $this->statePersistProcessorDecorated->process($data, $operation, $uriVariables, $context);
        ($this->sendNewsletterSubscriberMail)(
            email: $data->getEmail(),
        );

        return $result;
    }
}
