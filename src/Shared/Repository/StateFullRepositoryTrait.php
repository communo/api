<?php

declare(strict_types=1);

namespace App\Shared\Repository;

use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

/**
 * Chained Entity Repository trait.
 *
 * This trait was written to offer repos a chained workflow.
 */
trait StateFullRepositoryTrait
{
    protected ?string $mainAlias = null;
    protected ?QueryBuilder $qb = null;

    /**
     * Get query builder instance.
     *
     * @return QueryBuilder The active or default query builder
     */
    public function getInstance(?string $alias = null)
    {
        if (!$alias && !$this->mainAlias) {
            $namespace = explode('\\', $this->_entityName);
            $alias = strtolower(end($namespace));
            $this->mainAlias = $alias;
        } elseif ($alias) {
            $this->mainAlias = $alias;
        }

        return $this->qb ?: $this->qb = $this->createQueryBuilder($this->mainAlias);
    }

    /**
     * Run active query.
     *
     * @param string $method The method to run
     * @param int $hydrationMode How the results will be (Object ? Array )
     * @param bool $autoClear AutoClear means reset active instance
     */
    public function run(string $method = 'getResult', int $hydrationMode = Query::HYDRATE_OBJECT, bool $autoClear = true): mixed
    {
        $results = $this->qb->getQuery()->$method($hydrationMode);
        if ($autoClear) {
            $this->clearInstance();
        }

        return $results;
    }

    public function clearInstance(): self
    {
        $this->qb = null;
        $this->mainAlias = null;

        return $this;
    }
}
