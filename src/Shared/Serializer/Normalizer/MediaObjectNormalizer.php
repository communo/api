<?php

declare(strict_types=1);

namespace App\Shared\Serializer\Normalizer;

use App\Shared\Entity\Image;
use Aws\S3\S3Client;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Vich\UploaderBundle\Storage\StorageInterface;

final class MediaObjectNormalizer implements NormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'MEDIA_OBJECT_NORMALIZER_ALREADY_CALLED';

    public function __construct(
        private StorageInterface $storage,
        private S3Client $s3Client,
        private string $s3PublicHost,
        private string $s3BucketName,
        private string $s3Presign
    ) {
    }

    /**
     * @param Image $object
     * @param array{MEDIA_OBJECT_NORMALIZER_ALREADY_CALLED?: bool} $context
     *
     * @throws ExceptionInterface
     */
    public function normalize($object, ?string $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null
    {
        $context[self::ALREADY_CALLED] = true;
        if ($this->s3Presign) {
            $commandOptions = [
                'Endpoint' => $this->s3PublicHost,
                'Bucket' => $this->s3BucketName,
                'Key' => $this->storage->resolvePath($object, 'imageFile'),
            ];
            $commandOptions['ResponseContentDisposition'] = 'filename='.$object->getImageName();
            $cmd = $this->s3Client->getCommand('GetObject', $commandOptions);
            $object->contentUrl = (string) $this->s3Client->createPresignedRequest($cmd, '+1 hour')->getUri();
        } else {
            $object->contentUrl = $this->s3PublicHost.$this->storage->resolveUri($object, 'imageFile');
        }

        return $this->normalizer->normalize($object, $format, $context);
    }

    /**
     * @param array{MEDIA_OBJECT_NORMALIZER_ALREADY_CALLED?: bool} $context
     */
    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof Image;
    }

    //    public function getSupportedTypes(?string $format): array {
    //        return [
    //            'object' => null, // Doesn't supports any classes or interfaces
    //            '*' => false, // Supports any other types, but the result is not cacheable
    //            Image::class => true
    //        ];
    //    }
}
