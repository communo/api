<?php

declare(strict_types=1);

namespace App\Shared\Serializer\Normalizer;

use libphonenumber\PhoneNumber;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class PhoneNumberObjectNormalizer implements NormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'PHONE_NUMBER_OBJECT_NORMALIZER_ALREADY_CALLED';

    public function __construct(
    ) {
    }

    /**
     * @param PhoneNumber $object
     * @param array{PHONE_NUMBER_OBJECT_NORMALIZER_ALREADY_CALLED?: bool} $context
     *
     * @throws ExceptionInterface
     */
    public function normalize($object, ?string $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null
    {
        $context[self::ALREADY_CALLED] = true;

        return $this->normalizer->normalize($object, $format, $context);
    }

    /**
     * @param array{PHONE_NUMBER_OBJECT_NORMALIZER_ALREADY_CALLED?: bool} $context
     */
    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof PhoneNumber;
    }
}
