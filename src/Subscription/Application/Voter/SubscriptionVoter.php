<?php

declare(strict_types=1);

namespace App\Subscription\Domain\Model;

namespace App\Subscription\Application\Voter;

use ApiPlatform\Api\IriConverterInterface;
use App\AccountManagement\Entity\User\User;
use App\Circles\Entity\Circle;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SubscriptionVoter extends Voter
{
    public const SUBSCRIPTION_CREATE = 'SUBSCRIPTION_CREATE';

    public function __construct(private RequestStack $requestStack, private IriConverterInterface $iriConverter)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        try {
            $content = json_decode($request->getContent());

            return in_array($attribute, [self::SUBSCRIPTION_CREATE], true) && $content->operationName === 'SubscribeView_Subscribe';
        } catch (\Exception) {
            return false;
        }
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }

        $request = $this->requestStack->getCurrentRequest();
        $content = json_decode($request->getContent());
        /** @var Circle $circle */
        $circle = $this->iriConverter->getResourceFromIri($content->variables->input->circle);

        return $circle->getOwners()->contains($user);
    }
}
