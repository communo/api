<?php

declare(strict_types=1);

namespace App\Subscription\Domain\Model;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Query;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatablePropertiesTrait;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [],
    paginationEnabled: false,
    graphQlOperations: [
        new Query(
            name: 'item_query'
        ),
        new QueryCollection(
            name: 'collection_query',
        ),
    ]
)]
#[ORM\Table(name: 'subscription_plan_feature')]
#[ORM\Entity]
class Feature implements TranslatableInterface, TimestampableInterface
{
    use TranslatablePropertiesTrait;
    use TranslatableMethodsTrait;
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    private ?Uuid $id = null;

    #[ORM\ManyToOne(inversedBy: 'features', targetEntity: Plan::class, cascade: ['persist', 'remove'])]
    private Plan $plan;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    private bool $workInProgress = false;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    private bool $new = false;

    public function __construct()
    {
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPlan(): Plan
    {
        return $this->plan;
    }

    public function setPlan(Plan $plan): void
    {
        $this->plan = $plan;
    }

    public function getName(): ?string
    {
        return $this->translate(null, false)->getName();
    }

    public function setName(?string $name): void
    {
        $this->translate(null, false)->setName($name);
    }

    public function getDescription(): ?string
    {
        return $this->translate(null, false)->getDescription();
    }

    public function setDescription(?string $description): void
    {
        $this->translate(null, false)->setDescription($description);
    }

    public function isWorkInProgress(): bool
    {
        return $this->workInProgress;
    }

    public function setWorkInProgress(bool $workInProgress): void
    {
        $this->workInProgress = $workInProgress;
    }

    public function isNew(): bool
    {
        return $this->new;
    }

    public function setNew(bool $new): void
    {
        $this->new = $new;
    }
}
