<?php

declare(strict_types=1);

namespace App\Subscription\Domain\Model;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Query;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatablePropertiesTrait;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [],
    paginationEnabled: false,
    graphQlOperations: [
        new Query(
            name: 'item_query'
        ),
        new QueryCollection(
            name: 'collection_query',
        ),
    ]
)]
#[ORM\Table(name: 'subscription_plan')]
#[ORM\Entity]
class Plan implements TranslatableInterface, TimestampableInterface
{
    use TranslatablePropertiesTrait;
    use TranslatableMethodsTrait;
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    #[ApiProperty(identifier: false)]
    private ?Uuid $id = null;

    #[ORM\OneToMany(mappedBy: 'plan', targetEntity: Feature::class, orphanRemoval: true)]
    private ?Collection $features;

    #[ORM\Column(type: 'float')]
    private ?float $price;

    #[ApiProperty(identifier: true)]
    #[ORM\Column(type: 'string', length: 255)]
    private string $slug;

    #[ORM\OneToMany(mappedBy: 'plan', targetEntity: Subscription::class, orphanRemoval: true)]
    private ?Collection $subscriptions;

    public function __construct()
    {
        $this->subscriptions = new ArrayCollection();
        $this->features = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->translate(null, false)->getName();
    }

    public function setName(?string $name): void
    {
        $this->translate(null, false)->setName($name);
    }

    public function getDescription(): ?string
    {
        return $this->translate(null, false)->getDescription();
    }

    public function setDescription(?string $description): void
    {
        $this->translate(null, false)->setDescription($description);
    }

    public function getButton(): ?string
    {
        return $this->translate(null, false)->getButton();
    }

    public function setButton(?string $description): void
    {
        $this->translate(null, false)->setButton($description);
    }

    public function getCondition(): ?string
    {
        return $this->translate(null, false)->getCondition();
    }

    public function setCondition(?string $description): void
    {
        $this->translate(null, false)->setCondition($description);
    }

    public function addSubscription(Subscription $subscription): self
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions->add($subscription);
            $subscription->setPlan($this);
        }

        return $this;
    }

    public function removeSubscription(Subscription $subscription): self
    {
        if ($this->subscriptions->removeElement($subscription)) {
            if ($subscription->getPlan() === $this) {
                $subscription->setPlan(null);
            }
        }

        return $this;
    }

    public function addFeature(Feature $feature): self
    {
        if (!$this->features->contains($feature)) {
            $this->features->add($feature);
            $feature->setPlan($this);
        }

        return $this;
    }

    public function removeFeature(Feature $feature): self
    {
        if ($this->features->removeElement($feature)) {
            if ($feature->getPlan() === $this) {
                $feature->setPlan(null);
            }
        }

        return $this;
    }

    public function getFeatures(): ?Collection
    {
        return $this->features;
    }

    public function setFeatures(?Collection $features): void
    {
        $this->features = $features;
    }
}
