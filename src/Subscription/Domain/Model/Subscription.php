<?php

declare(strict_types=1);

namespace App\Subscription\Domain\Model;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Mutation;
use ApiPlatform\Metadata\GraphQl\Query;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use App\Circles\Entity\Circle;
use App\Subscription\Application\Voter\SubscriptionVoter;
use App\Subscription\Infra\Doctrine\Type\DoctrineSubscriptionStatusType;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampablePropertiesTrait;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [],
    graphQlOperations: [
        new Query(
            security: 'is_granted("ROLE_ADMIN")',
            name: 'item_query'
        ),
        new QueryCollection(
            security: 'is_granted("ROLE_ADMIN")',
            name: 'collection_query'
        ),
        new Mutation(
            args: [
                'circle' => ['type' => 'String!'],
                'plan' => ['type' => 'String!'],
            ],
            security: 'is_granted("'.SubscriptionVoter::SUBSCRIPTION_CREATE.'")',
            name: 'create',
        ),
    ]
)]
#[ORM\Entity]
class Subscription implements TimestampableInterface
{
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;

    public const STATUS_TRIAL = 'trial';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_WAITING_PAYMENT = 'waitingPayment';
    public const STATUS_FORMAL_NOTICED = 'formalNoticed';
    public const STATUS_SUSPENDED = 'suspended';
    public const STATUS_STOPPED = 'stopped';

    public const TRANSITION_VALIDATE = 'validate';
    public const TRANSITION_ASK_PAYMENT = 'askPayment';
    public const TRANSITION_PUT_ON_NOTICE = 'putOnNotice';
    public const TRANSITION_SUSPEND = 'suspend';
    public const TRANSITION_STOP = 'stop';

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Assert\Uuid]
    private ?Uuid $id = null;

    #[ORM\Column(type: 'float', length: 255, nullable: true)]
    private ?float $price;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $description;

    #[ORM\ManyToOne(inversedBy: 'subscriptions', targetEntity: Plan::class, cascade: ['persist', 'remove'])]
    private Plan $plan;

    #[ORM\OneToOne(inversedBy: 'subscription', cascade: ['persist'])]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private Circle $circle;

    #[ORM\Column(type: DoctrineSubscriptionStatusType::NAME, nullable: false, options: ['default' => SubscriptionStatusType::TRIAL])]
    private SubscriptionStatusType $status = SubscriptionStatusType::TRIAL;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $startDate;

    public function __construct()
    {
        $this->startDate ??= new \DateTime();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPlan(): Plan
    {
        return $this->plan;
    }

    public function setPlan(Plan $plan): void
    {
        $this->plan = $plan;
    }

    public function getCircle(): Circle
    {
        return $this->circle;
    }

    public function setCircle(Circle $circle): void
    {
        $this->circle = $circle;
    }

    public function isActive(): bool
    {
        return $this->status->isActive();
    }

    public function getStatus(): SubscriptionStatusType
    {
        return $this->status;
    }

    public function setStatus(SubscriptionStatusType $status): void
    {
        $this->status = $status;
    }

    public function getStartDate(): \DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): void
    {
        $this->startDate = $startDate;
    }
}
