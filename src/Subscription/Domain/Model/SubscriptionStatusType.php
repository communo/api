<?php

declare(strict_types=1);

namespace App\Subscription\Domain\Model;

enum SubscriptionStatusType: string
{
    case TRIAL = Subscription::STATUS_TRIAL;
    case ACTIVE = Subscription::STATUS_ACTIVE;
    case WAITING_PAYMENT = Subscription::STATUS_WAITING_PAYMENT;
    case FORMAL_NOTICED = Subscription::STATUS_FORMAL_NOTICED;
    case SUSPENDED = Subscription::STATUS_SUSPENDED;
    case STOPPED = Subscription::STATUS_STOPPED;

    public function isActive(): bool
    {
        return !in_array($this, [self::STOPPED, self::SUSPENDED], true);
    }
}
