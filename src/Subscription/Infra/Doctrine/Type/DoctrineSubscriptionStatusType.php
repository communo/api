<?php

declare(strict_types=1);

namespace App\Subscription\Infra\Doctrine\Type;

use App\Shared\Infra\Doctrine\Type\AbstractEnumType;
use App\Subscription\Domain\Model\SubscriptionStatusType;

class DoctrineSubscriptionStatusType extends AbstractEnumType
{
    public const NAME = 'subscriptionStatusType';

    public static function getEnumsClass(): string
    {
        return SubscriptionStatusType::class;
    }

    public function getName(): string
    {
        return self::NAME;
    }
}
